﻿namespace GUI
{
    partial class frmUser_FS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUser_FS));
            this.cmsSvgClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.btnEnableUpdate = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCycleTime = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnRemoveOEE = new System.Windows.Forms.Button();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.btnOEE = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmsSvgClick
            // 
            this.cmsSvgClick.Name = "cmsSvgClick";
            this.cmsSvgClick.ShowImageMargin = false;
            this.cmsSvgClick.Size = new System.Drawing.Size(36, 4);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser1.Location = new System.Drawing.Point(12, 63);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(945, 455);
            this.webBrowser1.TabIndex = 52;
            // 
            // btnEnableUpdate
            // 
            this.btnEnableUpdate.Location = new System.Drawing.Point(872, 23);
            this.btnEnableUpdate.Name = "btnEnableUpdate";
            this.btnEnableUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnEnableUpdate.TabIndex = 53;
            this.btnEnableUpdate.Text = "Start";
            this.btnEnableUpdate.UseVisualStyleBackColor = true;
            this.btnEnableUpdate.Click += new System.EventHandler(this.btnEnableUpdate_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(653, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 55;
            this.label3.Text = "Update cyclus:";
            // 
            // txtCycleTime
            // 
            this.txtCycleTime.Location = new System.Drawing.Point(737, 25);
            this.txtCycleTime.Name = "txtCycleTime";
            this.txtCycleTime.Size = new System.Drawing.Size(36, 20);
            this.txtCycleTime.TabIndex = 56;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(779, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 57;
            this.label4.Text = "seconden";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnRemoveOEE);
            this.groupBox4.Controls.Add(this.dtpTo);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.btnOEE);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.dtpFrom);
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(625, 45);
            this.groupBox4.TabIndex = 58;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "OEE berekenen";
            // 
            // btnRemoveOEE
            // 
            this.btnRemoveOEE.Location = new System.Drawing.Point(522, 16);
            this.btnRemoveOEE.Name = "btnRemoveOEE";
            this.btnRemoveOEE.Size = new System.Drawing.Size(97, 23);
            this.btnRemoveOEE.TabIndex = 6;
            this.btnRemoveOEE.Text = "Remove OEE";
            this.btnRemoveOEE.UseMnemonic = false;
            this.btnRemoveOEE.UseVisualStyleBackColor = true;
            this.btnRemoveOEE.Click += new System.EventHandler(this.btnRemoveOEE_Click);
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(249, 19);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(166, 20);
            this.dtpTo.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(214, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tot:";
            // 
            // btnOEE
            // 
            this.btnOEE.Location = new System.Drawing.Point(421, 16);
            this.btnOEE.Name = "btnOEE";
            this.btnOEE.Size = new System.Drawing.Size(95, 23);
            this.btnOEE.TabIndex = 5;
            this.btnOEE.Text = "Show OEE";
            this.btnOEE.UseVisualStyleBackColor = true;
            this.btnOEE.Click += new System.EventHandler(this.btnOEE_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Van:";
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(41, 19);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(166, 20);
            this.dtpFrom.TabIndex = 0;
            // 
            // frmUser_FS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 528);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCycleTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnEnableUpdate);
            this.Controls.Add(this.webBrowser1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmUser_FS";
            this.Text = "BFV - User";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUser_FS_FormClosing);
            this.Load += new System.EventHandler(this.frmUser_FS_Load);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip cmsSvgClick;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Button btnEnableUpdate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCycleTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnRemoveOEE;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnOEE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpFrom;
    }
}

