﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices; //ComVisibleAttribute
using System.IO;
using System.ComponentModel;
using BLL;
using BEC;
using System.Diagnostics;
using System.Threading;


namespace GUI
{
    public partial class frmUser_FS : Form
    {
        //Database keuze
        private string _sVestiging;
        private string _sAfdeling;

        //Tab Navigatie
        private clsMachine _SelectedMachine;
        private clsSvg _oSvg; //= null als machine nog geen SVG heeft

        //Tab Gegevens
        private List<clsItemData> _lstLinkedItemValues = new List<clsItemData>();
        private List<clsSvgLink> _lstLinks = new List<clsSvgLink>();
        private clsSvgLink _oSelectedLink;
        private System.Windows.Forms.Timer _oTimer;

        //BLL
        private clsSvgHandler _oSvgHandler = new clsSvgHandler();
        private clsMachineHandler _oMachineHandler = new clsMachineHandler();
        private clsServerHandler _oServerHandler = new clsServerHandler();


        //Constructor
        public frmUser_FS(clsMachine oMachine, string sVestiging, string sAfdeling, List<clsSvgLink> lstLinks)
        {
            InitializeComponent();

            WindowState = FormWindowState.Maximized;

            _sVestiging = sVestiging;
            _sAfdeling = sAfdeling;
            _lstLinks = lstLinks;

            //connectie maken met de gekozen database
            string sConnStr = _oServerHandler.GetConnectionString(_sVestiging, _sAfdeling);
            _oMachineHandler.SetDatabase(sConnStr);

            //timer
            _oTimer = new System.Windows.Forms.Timer();
            _oTimer.Tick += new EventHandler(_oTimer_Tick);//EventHandler toewijzen

            //WebBrowser init
            webBrowser1.ObjectForScripting = new SvgEventHandler(this); //Hier staan de methodes die vanuit de SVG worden opgeroepen
            webBrowser1.Navigate("about:blank");
            _SelectMachine(oMachine);

            //datetimepickers
            //https://stackoverflow.com/questions/93472/datetimepicker-pick-both-date-and-time
            dtpFrom.Format = DateTimePickerFormat.Custom;
            dtpFrom.CustomFormat = "dd/MM/yyy   HH:mm";
            dtpTo.Format = DateTimePickerFormat.Custom;
            dtpTo.CustomFormat = "dd/MM/yyy   HH:mm";
        }

        #region CyclischUpdate
        //Cyclisch updaten van de database gegevens
        private void _oTimer_Tick(object sender, EventArgs e)
        {
            _oTimer.Enabled = false;

            ////De stopwatch kan worden gebruikt om de cyclustijd te bekijken

            //Stopwatch watch = new Stopwatch();
            //watch.Start();

            _UpdateValues();

            //watch.Stop();
            //MessageBox.Show(watch.ElapsedMilliseconds.ToString());

            _oTimer.Enabled = true;
        }

        /// <summary>
        /// De actuele waarde uit de database ophalen en weergeven in de SVG en datagrid
        /// </summary>
        private void _UpdateValues()
        {
            if (_oSvg != null)
            {
                //Thread gebruiken zodat de software operationeel blijft
                //https://www.pluralsight.com/guides/how-to-write-your-first-multi-threaded-application-with-c
                Thread thrGetValues = new Thread(() =>
                {
                    _lstLinkedItemValues = _oMachineHandler.GetItemValues(_lstLinks);//Gegevens opvragen
                    _oSvg.UpdateValues(_lstLinkedItemValues);//SVG aanpassen
                });

                thrGetValues.Start();
                thrGetValues.Join();

            }
            //SVG updaten in de webbrowser
            _UpdateWebBrowser();

        }

        /// <summary>
        /// Weergave van de webbrowser updaten
        /// </summary>
        private void _UpdateWebBrowser()
        {
            string sSVG;

            //Controle: machine en SVG geselecteerd?
            if (_SelectedMachine == null)
            {
                sSVG = "No machine selected.";
            }
            else if (_oSvg == null)
            {
                sSVG = "This machine has no SVG-file.";
            }
            else
            {
                sSVG = _oSvg.GetXmlString();//string opvragen van de SVG-figuur
            }

            //String in de webbrowser laden 
            webBrowser1.Document.OpenNew(true);
            webBrowser1.Document.Write(sSVG);
            webBrowser1.Refresh();
        }
        #endregion

        //Vangt de click-event op uit de WebBrowser (SVG)
        #region SVG_Click

        [ComVisible(true)] //COM interface zichtbaar maken
        public class SvgEventHandler //De klasse waarvan het COM zichtbaar wordt
        {
            private frmUser_FS frmMain;

            //constructor
            public SvgEventHandler(frmUser_FS MainForm)
            {
                frmMain = MainForm;
            }

            //Methode die door het SVG-event wordt aangeroepen
            public void SvgGroup_Click(string mID) //mID wordt meegegeven vanuit de SVG zelf
            {
                frmMain.OpenGroupOptions(mID);
            }
        }

        //Deze methode wordt aangeroepen vanuit de klasse 'SvgEventHandler'
        //en bepaalt welke opties mogelijk zijn bij het klikken op de svg-tekening           
        private void OpenGroupOptions(string mID)
        {
            //SVG-group aanduiden
            _oSvg.GlowGroup(mID);
            _UpdateWebBrowser();

            //Menustrip kan gebruikt worden om rechtstreeks de opties voor trending aan te  bieden
            _oSelectedLink = _lstLinks.Find(Link => Link.mID == mID);

            if (_oSelectedLink != null)
            {
                //ContextMenuStrip opbouwen
                cmsSvgClick.Items.Clear();
                cmsSvgClick.Items.Insert(0, new ToolStripLabel(_oSelectedLink.Name));
                cmsSvgClick.Items.Insert(1, new ToolStripSeparator());
                //ToolStripItem tsiInfo = cmsSvgClick.Items.Add("Info");
                ToolStripItem tsiTrends = cmsSvgClick.Items.Add("Trending");

                //EventHandlers toevoegen
                //tsiInfo.Click += TsiInfo_Click;
                tsiTrends.Click += TsiTrends_Click;

                //ContextMenuStrip weergeven
                Point MousePosition = PointToClient(System.Windows.Forms.Control.MousePosition);
                cmsSvgClick.Show(this, MousePosition);
            }
                      
        }

        private void TsiTrends_Click(object sender, EventArgs e)
        {
            try
            {
                //ConnectionString meegeven
                string sConnString = _oServerHandler.GetConnectionString(_sVestiging, _sAfdeling);

                //Gegevens ophalen


                string sName = _oSelectedLink.Name;
                string sTrendNr2 = _oSelectedLink.TrendNr;

                //!!!!!!!!!!!!!Hier niet gekend
                //controleren of het item een bitwaarde is
                bool xIsBit = true;

                //Trending form openen
                frmTrending frmTrend = new frmTrending(sConnString, sName, sTrendNr2, xIsBit);
                frmTrend.Show();
            }
            catch (Exception)
            {
                MessageBox.Show("Historische data niet beschikbaar.");
            }
        }


        #endregion


        //Methode voor het selecteren van een machine
        private void _SelectMachine(clsMachine oMachine)
        {
            if (oMachine != null)
            {
                _SelectedMachine = oMachine;

                //De SVG-bestandsnaam opvragen
                string sSvgFileName = oMachine.GenerateSvgFileName();

                //Controleren of de SVG reeds geconfigureerd is
                //_oSvg blijft 'null' als de SVG nog niet bestaat
                _oSvg = _oSvgHandler.LoadExistingFile(sSvgFileName, _sVestiging, _sAfdeling);

                //De links uit de database opvragen
                _lstLinks = _oMachineHandler.GetLinkedItems(_SelectedMachine, _oSvg);

                //De actuele waarde van de procesdata opvragen en in de SVG en datagrid weergeven
                _UpdateValues();
            }
           
        }


        //Cyclisch updaten starten of stoppen
        private void btnEnableUpdate_Click(object sender, EventArgs e)
        {
            //Als de timer loopt
            if (_oTimer.Enabled)
            {
                //Cyclisch updaten pauzeren
                _oTimer.Enabled = false;
                btnEnableUpdate.Text = "Start";
            }
            //Als de timer gestopt is
            else
            {
                int iSeconden;

                //Controleren of het aantal seconden een getal is
                if (int.TryParse(txtCycleTime.Text, out iSeconden))
                {
                    //De timer starten
                    _oTimer.Interval = iSeconden * 1000;
                    _oTimer.Enabled = true;

                    btnEnableUpdate.Text = "Stop";
                    txtCycleTime.BackColor = Color.White;
                }
                else
                {
                    MessageBox.Show("Gelieve een correcte cyclustijd mee te geven.");
                    txtCycleTime.BackColor = Color.Red;
                }
            }

        }

        //Berekenen van alle OEE waarden in de SVG-tekening
        private void btnOEE_Click(object sender, EventArgs e)
        {
            if (_oSvg != null)
            {
                //alle groepen met een OEE link verzamelen
                foreach (clsSvgGroep oGroup in _oSvg.SvgGroups.FindAll(oGr => oGr.IsOEE()))
                {
                    DateTime oBegin = dtpFrom.Value;
                    DateTime oEind = dtpTo.Value;

                    //OEE waarde opvragen en in de SVG-figuur plaatsen
                    string sOEE = _oMachineHandler.GetOEE(oGroup.TrendTag, oBegin, oEind);
                    oGroup.UpdateOEE(sOEE);
                }

                //Actuele waarden refreshen
                _UpdateValues();//Zorgt ook voor het updaten van de Webbrowser
            }
           
        }

        private void btnRemoveOEE_Click(object sender, EventArgs e)
        {
          
                _SelectMachine(_SelectedMachine);
            
        }

        private void frmUser_FS_Load(object sender, EventArgs e)
        {

        }

        private void frmUser_FS_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_oTimer.Enabled)
            {
                e.Cancel = true;
                MessageBox.Show("Gelieve eerst de updatecyclus uit te schakelen");
            }
        }
    }
}
