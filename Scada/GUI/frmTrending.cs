﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using DAL;
using BLL;


namespace GUI
{
    public partial class frmTrending : Form
    {
        private clsTrendHandler _oTrendHandler;
        private clsMachineHandler _oMachineHandler;

        //Voor het limiteren van in en uitzoomen
        private int iZoomLevel = 0;

        //gegevens uit constructor
        private string _sTrendName;
        private string _sTrendNr2;
        private bool _xIsBit;

        //Mouse click labels
        private List<DataPoint> _lstClickPoints = new List<DataPoint>();

        //#####################################################################################################

        //constructor
        public frmTrending(string sConnString, string sItemName, string sTrendNr2, bool xIsBit)
        {
            InitializeComponent();
            chart1.MouseWheel += Chart1_MouseWheel; //eventhandler toevoegen voor het inzoomen met muiswiel

            _oTrendHandler = new clsTrendHandler(sConnString);
            _oMachineHandler = new clsMachineHandler();
            _oMachineHandler.SetDatabase(sConnString);

            _sTrendName = sItemName;
            _sTrendNr2 = sTrendNr2;
            _xIsBit = xIsBit;

            //Titels van de trend invullen
            chart1.Titles.Add(sItemName);
            chart1.Titles.Add(""); //wordt ingevuld bij selecteren per ploeg            

            //Een eerste trend tonen bij het laden van de form
            _UpdateForm(12); //12 uren ver            
            
        }
              
        //Trend maken van nu tot iUren geleden
        private void _UpdateForm(int iUren)
        {
            //Berekenen van de starttijd (afhankelijk van gewenst bereik)
            TimeSpan oTimeDiff = new TimeSpan(iUren, 0, 0);
            DateTime dtEind = DateTime.Now;
            DateTime dtBegin = dtEind - oTimeDiff;

            _UpdateForm(dtBegin, dtEind);            
        }

        //Trend maken tussen 2 uren (datums)
        private void _UpdateForm(DateTime dtBegin, DateTime dtEind)
        {
            _UpdateChart(dtBegin, dtEind, _sTrendNr2); //trend tekenen
            _UpdateOEE(dtBegin, dtEind, _sTrendNr2); //OEE berekenen
            
        }

        //Maken van de Trend
        private void _UpdateChart(DateTime dtBegin, DateTime dtEind, string sTrendNr2)
        {
            List<Series> oAllSeries = _oTrendHandler.GetAllSeries(dtBegin, dtEind, sTrendNr2);

            //oudere reeksen verwijderen
            chart1.Series.Clear();

            //nieuwe reeksen toevoegen
            foreach (Series oSeries in oAllSeries)
            {
                chart1.Series.Add(oSeries);
            }

            //Instellingen
            chart1.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM\nHH:mm"; //X-as formaat
            chart1.ChartAreas[0].AxisY.IsStartedFromZero = false;
            chart1.ChartAreas[0].RecalculateAxesScale();

            //Zoom resetten
            chart1.ChartAreas[0].AxisX.ScaleView.ZoomReset(); 
            iZoomLevel = 0;
        }
               
        //Berekenen van de OEE
        private void _UpdateOEE(DateTime dtBegin, DateTime dtEind, string sTrendNr2)
        {
            //enkel bits
            if (_xIsBit)
            {
                string sOEE = _oMachineHandler.GetOEE(sTrendNr2, dtBegin, dtEind);
                txtOEE.Text = sOEE;
            }
            else
            {
                txtOEE.Text = "None";
            }
        }

       
        //Trend & OEE berekenen per ploeg
        #region Per ploeg

        //Instellen van de Ploeg knoppen (afhankelijk van week of weekend)
        private void dtpDatum_ValueChanged(object sender, EventArgs e)
        {
            //weeekend
            if (dtpDatum.Value.DayOfWeek == DayOfWeek.Saturday | dtpDatum.Value.DayOfWeek == DayOfWeek.Sunday)
            {
                btnPloeg2.Text = "17h";
                btnPloeg3.Enabled = false;
            }
            //week
            else
            {
                btnPloeg2.Text = "13h";
                btnPloeg3.Enabled = true;
            }
        }

        //Ploeg 1
        private void btnPloeg1_Click(object sender, EventArgs e)
        {
            TimeSpan oStartTime = new TimeSpan(5, 0, 0);   //starttijd
            DateTime dtBegin = dtpDatum.Value.Date + oStartTime;  //startdatum + tijd

            DateTime dtEind = new DateTime();

            //Week
            if (btnPloeg3.Enabled)
            {
                dtEind = dtBegin + new TimeSpan(8, 0, 0); //8 uren
                chart1.Titles[1].Text = dtpDatum.Value.Date.ToString("dddd, dd MMMM") + " - Ploeg: " + "P1"; //Titel = datum + ploeg
            }
            //Weekend
            else
            {
                dtEind = dtBegin + new TimeSpan(12, 0, 0); //12 uren
                chart1.Titles[1].Text = dtpDatum.Value.Date.ToString("dddd, dd MMMM") + " - Ploeg: " + "W1"; //Titel = datum + ploeg
            }

            _UpdateForm(dtBegin, dtEind);
        }

        //Ploeg 2
        private void btnPloeg2_Click(object sender, EventArgs e)
        {
            //startuur afhankelijk van week of weekendploeg
            string sStartUur = btnPloeg2.Text.Replace("h", "");
            int iStartUur = int.Parse(sStartUur);

            TimeSpan oStartTime = new TimeSpan(iStartUur, 0, 0); //starttijd
            DateTime dtBegin = dtpDatum.Value.Date + oStartTime; //startdatum + tijd

            DateTime dtEind = new DateTime();

            //week
            if (btnPloeg3.Enabled)
            {
                dtEind = dtBegin + new TimeSpan(8, 0, 0); // 8 uren
                chart1.Titles[1].Text = dtpDatum.Value.Date.ToString("dddd, dd MMMM") + " - Ploeg: " + "P2"; //Titel = datum + ploeg
            }
            //weekend
            else
            {
                dtEind = dtBegin + new TimeSpan(12, 0, 0); //12 uren
                chart1.Titles[1].Text = dtpDatum.Value.Date.ToString("dddd, dd MMMM") + " - Ploeg: " + "W2"; //Titel = datum + ploeg
            }

            _UpdateForm(dtBegin, dtEind);
        }

        //Ploeg 3
        private void btnPloeg3_Click(object sender, EventArgs e)
        {
            TimeSpan oStartTime = new TimeSpan(21, 0, 0);
            DateTime dtBegin = dtpDatum.Value.Date + oStartTime;

            TimeSpan oTS = new TimeSpan(8, 0, 0);
            DateTime dtEind = dtBegin + oTS;

            chart1.Titles[1].Text = dtpDatum.Value.Date.ToString("dddd, dd MMMM") + " - Ploeg: " + "P3";

            _UpdateForm(dtBegin, dtEind);
        }


        #endregion

        //Trend & OEE Dag,Week,TotNu
        private void btnDag_Click(object sender, EventArgs e)
        {
            TimeSpan oStartTime = new TimeSpan(0, 0, 0);
            DateTime dtBegin = dtpDatum.Value.Date + oStartTime;

            TimeSpan oTS = new TimeSpan(24, 0, 0);
            DateTime dtEind = dtBegin + oTS;

            chart1.Titles[1].Text = dtpDatum.Value.Date.ToString("dddd, dd MMMM") + " - 24h ";

            _UpdateForm(dtBegin, dtEind);
        }

        private void btnWeek_Click(object sender, EventArgs e)
        {
            TimeSpan oStartTime = new TimeSpan(0, 0, 0);
            DateTime dtBegin = dtpDatum.Value.Date + oStartTime;

            TimeSpan oTS = new TimeSpan(7, 0, 0, 0);
            DateTime dtEind = dtBegin + oTS;

            chart1.Titles[1].Text = dtpDatum.Value.Date.ToString("dddd, dd MMMM") + " - " + dtEind.ToString("dddd, dd MMMM");

            _UpdateForm(dtBegin, dtEind);
        }

        private void btnTotNu_Click(object sender, EventArgs e)
        {
            TimeSpan oStartTime = new TimeSpan(0, 0, 0);
            DateTime dtBegin = dtpDatum.Value.Date + oStartTime;
            DateTime dtEind = DateTime.Now;

            chart1.Titles[1].Text = dtpDatum.Value.Date.ToString("dddd, dd MMMM") + " - " + dtEind.ToString("dddd, dd MMMM");

            _UpdateForm(dtBegin, dtEind);
        }


        //Maakt gebruik van de zoomfunctie mogelijk
        private void chart1_MouseEnter(object sender, EventArgs e)
        {
            if (!chart1.Focused)
                chart1.Focus();
        }

        //Hindert gebruik van de zoomfunctie
        private void chart1_MouseLeave(object sender, EventArgs e)
        {
            if (chart1.Focused)
                chart1.Parent.Focus();
        }

        //Zoomfunctie
        private void Chart1_MouseWheel(object sender, MouseEventArgs e)
        {
            //inzoomen
            if (e.Delta > 0)
            {
                //maximaal 9x inzoomen
                if (iZoomLevel < 9)
                {
                    try
                    {
                        //huidige minimum en maximum waarde
                        double xMin = chart1.ChartAreas[0].AxisX.ScaleView.ViewMinimum;
                        double xMax = chart1.ChartAreas[0].AxisX.ScaleView.ViewMaximum;

                        //deze formule kan aangepast worden voor meer of minder zoomen per scroll
                        double posXStart = chart1.ChartAreas[0].AxisX.PixelPositionToValue(e.Location.X) - (xMax - xMin) / 3;
                        double posXFinish = chart1.ChartAreas[0].AxisX.PixelPositionToValue(e.Location.X) + (xMax - xMin) / 3;

                        chart1.ChartAreas[0].AxisX.ScaleView.Zoom(posXStart, posXFinish);

                        iZoomLevel++;
                    }
                    catch (Exception)
                    { }

                }
            }
            //uitzoomen
            else
            {
                //volledig uitzoomen
                chart1.ChartAreas[0].AxisX.ScaleView.ZoomReset();
                iZoomLevel = 0;
            }
        }
                   
        //Voor het weergeven van labels in de trend
        private void chart1_MouseDown(object sender, MouseEventArgs e)
        {
            HitTestResult result = chart1.HitTest( e.X, e.Y );
            if (result.ChartElementType == ChartElementType.DataPoint)
            {
                DataPoint oPoint = result.Object as DataPoint;
                if (oPoint != null)
                {
                    string sLabel = DateTime.FromOADate(oPoint.XValue).ToString("HH:mm"); //Voor het omzetten van de 'double' naar datetime formaat

                    //Nieuw label toevoegen
                    oPoint.Label = sLabel;
                    oPoint.MarkerStyle = MarkerStyle.Square;

                    _lstClickPoints.Add(oPoint);

                }
            }
        }
        //Voor het verwijderen van alle labels
        private void btnDeleteLabels_Click(object sender, EventArgs e)
        {           
            foreach (DataPoint oDP in _lstClickPoints)
            {
                oDP.Label = "";
                oDP.MarkerStyle = MarkerStyle.None;
            }
            _lstClickPoints.Clear();
        }
        
        //Voor het exporteren van de huidige trend in JPEG formaat
        private void btnExportChart_Click(object sender, EventArgs e)
        {
            string sFileName = "Trend_" + _sTrendName; //Voorstel FileName

            //Gebruiker een bestandslocatie laten kiezen
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "JPEG (*.jpg)|*.jpg";
            sfd.FileName = sFileName;
            if (sfd.ShowDialog() == DialogResult.OK)
            {               
                try
                {
                    chart1.SaveImage(sfd.FileName, ChartImageFormat.Jpeg);
                }
                catch (Exception)
                {
                    MessageBox.Show("Een bestand met dezelfde naam wordt momenteel door een ander programma gebruikt.",
                                    "Niet opgeslagen",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

       
    }
}
