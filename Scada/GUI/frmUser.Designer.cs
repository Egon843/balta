﻿namespace GUI
{
    partial class frmUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUser));
            this.cmsSvgClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cboVestiging = new System.Windows.Forms.ComboBox();
            this.lblVestiging = new System.Windows.Forms.Label();
            this.lblAfdeling = new System.Windows.Forms.Label();
            this.cboAfdeling = new System.Windows.Forms.ComboBox();
            this.lboMachinegroep = new System.Windows.Forms.ListBox();
            this.lblMachinegroep = new System.Windows.Forms.Label();
            this.lblLevel = new System.Windows.Forms.Label();
            this.lboLevel = new System.Windows.Forms.ListBox();
            this.lblSubgroep = new System.Windows.Forms.Label();
            this.lboSubgroep = new System.Windows.Forms.ListBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Browse = new System.Windows.Forms.TabPage();
            this.btnFullScreen = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dgvLinkedItems = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnTrending = new System.Windows.Forms.Button();
            this.btnRapport = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnRemoveOEE = new System.Windows.Forms.Button();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.btnOEE = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.btnEnableUpdate = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCycleTime = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.Browse.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLinkedItems)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmsSvgClick
            // 
            this.cmsSvgClick.Name = "cmsSvgClick";
            this.cmsSvgClick.ShowImageMargin = false;
            this.cmsSvgClick.Size = new System.Drawing.Size(36, 4);
            // 
            // cboVestiging
            // 
            this.cboVestiging.FormattingEnabled = true;
            this.cboVestiging.Location = new System.Drawing.Point(79, 13);
            this.cboVestiging.Name = "cboVestiging";
            this.cboVestiging.Size = new System.Drawing.Size(145, 21);
            this.cboVestiging.TabIndex = 33;
            this.cboVestiging.SelectedIndexChanged += new System.EventHandler(this.cboVestiging_SelectedIndexChanged);
            // 
            // lblVestiging
            // 
            this.lblVestiging.AutoSize = true;
            this.lblVestiging.Location = new System.Drawing.Point(6, 16);
            this.lblVestiging.Name = "lblVestiging";
            this.lblVestiging.Size = new System.Drawing.Size(53, 13);
            this.lblVestiging.TabIndex = 34;
            this.lblVestiging.Text = "Vestiging:";
            // 
            // lblAfdeling
            // 
            this.lblAfdeling.AutoSize = true;
            this.lblAfdeling.Location = new System.Drawing.Point(6, 43);
            this.lblAfdeling.Name = "lblAfdeling";
            this.lblAfdeling.Size = new System.Drawing.Size(48, 13);
            this.lblAfdeling.TabIndex = 36;
            this.lblAfdeling.Text = "Afdeling:";
            // 
            // cboAfdeling
            // 
            this.cboAfdeling.FormattingEnabled = true;
            this.cboAfdeling.Location = new System.Drawing.Point(79, 40);
            this.cboAfdeling.Name = "cboAfdeling";
            this.cboAfdeling.Size = new System.Drawing.Size(145, 21);
            this.cboAfdeling.TabIndex = 35;
            this.cboAfdeling.SelectedIndexChanged += new System.EventHandler(this.cboAfdeling_SelectedIndexChanged);
            // 
            // lboMachinegroep
            // 
            this.lboMachinegroep.FormattingEnabled = true;
            this.lboMachinegroep.Location = new System.Drawing.Point(6, 32);
            this.lboMachinegroep.Name = "lboMachinegroep";
            this.lboMachinegroep.Size = new System.Drawing.Size(218, 82);
            this.lboMachinegroep.TabIndex = 37;
            this.lboMachinegroep.SelectedIndexChanged += new System.EventHandler(this.lboMachinegroep_SelectedIndexChanged);
            // 
            // lblMachinegroep
            // 
            this.lblMachinegroep.AutoSize = true;
            this.lblMachinegroep.Location = new System.Drawing.Point(6, 16);
            this.lblMachinegroep.Name = "lblMachinegroep";
            this.lblMachinegroep.Size = new System.Drawing.Size(39, 13);
            this.lblMachinegroep.TabIndex = 38;
            this.lblMachinegroep.Text = "Groep:";
            // 
            // lblLevel
            // 
            this.lblLevel.AutoSize = true;
            this.lblLevel.Location = new System.Drawing.Point(6, 117);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(36, 13);
            this.lblLevel.TabIndex = 40;
            this.lblLevel.Text = "Level:";
            // 
            // lboLevel
            // 
            this.lboLevel.FormattingEnabled = true;
            this.lboLevel.Location = new System.Drawing.Point(6, 133);
            this.lboLevel.Name = "lboLevel";
            this.lboLevel.Size = new System.Drawing.Size(218, 82);
            this.lboLevel.TabIndex = 39;
            this.lboLevel.SelectedIndexChanged += new System.EventHandler(this.lboLevel_SelectedIndexChanged);
            // 
            // lblSubgroep
            // 
            this.lblSubgroep.AutoSize = true;
            this.lblSubgroep.Location = new System.Drawing.Point(6, 218);
            this.lblSubgroep.Name = "lblSubgroep";
            this.lblSubgroep.Size = new System.Drawing.Size(56, 13);
            this.lblSubgroep.TabIndex = 44;
            this.lblSubgroep.Text = "Subgroep:";
            // 
            // lboSubgroep
            // 
            this.lboSubgroep.FormattingEnabled = true;
            this.lboSubgroep.Location = new System.Drawing.Point(6, 234);
            this.lboSubgroep.Name = "lboSubgroep";
            this.lboSubgroep.Size = new System.Drawing.Size(218, 69);
            this.lboSubgroep.TabIndex = 43;
            this.lboSubgroep.SelectedIndexChanged += new System.EventHandler(this.lboSubgroep_SelectedIndexChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.Browse);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 29);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(270, 493);
            this.tabControl1.TabIndex = 47;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // Browse
            // 
            this.Browse.AutoScroll = true;
            this.Browse.BackColor = System.Drawing.SystemColors.Control;
            this.Browse.Controls.Add(this.btnFullScreen);
            this.Browse.Controls.Add(this.groupBox2);
            this.Browse.Controls.Add(this.groupBox1);
            this.Browse.Location = new System.Drawing.Point(4, 22);
            this.Browse.Name = "Browse";
            this.Browse.Padding = new System.Windows.Forms.Padding(3);
            this.Browse.Size = new System.Drawing.Size(262, 467);
            this.Browse.TabIndex = 0;
            this.Browse.Text = "Navigatie";
            // 
            // btnFullScreen
            // 
            this.btnFullScreen.Location = new System.Drawing.Point(12, 408);
            this.btnFullScreen.Name = "btnFullScreen";
            this.btnFullScreen.Size = new System.Drawing.Size(218, 23);
            this.btnFullScreen.TabIndex = 58;
            this.btnFullScreen.Text = "Full Screen";
            this.btnFullScreen.UseVisualStyleBackColor = true;
            this.btnFullScreen.Click += new System.EventHandler(this.btnFullScreen_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblVestiging);
            this.groupBox2.Controls.Add(this.lblAfdeling);
            this.groupBox2.Controls.Add(this.cboAfdeling);
            this.groupBox2.Controls.Add(this.cboVestiging);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(230, 73);
            this.groupBox2.TabIndex = 45;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Database";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblMachinegroep);
            this.groupBox1.Controls.Add(this.lboLevel);
            this.groupBox1.Controls.Add(this.lblSubgroep);
            this.groupBox1.Controls.Add(this.lblLevel);
            this.groupBox1.Controls.Add(this.lboSubgroep);
            this.groupBox1.Controls.Add(this.lboMachinegroep);
            this.groupBox1.Location = new System.Drawing.Point(6, 85);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(230, 317);
            this.groupBox1.TabIndex = 50;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Machine";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(262, 467);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "Gegevens";
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox5.Controls.Add(this.dgvLinkedItems);
            this.groupBox5.Location = new System.Drawing.Point(7, 7);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(217, 260);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Actuele data";
            // 
            // dgvLinkedItems
            // 
            this.dgvLinkedItems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvLinkedItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLinkedItems.Location = new System.Drawing.Point(6, 19);
            this.dgvLinkedItems.Name = "dgvLinkedItems";
            this.dgvLinkedItems.Size = new System.Drawing.Size(205, 235);
            this.dgvLinkedItems.TabIndex = 1;
            this.dgvLinkedItems.SelectionChanged += new System.EventHandler(this.dgvLinkedItems_SelectionChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.btnTrending);
            this.groupBox3.Controls.Add(this.btnRapport);
            this.groupBox3.Location = new System.Drawing.Point(7, 382);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(217, 79);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Historische data";
            // 
            // btnTrending
            // 
            this.btnTrending.Location = new System.Drawing.Point(8, 19);
            this.btnTrending.Name = "btnTrending";
            this.btnTrending.Size = new System.Drawing.Size(198, 23);
            this.btnTrending.TabIndex = 2;
            this.btnTrending.Text = "Trending";
            this.btnTrending.UseVisualStyleBackColor = true;
            this.btnTrending.Click += new System.EventHandler(this.btnTrending_Click);
            // 
            // btnRapport
            // 
            this.btnRapport.Location = new System.Drawing.Point(8, 48);
            this.btnRapport.Name = "btnRapport";
            this.btnRapport.Size = new System.Drawing.Size(198, 23);
            this.btnRapport.TabIndex = 3;
            this.btnRapport.Text = "Rapport";
            this.btnRapport.UseVisualStyleBackColor = true;
            this.btnRapport.Click += new System.EventHandler(this.btnRapport_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox4.Controls.Add(this.btnRemoveOEE);
            this.groupBox4.Controls.Add(this.dtpTo);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.btnOEE);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.dtpFrom);
            this.groupBox4.Location = new System.Drawing.Point(6, 273);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(218, 102);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "OEE berekenen";
            // 
            // btnRemoveOEE
            // 
            this.btnRemoveOEE.Location = new System.Drawing.Point(110, 71);
            this.btnRemoveOEE.Name = "btnRemoveOEE";
            this.btnRemoveOEE.Size = new System.Drawing.Size(97, 23);
            this.btnRemoveOEE.TabIndex = 6;
            this.btnRemoveOEE.Text = "Remove OEE";
            this.btnRemoveOEE.UseMnemonic = false;
            this.btnRemoveOEE.UseVisualStyleBackColor = true;
            this.btnRemoveOEE.Click += new System.EventHandler(this.btnRemoveOEE_Click);
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(41, 45);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(166, 20);
            this.dtpTo.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tot:";
            // 
            // btnOEE
            // 
            this.btnOEE.Location = new System.Drawing.Point(9, 71);
            this.btnOEE.Name = "btnOEE";
            this.btnOEE.Size = new System.Drawing.Size(95, 23);
            this.btnOEE.TabIndex = 5;
            this.btnOEE.Text = "Show OEE";
            this.btnOEE.UseVisualStyleBackColor = true;
            this.btnOEE.Click += new System.EventHandler(this.btnOEE_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Van:";
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(41, 19);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(166, 20);
            this.dtpFrom.TabIndex = 0;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser1.Location = new System.Drawing.Point(288, 51);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(669, 467);
            this.webBrowser1.TabIndex = 52;
            // 
            // btnEnableUpdate
            // 
            this.btnEnableUpdate.Location = new System.Drawing.Point(506, 12);
            this.btnEnableUpdate.Name = "btnEnableUpdate";
            this.btnEnableUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnEnableUpdate.TabIndex = 53;
            this.btnEnableUpdate.Text = "Start";
            this.btnEnableUpdate.UseVisualStyleBackColor = true;
            this.btnEnableUpdate.Click += new System.EventHandler(this.btnEnableUpdate_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(287, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 55;
            this.label3.Text = "Update cyclus:";
            // 
            // txtCycleTime
            // 
            this.txtCycleTime.Location = new System.Drawing.Point(371, 14);
            this.txtCycleTime.Name = "txtCycleTime";
            this.txtCycleTime.Size = new System.Drawing.Size(36, 20);
            this.txtCycleTime.TabIndex = 56;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(413, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 57;
            this.label4.Text = "seconden";
            // 
            // frmUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 528);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCycleTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnEnableUpdate);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmUser";
            this.Text = "BFV - User";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUser_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.Browse.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLinkedItems)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip cmsSvgClick;
        private System.Windows.Forms.ComboBox cboVestiging;
        private System.Windows.Forms.Label lblVestiging;
        private System.Windows.Forms.Label lblAfdeling;
        private System.Windows.Forms.ComboBox cboAfdeling;
        private System.Windows.Forms.ListBox lboMachinegroep;
        private System.Windows.Forms.Label lblMachinegroep;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.ListBox lboLevel;
        private System.Windows.Forms.Label lblSubgroep;
        private System.Windows.Forms.ListBox lboSubgroep;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Browse;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvLinkedItems;
        private System.Windows.Forms.Button btnRapport;
        private System.Windows.Forms.Button btnTrending;
        private System.Windows.Forms.Button btnOEE;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.Button btnEnableUpdate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCycleTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnRemoveOEE;
        private System.Windows.Forms.Button btnFullScreen;
    }
}

