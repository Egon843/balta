﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace GUI
{
    public partial class frmLogin : Form
    {
        //Tutorial
        //https://www.godo.dev/tutorials/csharp-string-hash/
        
        //Constructor
        public frmLogin()
        {
            InitializeComponent();            
            txtPwd.PasswordChar = '●';//Symbool voor de weergave van de tekst
        }

        //Openen van de 'User' omgeving
        private void btnUser_Click(object sender, EventArgs e)
        {
            frmUser oFormUser = new frmUser();
            oFormUser.Show();
        }

        //Openen van de 'Admin' omgeving
        private void btnAdmin_Click(object sender, EventArgs e)
        {
            //Controle van het wachtwoord
            if (clsHasher.CheckHash(txtUser.Text, txtPwd.Text))
            {
                frmDevelopment oFormDev = new frmDevelopment(txtUser.Text);
                oFormDev.ShowDialog();
            }
            else
            {
                MessageBox.Show("Gebruiker of wachtwoord onjuist.");
            }

            txtPwd.Text = "";
        }

        //Maakt het gebruik van 'Enter' mogelijk
        private void txtUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPwd.Focus();//Tekstvak 'wachtwoord' focussen
            }
        }
        private void txtPwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAdmin_Click(this, new EventArgs());//Proberen aanmelden als 'Admin'
            }
        }
    }
}
