﻿namespace GUI
{
    partial class frmDevelopment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDevelopment));
            this.btnSave = new System.Windows.Forms.Button();
            this.cmsSvgClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAddDatabases = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAddUsers = new System.Windows.Forms.ToolStripMenuItem();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chbOEE = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboProgressBar = new System.Windows.Forms.ComboBox();
            this.btnDeleteLink = new System.Windows.Forms.Button();
            this.dgvLinkedItems = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lboItems = new System.Windows.Forms.ListBox();
            this.txtTrendNr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gpbFilter = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboDatagroep = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboTrendGroep = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboLevel = new System.Windows.Forms.ComboBox();
            this.cboSubgroep = new System.Windows.Forms.ComboBox();
            this.Browse = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblVestiging = new System.Windows.Forms.Label();
            this.lblAfdeling = new System.Windows.Forms.Label();
            this.cboAfdeling = new System.Windows.Forms.ComboBox();
            this.cboVestiging = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblMachinegroep = new System.Windows.Forms.Label();
            this.lboLevel = new System.Windows.Forms.ListBox();
            this.lblSubgroep = new System.Windows.Forms.Label();
            this.lblLevel = new System.Windows.Forms.Label();
            this.lboSubgroep = new System.Windows.Forms.ListBox();
            this.lboMachinegroep = new System.Windows.Forms.ListBox();
            this.gpbSVG = new System.Windows.Forms.GroupBox();
            this.btnUpdateSVG = new System.Windows.Forms.Button();
            this.btnDeleteSvg = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnImportSvg = new System.Windows.Forms.Button();
            this.txtSvgFileName = new System.Windows.Forms.TextBox();
            this.btnGenerateSvgs = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLinkedItems)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.gpbFilter.SuspendLayout();
            this.Browse.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gpbSVG.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(0, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 45;
            // 
            // cmsSvgClick
            // 
            this.cmsSvgClick.Name = "cmsSvgClick";
            this.cmsSvgClick.ShowImageMargin = false;
            this.cmsSvgClick.Size = new System.Drawing.Size(36, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(711, 24);
            this.menuStrip1.TabIndex = 44;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAddDatabases,
            this.tsmAddUsers});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // tsmAddDatabases
            // 
            this.tsmAddDatabases.Name = "tsmAddDatabases";
            this.tsmAddDatabases.Size = new System.Drawing.Size(151, 22);
            this.tsmAddDatabases.Text = "Add databases";
            this.tsmAddDatabases.Click += new System.EventHandler(this.tsmAddDatabases_Click);
            // 
            // tsmAddUsers
            // 
            this.tsmAddUsers.Name = "tsmAddUsers";
            this.tsmAddUsers.Size = new System.Drawing.Size(151, 22);
            this.tsmAddUsers.Text = "Add users";
            this.tsmAddUsers.Click += new System.EventHandler(this.tsmAddUsers_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser1.Location = new System.Drawing.Point(288, 51);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(411, 638);
            this.webBrowser1.TabIndex = 52;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.gpbFilter);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(262, 612);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Link SQL data";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chbOEE);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.cboProgressBar);
            this.groupBox4.Controls.Add(this.btnDeleteLink);
            this.groupBox4.Controls.Add(this.dgvLinkedItems);
            this.groupBox4.Location = new System.Drawing.Point(9, 358);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(230, 253);
            this.groupBox4.TabIndex = 48;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Linked SVG groups";
            // 
            // chbOEE
            // 
            this.chbOEE.AutoSize = true;
            this.chbOEE.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbOEE.Location = new System.Drawing.Point(173, 229);
            this.chbOEE.Name = "chbOEE";
            this.chbOEE.Size = new System.Drawing.Size(51, 17);
            this.chbOEE.TabIndex = 7;
            this.chbOEE.Text = "OEE:";
            this.chbOEE.UseVisualStyleBackColor = true;
            this.chbOEE.CheckedChanged += new System.EventHandler(this.chbOEE_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 230);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Progressbar:";
            // 
            // cboProgressBar
            // 
            this.cboProgressBar.FormattingEnabled = true;
            this.cboProgressBar.Items.AddRange(new object[] {
            "None",
            "Up",
            "Left",
            "Right"});
            this.cboProgressBar.Location = new System.Drawing.Point(71, 227);
            this.cboProgressBar.Name = "cboProgressBar";
            this.cboProgressBar.Size = new System.Drawing.Size(71, 21);
            this.cboProgressBar.TabIndex = 4;
            this.cboProgressBar.SelectionChangeCommitted += new System.EventHandler(this.cboProgressBar_SelectionChangeCommitted);
            // 
            // btnDeleteLink
            // 
            this.btnDeleteLink.Location = new System.Drawing.Point(6, 191);
            this.btnDeleteLink.Name = "btnDeleteLink";
            this.btnDeleteLink.Size = new System.Drawing.Size(218, 30);
            this.btnDeleteLink.TabIndex = 1;
            this.btnDeleteLink.Text = "DELETE";
            this.btnDeleteLink.UseVisualStyleBackColor = true;
            this.btnDeleteLink.Click += new System.EventHandler(this.btnDeleteLink_Click);
            // 
            // dgvLinkedItems
            // 
            this.dgvLinkedItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLinkedItems.Location = new System.Drawing.Point(6, 19);
            this.dgvLinkedItems.Name = "dgvLinkedItems";
            this.dgvLinkedItems.Size = new System.Drawing.Size(218, 166);
            this.dgvLinkedItems.TabIndex = 0;
            this.dgvLinkedItems.SelectionChanged += new System.EventHandler(this.dgvLinkedItems_SelectionChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lboItems);
            this.groupBox3.Controls.Add(this.txtTrendNr);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(9, 139);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(230, 213);
            this.groupBox3.TabIndex = 56;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "SQL-data";
            // 
            // lboItems
            // 
            this.lboItems.FormattingEnabled = true;
            this.lboItems.Location = new System.Drawing.Point(6, 19);
            this.lboItems.Name = "lboItems";
            this.lboItems.Size = new System.Drawing.Size(218, 160);
            this.lboItems.TabIndex = 45;
            this.lboItems.SelectedIndexChanged += new System.EventHandler(this.lboItems_SelectedIndexChanged);
            // 
            // txtTrendNr
            // 
            this.txtTrendNr.Location = new System.Drawing.Point(111, 185);
            this.txtTrendNr.Name = "txtTrendNr";
            this.txtTrendNr.Size = new System.Drawing.Size(113, 20);
            this.txtTrendNr.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 188);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 47;
            this.label1.Text = "TrendNr:";
            // 
            // gpbFilter
            // 
            this.gpbFilter.Controls.Add(this.label2);
            this.gpbFilter.Controls.Add(this.cboDatagroep);
            this.gpbFilter.Controls.Add(this.label7);
            this.gpbFilter.Controls.Add(this.cboTrendGroep);
            this.gpbFilter.Controls.Add(this.label4);
            this.gpbFilter.Controls.Add(this.label5);
            this.gpbFilter.Controls.Add(this.cboLevel);
            this.gpbFilter.Controls.Add(this.cboSubgroep);
            this.gpbFilter.Location = new System.Drawing.Point(9, 6);
            this.gpbFilter.Name = "gpbFilter";
            this.gpbFilter.Size = new System.Drawing.Size(230, 127);
            this.gpbFilter.TabIndex = 55;
            this.gpbFilter.TabStop = false;
            this.gpbFilter.Text = "Filter";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 58;
            this.label2.Text = "Datagroep:";
            // 
            // cboDatagroep
            // 
            this.cboDatagroep.FormattingEnabled = true;
            this.cboDatagroep.Items.AddRange(new object[] {
            "PV",
            "SP",
            "OUT",
            "MAX",
            "MIN",
            "AVG",
            "ED",
            "CNT",
            "OEE"});
            this.cboDatagroep.Location = new System.Drawing.Point(77, 94);
            this.cboDatagroep.Name = "cboDatagroep";
            this.cboDatagroep.Size = new System.Drawing.Size(147, 21);
            this.cboDatagroep.TabIndex = 57;
            this.cboDatagroep.SelectionChangeCommitted += new System.EventHandler(this.cboDatagroep_SelectionChangeCommitted);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 56;
            this.label7.Text = "Trendgroep:";
            // 
            // cboTrendGroep
            // 
            this.cboTrendGroep.FormattingEnabled = true;
            this.cboTrendGroep.Location = new System.Drawing.Point(77, 67);
            this.cboTrendGroep.Name = "cboTrendGroep";
            this.cboTrendGroep.Size = new System.Drawing.Size(147, 21);
            this.cboTrendGroep.TabIndex = 55;
            this.cboTrendGroep.SelectionChangeCommitted += new System.EventHandler(this.cboTrendGroep_SelectionChangeCommitted);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 53;
            this.label4.Text = "Level:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 54;
            this.label5.Text = "Subgroep:";
            // 
            // cboLevel
            // 
            this.cboLevel.FormattingEnabled = true;
            this.cboLevel.Location = new System.Drawing.Point(77, 13);
            this.cboLevel.Name = "cboLevel";
            this.cboLevel.Size = new System.Drawing.Size(147, 21);
            this.cboLevel.TabIndex = 51;
            this.cboLevel.SelectionChangeCommitted += new System.EventHandler(this.cboLevel_SelectionChangeCommitted);
            // 
            // cboSubgroep
            // 
            this.cboSubgroep.FormattingEnabled = true;
            this.cboSubgroep.Location = new System.Drawing.Point(77, 40);
            this.cboSubgroep.Name = "cboSubgroep";
            this.cboSubgroep.Size = new System.Drawing.Size(147, 21);
            this.cboSubgroep.TabIndex = 52;
            this.cboSubgroep.SelectionChangeCommitted += new System.EventHandler(this.cboSubgroep_SelectionChangeCommitted);
            // 
            // Browse
            // 
            this.Browse.AutoScroll = true;
            this.Browse.BackColor = System.Drawing.SystemColors.Control;
            this.Browse.Controls.Add(this.groupBox2);
            this.Browse.Controls.Add(this.groupBox1);
            this.Browse.Controls.Add(this.gpbSVG);
            this.Browse.Location = new System.Drawing.Point(4, 22);
            this.Browse.Name = "Browse";
            this.Browse.Padding = new System.Windows.Forms.Padding(3);
            this.Browse.Size = new System.Drawing.Size(262, 612);
            this.Browse.TabIndex = 0;
            this.Browse.Text = "Navigatie";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblVestiging);
            this.groupBox2.Controls.Add(this.lblAfdeling);
            this.groupBox2.Controls.Add(this.cboAfdeling);
            this.groupBox2.Controls.Add(this.cboVestiging);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(230, 73);
            this.groupBox2.TabIndex = 45;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Database";
            // 
            // lblVestiging
            // 
            this.lblVestiging.AutoSize = true;
            this.lblVestiging.Location = new System.Drawing.Point(6, 16);
            this.lblVestiging.Name = "lblVestiging";
            this.lblVestiging.Size = new System.Drawing.Size(53, 13);
            this.lblVestiging.TabIndex = 34;
            this.lblVestiging.Text = "Vestiging:";
            // 
            // lblAfdeling
            // 
            this.lblAfdeling.AutoSize = true;
            this.lblAfdeling.Location = new System.Drawing.Point(6, 43);
            this.lblAfdeling.Name = "lblAfdeling";
            this.lblAfdeling.Size = new System.Drawing.Size(48, 13);
            this.lblAfdeling.TabIndex = 36;
            this.lblAfdeling.Text = "Afdeling:";
            // 
            // cboAfdeling
            // 
            this.cboAfdeling.FormattingEnabled = true;
            this.cboAfdeling.Location = new System.Drawing.Point(79, 40);
            this.cboAfdeling.Name = "cboAfdeling";
            this.cboAfdeling.Size = new System.Drawing.Size(145, 21);
            this.cboAfdeling.TabIndex = 35;
            this.cboAfdeling.SelectedIndexChanged += new System.EventHandler(this.cboAfdeling_SelectedIndexChanged);
            // 
            // cboVestiging
            // 
            this.cboVestiging.FormattingEnabled = true;
            this.cboVestiging.Location = new System.Drawing.Point(79, 13);
            this.cboVestiging.Name = "cboVestiging";
            this.cboVestiging.Size = new System.Drawing.Size(145, 21);
            this.cboVestiging.TabIndex = 33;
            this.cboVestiging.SelectedIndexChanged += new System.EventHandler(this.cboVestiging_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblMachinegroep);
            this.groupBox1.Controls.Add(this.lboLevel);
            this.groupBox1.Controls.Add(this.lblSubgroep);
            this.groupBox1.Controls.Add(this.lblLevel);
            this.groupBox1.Controls.Add(this.lboSubgroep);
            this.groupBox1.Controls.Add(this.lboMachinegroep);
            this.groupBox1.Location = new System.Drawing.Point(6, 85);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(230, 317);
            this.groupBox1.TabIndex = 50;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Machine";
            // 
            // lblMachinegroep
            // 
            this.lblMachinegroep.AutoSize = true;
            this.lblMachinegroep.Location = new System.Drawing.Point(6, 16);
            this.lblMachinegroep.Name = "lblMachinegroep";
            this.lblMachinegroep.Size = new System.Drawing.Size(39, 13);
            this.lblMachinegroep.TabIndex = 38;
            this.lblMachinegroep.Text = "Groep:";
            // 
            // lboLevel
            // 
            this.lboLevel.FormattingEnabled = true;
            this.lboLevel.Location = new System.Drawing.Point(6, 133);
            this.lboLevel.Name = "lboLevel";
            this.lboLevel.Size = new System.Drawing.Size(218, 82);
            this.lboLevel.TabIndex = 39;
            this.lboLevel.SelectedIndexChanged += new System.EventHandler(this.lboLevel_SelectedIndexChanged);
            // 
            // lblSubgroep
            // 
            this.lblSubgroep.AutoSize = true;
            this.lblSubgroep.Location = new System.Drawing.Point(6, 218);
            this.lblSubgroep.Name = "lblSubgroep";
            this.lblSubgroep.Size = new System.Drawing.Size(56, 13);
            this.lblSubgroep.TabIndex = 44;
            this.lblSubgroep.Text = "Subgroep:";
            // 
            // lblLevel
            // 
            this.lblLevel.AutoSize = true;
            this.lblLevel.Location = new System.Drawing.Point(6, 117);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(36, 13);
            this.lblLevel.TabIndex = 40;
            this.lblLevel.Text = "Level:";
            // 
            // lboSubgroep
            // 
            this.lboSubgroep.FormattingEnabled = true;
            this.lboSubgroep.Location = new System.Drawing.Point(6, 234);
            this.lboSubgroep.Name = "lboSubgroep";
            this.lboSubgroep.Size = new System.Drawing.Size(218, 69);
            this.lboSubgroep.TabIndex = 43;
            this.lboSubgroep.SelectedIndexChanged += new System.EventHandler(this.lboSubgroep_SelectedIndexChanged);
            // 
            // lboMachinegroep
            // 
            this.lboMachinegroep.FormattingEnabled = true;
            this.lboMachinegroep.Location = new System.Drawing.Point(6, 32);
            this.lboMachinegroep.Name = "lboMachinegroep";
            this.lboMachinegroep.Size = new System.Drawing.Size(218, 82);
            this.lboMachinegroep.TabIndex = 37;
            this.lboMachinegroep.SelectedIndexChanged += new System.EventHandler(this.lboMachinegroep_SelectedIndexChanged);
            // 
            // gpbSVG
            // 
            this.gpbSVG.Controls.Add(this.btnUpdateSVG);
            this.gpbSVG.Controls.Add(this.btnDeleteSvg);
            this.gpbSVG.Controls.Add(this.label6);
            this.gpbSVG.Controls.Add(this.btnImportSvg);
            this.gpbSVG.Controls.Add(this.txtSvgFileName);
            this.gpbSVG.Controls.Add(this.btnGenerateSvgs);
            this.gpbSVG.Location = new System.Drawing.Point(6, 408);
            this.gpbSVG.Name = "gpbSVG";
            this.gpbSVG.Size = new System.Drawing.Size(230, 166);
            this.gpbSVG.TabIndex = 48;
            this.gpbSVG.TabStop = false;
            this.gpbSVG.Text = "SVG";
            // 
            // btnUpdateSVG
            // 
            this.btnUpdateSVG.Location = new System.Drawing.Point(6, 74);
            this.btnUpdateSVG.Name = "btnUpdateSVG";
            this.btnUpdateSVG.Size = new System.Drawing.Size(218, 23);
            this.btnUpdateSVG.TabIndex = 51;
            this.btnUpdateSVG.Text = "Update SVG";
            this.btnUpdateSVG.UseVisualStyleBackColor = true;
            this.btnUpdateSVG.Click += new System.EventHandler(this.btnUpdateSVG_Click);
            // 
            // btnDeleteSvg
            // 
            this.btnDeleteSvg.Location = new System.Drawing.Point(6, 103);
            this.btnDeleteSvg.Name = "btnDeleteSvg";
            this.btnDeleteSvg.Size = new System.Drawing.Size(218, 23);
            this.btnDeleteSvg.TabIndex = 50;
            this.btnDeleteSvg.Text = "Verwijder SVG";
            this.btnDeleteSvg.UseVisualStyleBackColor = true;
            this.btnDeleteSvg.Click += new System.EventHandler(this.btnDeleteSvg_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 49;
            this.label6.Text = "Bestand:";
            // 
            // btnImportSvg
            // 
            this.btnImportSvg.Location = new System.Drawing.Point(6, 45);
            this.btnImportSvg.Name = "btnImportSvg";
            this.btnImportSvg.Size = new System.Drawing.Size(218, 23);
            this.btnImportSvg.TabIndex = 46;
            this.btnImportSvg.Text = "Importeer SVG";
            this.btnImportSvg.UseVisualStyleBackColor = true;
            this.btnImportSvg.Click += new System.EventHandler(this.btnImportSvg_Click);
            // 
            // txtSvgFileName
            // 
            this.txtSvgFileName.Location = new System.Drawing.Point(79, 19);
            this.txtSvgFileName.Name = "txtSvgFileName";
            this.txtSvgFileName.ReadOnly = true;
            this.txtSvgFileName.Size = new System.Drawing.Size(145, 20);
            this.txtSvgFileName.TabIndex = 45;
            this.txtSvgFileName.Text = "svgXY_ZZ.svg";
            // 
            // btnGenerateSvgs
            // 
            this.btnGenerateSvgs.Location = new System.Drawing.Point(6, 132);
            this.btnGenerateSvgs.Name = "btnGenerateSvgs";
            this.btnGenerateSvgs.Size = new System.Drawing.Size(218, 23);
            this.btnGenerateSvgs.TabIndex = 47;
            this.btnGenerateSvgs.Text = "Genereer";
            this.btnGenerateSvgs.UseVisualStyleBackColor = true;
            this.btnGenerateSvgs.Click += new System.EventHandler(this.btnGenerateSvgs_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.Browse);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 51);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(270, 638);
            this.tabControl1.TabIndex = 47;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 53;
            this.label3.Text = "Admin:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(76, 27);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(117, 20);
            this.txtUserName.TabIndex = 54;
            // 
            // frmDevelopment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 699);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnSave);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmDevelopment";
            this.Text = "BFV - Admin";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLinkedItems)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.gpbFilter.ResumeLayout(false);
            this.gpbFilter.PerformLayout();
            this.Browse.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gpbSVG.ResumeLayout(false);
            this.gpbSVG.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ContextMenuStrip cmsSvgClick;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnDeleteLink;
        private System.Windows.Forms.DataGridView dgvLinkedItems;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox lboItems;
        private System.Windows.Forms.TextBox txtTrendNr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gpbFilter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboLevel;
        private System.Windows.Forms.ComboBox cboSubgroep;
        private System.Windows.Forms.TabPage Browse;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblVestiging;
        private System.Windows.Forms.Label lblAfdeling;
        private System.Windows.Forms.ComboBox cboAfdeling;
        private System.Windows.Forms.ComboBox cboVestiging;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblMachinegroep;
        private System.Windows.Forms.ListBox lboLevel;
        private System.Windows.Forms.Label lblSubgroep;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.ListBox lboSubgroep;
        private System.Windows.Forms.ListBox lboMachinegroep;
        private System.Windows.Forms.GroupBox gpbSVG;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnImportSvg;
        private System.Windows.Forms.TextBox txtSvgFileName;
        private System.Windows.Forms.Button btnGenerateSvgs;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmAddDatabases;
        private System.Windows.Forms.ToolStripMenuItem tsmAddUsers;
        private System.Windows.Forms.Button btnDeleteSvg;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboTrendGroep;
        private System.Windows.Forms.Button btnUpdateSVG;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboProgressBar;
        private System.Windows.Forms.CheckBox chbOEE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboDatagroep;
    }
}

