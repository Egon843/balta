﻿namespace GUI
{
    partial class frmTrending
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTrending));
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dtpDatum = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPloeg3 = new System.Windows.Forms.Button();
            this.btnPloeg2 = new System.Windows.Forms.Button();
            this.btnPloeg1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOEE = new System.Windows.Forms.TextBox();
            this.btnExportChart = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnDeleteLabels = new System.Windows.Forms.Button();
            this.btnTotNu = new System.Windows.Forms.Button();
            this.btnWeek = new System.Windows.Forms.Button();
            this.btnDag = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(-1, 114);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(777, 368);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            this.chart1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chart1_MouseDown);
            this.chart1.MouseEnter += new System.EventHandler(this.chart1_MouseEnter);
            this.chart1.MouseLeave += new System.EventHandler(this.chart1_MouseLeave);
            // 
            // dtpDatum
            // 
            this.dtpDatum.Location = new System.Drawing.Point(6, 19);
            this.dtpDatum.Name = "dtpDatum";
            this.dtpDatum.Size = new System.Drawing.Size(166, 20);
            this.dtpDatum.TabIndex = 6;
            this.dtpDatum.ValueChanged += new System.EventHandler(this.dtpDatum_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnTotNu);
            this.groupBox1.Controls.Add(this.btnWeek);
            this.groupBox1.Controls.Add(this.btnDag);
            this.groupBox1.Controls.Add(this.btnPloeg3);
            this.groupBox1.Controls.Add(this.btnPloeg2);
            this.groupBox1.Controls.Add(this.btnPloeg1);
            this.groupBox1.Controls.Add(this.dtpDatum);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(753, 45);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data opvragen";
            // 
            // btnPloeg3
            // 
            this.btnPloeg3.Location = new System.Drawing.Point(340, 16);
            this.btnPloeg3.Name = "btnPloeg3";
            this.btnPloeg3.Size = new System.Drawing.Size(75, 23);
            this.btnPloeg3.TabIndex = 9;
            this.btnPloeg3.Text = "21h";
            this.btnPloeg3.UseVisualStyleBackColor = true;
            this.btnPloeg3.Click += new System.EventHandler(this.btnPloeg3_Click);
            // 
            // btnPloeg2
            // 
            this.btnPloeg2.Location = new System.Drawing.Point(259, 16);
            this.btnPloeg2.Name = "btnPloeg2";
            this.btnPloeg2.Size = new System.Drawing.Size(75, 23);
            this.btnPloeg2.TabIndex = 8;
            this.btnPloeg2.Text = "13h";
            this.btnPloeg2.UseVisualStyleBackColor = true;
            this.btnPloeg2.Click += new System.EventHandler(this.btnPloeg2_Click);
            // 
            // btnPloeg1
            // 
            this.btnPloeg1.Location = new System.Drawing.Point(178, 16);
            this.btnPloeg1.Name = "btnPloeg1";
            this.btnPloeg1.Size = new System.Drawing.Size(75, 23);
            this.btnPloeg1.TabIndex = 7;
            this.btnPloeg1.Text = "05h";
            this.btnPloeg1.UseVisualStyleBackColor = true;
            this.btnPloeg1.Click += new System.EventHandler(this.btnPloeg1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "OEE:";
            // 
            // txtOEE
            // 
            this.txtOEE.Location = new System.Drawing.Point(44, 19);
            this.txtOEE.Name = "txtOEE";
            this.txtOEE.Size = new System.Drawing.Size(48, 20);
            this.txtOEE.TabIndex = 10;
            // 
            // btnExportChart
            // 
            this.btnExportChart.Location = new System.Drawing.Point(98, 16);
            this.btnExportChart.Name = "btnExportChart";
            this.btnExportChart.Size = new System.Drawing.Size(96, 23);
            this.btnExportChart.TabIndex = 13;
            this.btnExportChart.Text = "Exporteer trend";
            this.btnExportChart.UseVisualStyleBackColor = true;
            this.btnExportChart.Click += new System.EventHandler(this.btnExportChart_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnExportChart);
            this.groupBox2.Controls.Add(this.txtOEE);
            this.groupBox2.Location = new System.Drawing.Point(134, 63);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 45);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Trend gegevens";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnDeleteLabels);
            this.groupBox3.Location = new System.Drawing.Point(12, 63);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(113, 45);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Labels";
            // 
            // btnDeleteLabels
            // 
            this.btnDeleteLabels.Location = new System.Drawing.Point(6, 16);
            this.btnDeleteLabels.Name = "btnDeleteLabels";
            this.btnDeleteLabels.Size = new System.Drawing.Size(96, 23);
            this.btnDeleteLabels.TabIndex = 13;
            this.btnDeleteLabels.Text = "Verwijder alle";
            this.btnDeleteLabels.UseVisualStyleBackColor = true;
            this.btnDeleteLabels.Click += new System.EventHandler(this.btnDeleteLabels_Click);
            // 
            // btnTotNu
            // 
            this.btnTotNu.Location = new System.Drawing.Point(583, 16);
            this.btnTotNu.Name = "btnTotNu";
            this.btnTotNu.Size = new System.Drawing.Size(75, 23);
            this.btnTotNu.TabIndex = 12;
            this.btnTotNu.Text = "Tot nu";
            this.btnTotNu.UseVisualStyleBackColor = true;
            this.btnTotNu.Click += new System.EventHandler(this.btnTotNu_Click);
            // 
            // btnWeek
            // 
            this.btnWeek.Location = new System.Drawing.Point(502, 16);
            this.btnWeek.Name = "btnWeek";
            this.btnWeek.Size = new System.Drawing.Size(75, 23);
            this.btnWeek.TabIndex = 11;
            this.btnWeek.Text = "Week";
            this.btnWeek.UseVisualStyleBackColor = true;
            this.btnWeek.Click += new System.EventHandler(this.btnWeek_Click);
            // 
            // btnDag
            // 
            this.btnDag.Location = new System.Drawing.Point(421, 16);
            this.btnDag.Name = "btnDag";
            this.btnDag.Size = new System.Drawing.Size(75, 23);
            this.btnDag.TabIndex = 10;
            this.btnDag.Text = "Dag";
            this.btnDag.UseVisualStyleBackColor = true;
            this.btnDag.Click += new System.EventHandler(this.btnDag_Click);
            // 
            // frmTrending
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 482);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chart1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmTrending";
            this.Text = "BFV Trending";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DateTimePicker dtpDatum;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnPloeg3;
        private System.Windows.Forms.Button btnPloeg2;
        private System.Windows.Forms.Button btnPloeg1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtOEE;
        private System.Windows.Forms.Button btnExportChart;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnDeleteLabels;
        private System.Windows.Forms.Button btnTotNu;
        private System.Windows.Forms.Button btnWeek;
        private System.Windows.Forms.Button btnDag;
    }
}