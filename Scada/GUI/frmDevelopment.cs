﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices; //ComVisibleAttribute
using System.IO;
using System.ComponentModel;
using BLL;
using BEC;
using System.Xml.Linq;

namespace GUI
{
    public partial class frmDevelopment : Form
    {
        //!!AANPASSEN!!
        private string _sPathVisio = @"C:\sch00l\2019\Masterproef\SVG bestand laden\SvgFiles";//Pad waar de SVGS uit Visio worden opgeslagen



        //Database keuze
        private string _sVestiging;
        private string _sAfdeling;

        //Tab Navigatie
        private clsMachine _SelectedMachine;
        private clsSvg _oSvg; //= null als machine nog geen SVG heeft

        //Tab SQL Link
        private clsMachine _FilterMachine;
        private clsSvgLink _SelectedItem;
        private clsSvgGroep _SelectedSvgGroup;

        //BLL
        private clsSvgHandler _oSvgHandler = new clsSvgHandler();
        private clsMachineHandler _oMachineHandler = new clsMachineHandler();
        private clsServerHandler _oServerHandler = new clsServerHandler();
        
        //Constructor
        public frmDevelopment(string sUserName)
        {
            InitializeComponent();

            //weergeven van de user
            txtUserName.Text = sUserName;
            txtUserName.Enabled = false;

            //voor gebruik van een dictionary (keyvaluepairs) in de combobox
            cboTrendGroep.DisplayMember = "Value";
            cboTrendGroep.ValueMember = "Key";

            //DatagridView init
            dgvLinkedItems.RowHeadersVisible = false;
            dgvLinkedItems.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dgvLinkedItems.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvLinkedItems.ReadOnly = true;
            dgvLinkedItems.AllowUserToDeleteRows = false;
            dgvLinkedItems.AllowUserToAddRows = false;

            //WebBrowser init
            webBrowser1.ObjectForScripting = new SvgEventHandler(this); //Hier staan de methodes die vanuit de SVG worden opgeroepen
            webBrowser1.Navigate("about:blank");

            //klikken niet mogelijk bij navigatie
            cmsSvgClick.Enabled = false;

            //De mogelijke vestigingen weergeven
            cboVestiging.DataSource = _oServerHandler.GetServers();

            //Svg bewerkingen zijn pas mogelijk na het selecteren van een machine
            btnImportSvg.Enabled = false;
            btnDeleteSvg.Enabled = false;
            btnUpdateSVG.Enabled = false;
            btnGenerateSvgs.Enabled = false;
        }
        
        //Weergave van de webbrowser updaten
        private void _UpdateWebBrowser()
        {
            string sSVG;

            if (_SelectedMachine == null)
            {
                sSVG = "No machine selected.";
            }
            else if (_oSvg == null)
            {
                sSVG = "This machine has no SVG-file.";
            }
            else
            {//inladen van de SVG-figuur
                sSVG = _oSvg.GetXmlString();
            }

            webBrowser1.Document.OpenNew(true);
            webBrowser1.Document.Write(sSVG);
            webBrowser1.Refresh();
        }
                    
        //Wijzigen servers
        private void tsmAddDatabases_Click(object sender, EventArgs e)
        {
            frmChangeServers _oFormNav = new frmChangeServers();
            _oFormNav.Show();
        }

        //Wijzigen gebruikers
        private void tsmAddUsers_Click(object sender, EventArgs e)
        {
            frmChangeUsers oFormChangeUser = new frmChangeUsers();
            oFormChangeUser.Show();
        }


        //Vangt de click-event op uit de WebBrowser (SVG)
        #region SVG_Click

        [ComVisible(true)] //COM interface zichtbaar maken
        public class SvgEventHandler //De klasse waarvan het COM zichtbaar wordt
        {
            private frmDevelopment frmMain;

            //constructor
            public SvgEventHandler(frmDevelopment MainForm) 
            {
                frmMain = MainForm; //De form waarin de webBrowser control zich bevind
            }

            //Methode die door het SVG-event wordt aangeroepen
            public void SvgGroup_Click(string mID) //mID wordt meegegeven vanuit de SVG zelf
            {
                frmMain.OpenGroupOptions(mID);
            }
        }

        //Deze methode wordt aangeroepen vanuit de klasse 'SvgEventHandler'
        //en bepaalt welke opties mogelijk zijn bij het klikken op de svg-tekening
        private void OpenGroupOptions(string mID)
        {
            if (cmsSvgClick.Enabled)
            {
                //'Group' selecteren
                _SelectedSvgGroup = _oSvg.SvgGroups.Find(oGr => oGr.mID == mID);

                //SVG-group aanduiden
                _oSvg.GlowGroup(mID);
                _UpdateWebBrowser();

                //ContextMenuStrip opbouwen
                cmsSvgClick.Items.Clear();
                cmsSvgClick.Items.Insert(0, new ToolStripLabel("mID: " + mID));
                cmsSvgClick.Items.Insert(1, new ToolStripSeparator());
                ToolStripItem tsiInfo = cmsSvgClick.Items.Add("Info");

                //EventHandlers toevoegen
                tsiInfo.Click += TsiInfo_Click;

                //Is de SVG-group gelinkt aan data?
                if (_SelectedSvgGroup.TrendTag.Count() > 5)//geen link => '-1'
                {
                    //gelinkte data selecteren
                    _SelectLink(mID);

                    //Optie 'remove' toevoegen
                    ToolStripItem tsiRemoveLink = cmsSvgClick.Items.Add("Remove link");
                    tsiRemoveLink.Click += TsiRemoveLink_Click;
                }
                else
                {
                    //geen enkele link selecteren
                    dgvLinkedItems.ClearSelection();

                    //optie 'link' toevoegen
                    ToolStripItem tsiLink = cmsSvgClick.Items.Add("Link");
                    tsiLink.Click += TsiLink_Click;
                }

                //ContextMenuStrip weergeven
                Point MousePosition = PointToClient(System.Windows.Forms.Control.MousePosition);
                cmsSvgClick.Show(this, MousePosition);

                _UpdateCboPB();
                _UpdateOEESettings();//Update checkbox OEE     
            }
            
        }

        //Deze methode zal worden gebruikt voor het toevoegen van informatie
        private void TsiInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Dit is de info die hoort bij: \n mID = " + _SelectedSvgGroup.mID);
        }

        // De link maken tussen het geselecteerde item en de geselecteerde SVG-group
        private void TsiLink_Click(object sender, EventArgs e)
        {
            if (_SelectedItem != null)
            {
                string sTrendNr = _SelectedItem.TrendNr;
                string sTagName = _SelectedItem.Name;

                //De link toevoegen
                _oMachineHandler.AddLink(_oSvg, _SelectedSvgGroup.mID, sTrendNr, sTagName);

                //Controleren OEE
                if (sTrendNr.Count() == 7 & sTrendNr.Substring(0,1) == "4")
                {
                    _SelectedSvgGroup.MakeOEE();
                }

                //SVG opslaan na aanpassingen
                _oSvgHandler.SaveSvgFile(_oSvg, _sVestiging, _sAfdeling);

                //De laatst toegevoegde groep 'selecteren'
                string mID = _SelectedSvgGroup.mID;

                //De gelinkte items updaten
                _UpdateLinkedItems();
                
                                
                _SelectLink(mID);
                _SelectedSvgGroup.Glow();
                _UpdateWebBrowser();

            }
            else
            {
                MessageBox.Show("Geen item geselecteerd.");
            }           
        }

        // De link verwijderen
        private void TsiRemoveLink_Click(object sender, EventArgs e)
        {
            string sTrendNr = _SelectedSvgGroup.TrendTag;

            //controle OEE
            if (sTrendNr.Count() == 7 & sTrendNr.Substring(0, 1) == "4")
            {
                _SelectedSvgGroup.RemoveOEE();
            }

            List<DataGridViewRow> lstRowsToDelete = new List<DataGridViewRow>();
            lstRowsToDelete.Add(dgvLinkedItems.SelectedRows[0]);
            _DeleteLinkedItems(lstRowsToDelete);
        }

        //Voor het selecteren van de gelinkte data bij het aanklikken van een SVG-group
        private void _SelectLink(string mID)
        {           
                    //Huidige selectie verwijderen
                    dgvLinkedItems.ClearSelection();

                    //De juiste link in de dgv zoeken
                    DataGridViewRow row = dgvLinkedItems.Rows
                        .Cast<DataGridViewRow>()
                        .Where(r => r.Cells["mID"].Value.ToString().Equals(mID))
                        .First();

                    //De link selecteren en weergeven
                    int iIndex = row.Index;
                    dgvLinkedItems.Rows[iIndex].Selected = true;
                    dgvLinkedItems.FirstDisplayedScrollingRowIndex = iIndex;    
            
                
        }

        #endregion


        //TabControl
        //########################################################### 

        //Laden van gegevens bij het veranderen van tabvenster
        //!!!Indexen aanpassen als Tabs verplaatst worden
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Tab Navigatie = 0
            if (tabControl1.SelectedIndex == 0)
            {
                cmsSvgClick.Enabled = false;//Klikken op SVG niet mogelijk
            }

            //Tab SQL Links = 1
            else if (tabControl1.SelectedIndex == 1)
            {
                cmsSvgClick.Enabled = true;//Klikken wel mogelijk

                //Opties niet beschikbaar
                cboProgressBar.Enabled = false;
                cboProgressBar.SelectedIndex = 0;

                //als de gekozen machine een subgroep is
                if (_SelectedMachine.SubgroupNr > 0)
                {
                    _FilterMachine = _SelectedMachine;
                }

                //procesvariabelen van de geselecteerde machine weergeven (zonder TrendGroep => 0)
                //_UpdateItems(_SelectedMachine, 0);
                //gebeurt na selectie, anders te traag

                _UpdateFilters(); //filters instellen
                lboItems.DataSource = null;
                _UpdateLinkedItems(); //Gelinkte items weergeven
            }           
        }

        //Events en methodes van het navigatie tabblad
        #region Tabblad Navigatie

        //Vestiging selecteren       
        private void cboVestiging_SelectedIndexChanged(object sender, EventArgs e)
        {
            _sVestiging = cboVestiging.SelectedItem.ToString();
            
            //de mogelijke afdelingen weergeven
            cboAfdeling.DataSource = _oServerHandler.GetDatabases(_sVestiging);

            //Svg bewerkingen zijn pas mogelijk na het selecteren van een machine
            btnImportSvg.Enabled = false;
            btnDeleteSvg.Enabled = false;
            btnUpdateSVG.Enabled = false;
        }     
     
        //Afdeling selecteren
        private void cboAfdeling_SelectedIndexChanged(object sender, EventArgs e)
        {         
            _sAfdeling = cboAfdeling.SelectedItem.ToString();

            //connectie maken met de gekozen database
            string sConnStr = _oServerHandler.GetConnectionString(_sVestiging, _sAfdeling);
            _oMachineHandler.SetDatabase(sConnStr);
            

            //De mogelijke groepen weergeven
            try
            {                   
                List<clsMachine> lstGroups = _oMachineHandler.GetGroups();

                //Bij het weergeven van de groepen nog geen selectie maken
                lboMachinegroep.SelectionMode = SelectionMode.None;
                lboMachinegroep.DataSource = new BindingSource(lstGroups, null);
                lboMachinegroep.SelectionMode = SelectionMode.One;

                //Na het selecteren van de afdeling moet eerst een 'Groep' worden gekozen
                lboLevel.DataSource = null;
                lboSubgroep.DataSource = null;
            }
            //Als de mogelijke groepen niet kunnen geladen worden is er geen verbinding met de database
            catch (Exception)
            {
                MessageBox.Show("Kan geen verbinding maken met de gekozen database.");
            }

            //Svg bewerkingen zijn pas mogelijk na het selecteren van een machine
            btnImportSvg.Enabled = false;
            btnDeleteSvg.Enabled = false;
            btnUpdateSVG.Enabled = false;
        }
        
        //Groep selecteren
        private void lboMachinegroep_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lboMachinegroep.SelectedIndex > -1)
            {
                //Selecteren van de machine
                clsMachine oGroup = lboMachinegroep.SelectedItem as clsMachine;
                _SelectMachine(oGroup);

                //De mogelijke levels weergeven zonder een level te selecteren
                List<clsMachine> lstLevels = _oMachineHandler.GetLevels(oGroup.GroupNr);                
                lboLevel.SelectionMode = SelectionMode.None;
                lboLevel.DataSource = new BindingSource(lstLevels, null);
                lboLevel.SelectionMode = SelectionMode.One;

                //Voor het weergeven van de Subgroepen moet eerst een Level worden gekozen
                lboSubgroep.DataSource = null;
                lboSubgroep.Text = "";               
            }
        }

        //Level selecteren
        private void lboLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lboLevel.SelectedIndex > -1)
            {
                //Selecteren van de machine
                clsMachine oLevel = lboLevel.SelectedItem as clsMachine;
                _SelectMachine(oLevel);

                //De mogelijke Subgroepen weergeven zonder selectie
                List<clsMachine> lstSubGroepen = _oMachineHandler.GetSubgroups(oLevel.GroupNr, oLevel.LevelNr);
                lboSubgroep.SelectionMode = SelectionMode.None;
                lboSubgroep.DataSource = new BindingSource(lstSubGroepen, null);
                lboSubgroep.SelectionMode = SelectionMode.One;
            }
        }

        //Subgroep selecteren
        private void lboSubgroep_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lboSubgroep.SelectedIndex > -1)
            {
                //Selecteren van de machine
                clsMachine oSubgroup = (clsMachine)lboSubgroep.SelectedItem;
                _SelectMachine(oSubgroup);
            }
        }

        //Methode voor het selecteren van een machine
        private void _SelectMachine(clsMachine oMachine)
        {
            _SelectedMachine = oMachine;
            _FilterMachine = null;

            //De SVG-bestandsnaam opvragen en weergeven
            string sSvgFileName = oMachine.GenerateSvgFileName();
            txtSvgFileName.Text = sSvgFileName;

            //Controleren of de SVG reeds geconfigureerd is
            //_oSvg blijft 'null' als de SVG nog niet bestaat
            _oSvg = _oSvgHandler.LoadExistingFile(sSvgFileName, _sVestiging, _sAfdeling);
            _UpdateWebBrowser();

            //Svg bewerkingen zijn pas mogelijk na het selecteren van een machine
            if (_oSvg == null)
            {
                btnImportSvg.Enabled = true;
                btnUpdateSVG.Enabled = false;
                btnDeleteSvg.Enabled = false;
                btnGenerateSvgs.Enabled = false;
            }
            else
            {
                btnImportSvg.Enabled = false;
                btnUpdateSVG.Enabled = true;
                btnDeleteSvg.Enabled = true;

                //subgroepen kunnnen gegenereerd worden
                if (oMachine.SubgroupNr > 0)
                {
                    btnGenerateSvgs.Enabled = true;
                }
                else
                {
                    btnGenerateSvgs.Enabled = false;
                }

            }                        
        }
        
        //Een nieuw SVG-bestand toewijzen aan de machine
        private void btnImportSvg_Click(object sender, EventArgs e)
        {
            if (_SelectedMachine != null)
            {   
                //FileDialog voor het kiezen van een SVG-bestand
                OpenFileDialog FilePicker = new OpenFileDialog
                {
                    InitialDirectory = _sPathVisio,
                    Filter = "SVG files | *.svg",
                    Title = "Please select an SVG file to load"
                };
    
                //Bestand toewijzen
                if (FilePicker.ShowDialog() == DialogResult.OK)
                {
                    string sPath = FilePicker.FileName;
                    string sSvgName = _SelectedMachine.GenerateSvgFileName();
    
                    //clsSvg maken op basis van bestand en SvgNaam
                    _oSvg = _oSvgHandler.LoadNewFile(sSvgName, sPath);

                    //geconverteerd bestand opslaan in de juiste map
                    _oSvgHandler.SaveSvgFile(_oSvg, _sVestiging, _sAfdeling);  

                    //nieuw SVG-bestand weergeven         
                    _UpdateWebBrowser();

                    btnDeleteSvg.Enabled = true;
                    btnUpdateSVG.Enabled = true;
                    btnImportSvg.Enabled = false;
                }
            }
        }

        //Het huidge SVG-bestand vervangen en de bestaande links behouden.
        private void btnUpdateSVG_Click(object sender, EventArgs e)
        {            
            if (_SelectedMachine != null & _oSvg != null)
            {
                //FileDialog voor het kiezen van een SVG-bestand
                OpenFileDialog FilePicker = new OpenFileDialog
                {
                    InitialDirectory = _sPathVisio,
                    Filter = "SVG files | *.svg",
                    Title = "Please select an SVG file to load"
                };

                //Bestand vervangen
                if (FilePicker.ShowDialog() == DialogResult.OK)
                {
                    //De oude SVG-figuur verwijderen uit de map
                    _oSvgHandler.RemoveSvgFile(_oSvg, _sVestiging, _sAfdeling);

                    string sPath = FilePicker.FileName;
                    string sSvgName = _SelectedMachine.GenerateSvgFileName();

                    //Oude SVG bijhouden voor info
                    clsSvg oOldSvg = _oSvg;

                    //clsSvg maken op basis van bestand en SvgNaam
                    _oSvg = _oSvgHandler.LoadNewFile(sSvgName, sPath);

                    //Oude links toevoegen aan nieuwe SVG                    
                    List<clsSvgLink> lstLinkedItems = _oMachineHandler.GetLinkedItems(_SelectedMachine, _oSvg);                    
                    foreach (clsSvgLink oLink in lstLinkedItems)
                    {
                        //Controleren of de svgGroup niet is verwijderd in de nieuwe SVG                        
                        if (_oSvg.SvgGroups.Any(svgGr => svgGr.mID == oLink.mID))
                        {
                            //De link opnieuw toevoegen aan de SVG
                            _oMachineHandler.AddLinkToSvg(_oSvg, oLink.mID, oLink.TrendNr, oLink.Name);

                            //controle OEE of PB van oude SVG
                            clsSvgGroep oOldGroup = oOldSvg.GetSvgGroup(oLink.mID);
                            if (oOldGroup.IsProgressBar())
                            {
                                string sDir = oOldGroup.ProgressBarDirection();
                                _oSvg.GetSvgGroup(oLink.mID).MakeProgressBar(sDir);
                            }                           
                            else if (oOldGroup.IsOEE())
                            {
                                _oSvg.GetSvgGroup(oLink.mID).MakeOEE();
                            }
                        }
                        else
                        {
                            //De link verwijderen uit de DB
                            _oMachineHandler.RemoveLinkFromDb(_oSvg, oLink.mID);
                        }
                    }

                    //geconverteerd bestand opslaan in de juiste map
                    _oSvgHandler.SaveSvgFile(_oSvg, _sVestiging, _sAfdeling);

                    //nieuw SVG-bestand weergeven         
                    _UpdateWebBrowser();
                }
            }

        }

        //Het huidige SVG-bestand en zijn links in de database verwijderen
        private void btnDeleteSvg_Click(object sender, EventArgs e)
        {
            //!!!Verwijderen van gegevens uit de database
            //Bevestiging vragen aan de gebruiker
            //string sMessage = "De SVG en de bijhorende links in de database verwijderen?";
            //DialogResult oDelete = MessageBox.Show( sMessage, "Confirm Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            //if (oDelete == DialogResult.OK)
            //{
                //ALLE gelinkte items verwijderen
                List<DataGridViewRow> oRowsToDelete = new List<DataGridViewRow>();
                foreach (DataGridViewRow row in dgvLinkedItems.Rows)
                {
                    oRowsToDelete.Add(row);
                }
                _DeleteLinkedItems(oRowsToDelete);

                //De gekoppede SVG-figuur verwijderen uit de map
                _oSvgHandler.RemoveSvgFile(_oSvg, _sVestiging, _sAfdeling);

                //De SVG uit het geheugen verwijderen
                _oSvg = null;
                _UpdateWebBrowser();

                btnDeleteSvg.Enabled = false;
                btnUpdateSVG.Enabled = false;
                btnImportSvg.Enabled = true;
            //}
        }

        //ANDERE FORM 'Generate' ???? 
        //indien meer info of mogelijkheden gewenst.
        private void btnGenerateSvgs_Click(object sender, EventArgs e)
        {            

            //Lijst opstellen van te genereren machines
            List<clsMachine> lstToGenerate = new List<clsMachine>();
            string sMessage = "Volgende subgroepen werden gegenereerd: ";

            foreach (clsMachine machine in lboSubgroep.Items)
            {
                if (machine == _SelectedMachine)
                {
                    //Geselecteerde machine niet genereren
                }
                else
                {
                    //controleren of de SVG al bestaat
                    string sFile = machine.GenerateSvgFileName();                    
                    if (_oSvgHandler.LoadExistingFile(sFile, _sVestiging, _sAfdeling) != null)
                    {
                       //niet genereren
                    }
                    else
                    {
                        //wel genereren
                        lstToGenerate.Add(machine);
                        sMessage = sMessage + machine.SubgroupNr + ", ";
                    }
                }
            }
            
            //De SVGS genereren van de lijst machines
            List<clsSvg> lstGenerated = _oMachineHandler.GenerateSubgroup(_oSvg, _SelectedMachine, lstToGenerate);

            //De SVGS opslaan
            foreach (clsSvg oSvg in lstGenerated)
            {
                _oSvgHandler.SaveSvgFile(oSvg, _sVestiging, _sAfdeling);
            }



            MessageBox.Show(sMessage);

        }

        #endregion

        //Events en methoden van het SqlLink tabblad
        #region Tabblad SqlLink
            
        //Weergave Items
        //##############################################################

        //De procesvariabelen weergeven van de gekozen filter
        private void _UpdateItems(clsMachine oMachine, int iTrendGroep)
        {
            //Items uit de database halen
            List<clsSvgLink> lstItems = _oMachineHandler.GetItems(oMachine.GroupNr, oMachine.LevelNr, oMachine.SubgroupNr, iTrendGroep);

            //data toekennen zonder selectie
            lboItems.SelectionMode = SelectionMode.None;
            lboItems.DataSource = new BindingSource(lstItems, null);
            lboItems.SelectionMode = SelectionMode.One;

            _SelectedItem = null;
        }

        //De gelinkte Items weergeven van de geselecteerde machine
        private void _UpdateLinkedItems()
        {
            try
            {
                //Links uit de database halen
                List<clsSvgLink> lstLinkedItems = _oMachineHandler.GetLinkedItems(_SelectedMachine, _oSvg);

                //Links weergeven
                var blLinkedItems = new BindingList<clsSvgLink>(lstLinkedItems);
                dgvLinkedItems.DataSource = blLinkedItems;
            }
            catch (Exception)
            {
                MessageBox.Show("De gelinkte items konden niet opgevraagd worden uit de database.");
            }
        }               

        //Verwijderen van gelinkte items
        private void _DeleteLinkedItems(List<DataGridViewRow> oLinkedItemsToRemove)
        {
            foreach (DataGridViewRow row in oLinkedItemsToRemove)
            {
                //De mID uit de datarow halen
                string mID = row.Cells["mID"].Value.ToString();
                //De link verwijderen
                _oMachineHandler.RemoveLink(_oSvg, mID);
            }

            //opslaan na verandering
            _oSvgHandler.SaveSvgFile(_oSvg, _sVestiging, _sAfdeling);
            _UpdateLinkedItems();
            _UpdateWebBrowser();
        }

        //Selecteren van een item
        private void lboItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lboItems.SelectedIndex > -1)
            {
                _SelectedItem = (clsSvgLink)lboItems.SelectedItem;
                txtTrendNr.Text = _SelectedItem.TrendNr;
            }
            
        }

        //Selecteren van een gelinkte item
        private void dgvLinkedItems_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                string mID = dgvLinkedItems.SelectedRows[0].Cells[2].Value.ToString();
                _SelectedSvgGroup = _oSvg.SvgGroups.Find(oGr => oGr.mID == mID);//nodig voor het instellen van PB of OEE
                _UpdateCboPB();//update checkbox PB
                _UpdateOEESettings();//Update checkbox OEE

                _oSvg.GlowGroup(mID);//Het gelinkte SVG-element markeren
                _UpdateWebBrowser();//Update SVG

                
            }
            catch (Exception)
            { }
        }

        //De GESELECTEERDE gelinkte items verwijderen
        private void btnDeleteLink_Click(object sender, EventArgs e)
        {
            List<DataGridViewRow> oRowsToDelete = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dgvLinkedItems.SelectedRows)
            {
                oRowsToDelete.Add(row);
            }
            _DeleteLinkedItems(oRowsToDelete);

            _UpdateCboPB();
            _UpdateOEESettings();//Update checkbox OEE
        }

        //De checkbox enablen en aanvinken indien van toepassing
        private void _UpdateOEESettings()
        {
            try
            {
                if (_SelectedSvgGroup.IsText() | _SelectedSvgGroup.IsRectangle())
                {
                    chbOEE.Enabled = true;

                    if (_SelectedSvgGroup.IsOEE())
                    {
                        chbOEE.Checked = true;
                    }
                    else
                    {
                        chbOEE.Checked = false;
                    }
                }
                else
                {
                    chbOEE.Enabled = false;
                    chbOEE.Checked = false;
                }
            }
            catch (Exception)
            {
            }
        }

        //Filters
        //##############################################################

        //Filter mogeljkheden instellen op basis van geselecteerde machine
        private void _UpdateFilters()
        {
            //Indien nog geen machine geselecteerd is kunnen geen items worden weergeven
            if (_SelectedMachine != null)
            {              

                //Als geselecteerde machine een subgroep voorstelt => geen filter mogelijk
                if (_SelectedMachine.SubgroupNr > 0)
                {
                    //Level filter
                    cboLevel.Enabled = false;
                    cboLevel.Text = lboLevel.GetItemText(lboLevel.SelectedItem); //geselecteerde level weergeven

                    //Subgroep filter
                    cboSubgroep.Enabled = false;
                    cboSubgroep.Text = lboSubgroep.GetItemText(lboSubgroep.SelectedItem); //geselecteerde subgroep weergeven

                    //Mogelijke trendgroepen ophalen                    
                    Dictionary<int, string> lstFilterTrendgroups = _oMachineHandler.GetTrendgroups(_SelectedMachine);
                    cboTrendGroep.DataSource = new BindingSource(lstFilterTrendgroups, null);

                    //Datagroep selecteren
                    cboDatagroep.SelectedIndex = -1;

                }
                //Als een level geselecteerd is kan de subgroep filter worden gebruikt
                else if (_SelectedMachine.LevelNr > 0)
                {
                    //Level filter
                    cboLevel.Enabled = false;
                    cboLevel.Text = lboLevel.GetItemText(lboLevel.SelectedItem); //geselecteerde level weergeven

                    //Mogelijke Subgroepen instellen
                    cboSubgroep.Enabled = true;
                    var blFilterSubgroups = new BindingList<clsMachine>();
                    foreach (clsMachine oSubgroup in lboSubgroep.Items)
                    {
                        blFilterSubgroups.Add(oSubgroup);
                    }
                    cboSubgroep.DataSource = blFilterSubgroups;
                    cboSubgroep.Text = "";

                    //Mogelijke trendgroepen verwijderen
                    cboTrendGroep.DataSource = null;
                    cboTrendGroep.Text = "";

                    //Datagroep selecteren
                    cboDatagroep.SelectedIndex = -1;
                }
                //Als een groep geselecteerd is kan de level en subgroep filter worden gebruikt
                else
                {
                    //Mogelijke levels instellen
                    cboLevel.Enabled = true;
                    var blFilterLevels = new BindingList<clsMachine>();

                    foreach (clsMachine oLevel in lboLevel.Items)
                    {
                        blFilterLevels.Add(oLevel);
                    }
                    cboLevel.DataSource = blFilterLevels;

                    //Mogelijke Subgroepen verwijderen
                    cboSubgroep.Enabled = true;
                    cboSubgroep.DataSource = null;
                    cboSubgroep.Text = "";

                    //Mogelijke trendgroepen verwijderen
                    cboTrendGroep.DataSource = null;
                    cboTrendGroep.Text = "";

                    //Datagroep selecteren
                    cboDatagroep.SelectedIndex = -1;
                }
            }
        }

        //Level filter selectie
        private void cboLevel_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //Filter Subgroepen instellen
            _FilterMachine = cboLevel.SelectedItem as clsMachine;
            List<clsMachine> lstFilterSubgroups = _oMachineHandler.GetSubgroups(_FilterMachine.GroupNr, _FilterMachine.LevelNr);

            //De mogelijkheid toevoegen om alle subgroepen te weergeven
            lstFilterSubgroups.Insert(0, (new clsMachine("Alles*", _FilterMachine.GroupNr, _FilterMachine.LevelNr, 0, 0, 0)));

            var blFilterSubgroups = new BindingList<clsMachine>(lstFilterSubgroups);
            cboSubgroep.DataSource = blFilterSubgroups;

            //Mogelijke trendgroepen verwijderen
            cboTrendGroep.DataSource = null;
            cboTrendGroep.Text = "";

            //mogelijke Items weergeven
            _UpdateItems(_FilterMachine, 0);

            cboDatagroep_SelectionChangeCommitted(sender, new EventArgs());
        }

        //Subgroep filter selectie
        private void cboSubgroep_SelectionChangeCommitted(object sender, EventArgs e)
        {
            _FilterMachine = cboSubgroep.SelectedItem as clsMachine;
            //###############################################################################"in clsmachine
            //omachine.gettrendgroups

            //Moggelijke trendgroepen ophalen
            Dictionary<int,string> lstFilterTrendgroups = _oMachineHandler.GetTrendgroups(_FilterMachine);
            cboTrendGroep.DataSource = new BindingSource(lstFilterTrendgroups, null);

            //mogelijke items van de subgroep weergeven
            _UpdateItems(_FilterMachine, 0);

            cboDatagroep_SelectionChangeCommitted(sender, new EventArgs());
        }

        //Trendgroep filter selectie
        private void cboTrendGroep_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //mogelijke items van de subgroep weergeven
            int iTrendgroup = int.Parse(cboTrendGroep.SelectedValue.ToString());

            _UpdateItems(_FilterMachine, iTrendgroup);

            cboDatagroep_SelectionChangeCommitted(sender, new EventArgs());
        }

        private void cboDatagroep_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (_FilterMachine == null)
            {
                return;
            }
            if (cboDatagroep.SelectedIndex == -1)
            {
                return;
            }

            string sDatagroep = cboDatagroep.GetItemText(cboDatagroep.SelectedItem);
            int iTrendgroup;
            try
            {
                iTrendgroup = int.Parse(cboTrendGroep.SelectedValue.ToString());
            }
            catch (Exception)
            {
                iTrendgroup = 0;
            }

            List<clsSvgLink> lstItems = _oMachineHandler.GetItems(_FilterMachine.GroupNr, _FilterMachine.LevelNr, _FilterMachine.SubgroupNr, iTrendgroup);
            List<clsSvgLink> lstFilteredItems = new List<clsSvgLink>();

            switch(sDatagroep)
            {
                case "PV":
                    foreach (clsSvgLink oItem in lstItems)
                    {
                        if (oItem.TrendNr.Count() == 6)
                        {
                           string sLast3 = oItem.TrendNr.Substring(3);
                           int iLast3 = int.Parse(sLast3);
                            if (iLast3 < 200)
                            {
                                lstFilteredItems.Add(oItem);
                            }
                        }
                    }
                    break;
                case "SP":
                    foreach (clsSvgLink oItem in lstItems)
                    {
                        if (oItem.TrendNr.Count() == 6)
                        {
                            string sLast3 = oItem.TrendNr.Substring(3);
                            int iLast3 = int.Parse(sLast3);
                            if (iLast3 > 199 & iLast3 < 400)
                            {
                                lstFilteredItems.Add(oItem);
                            }
                        }
                    }
                    break;
                case "OUT":
                    foreach (clsSvgLink oItem in lstItems)
                    {
                        if (oItem.TrendNr.Count() == 6)
                        {
                            string sLast3 = oItem.TrendNr.Substring(3);
                            int iLast3 = int.Parse(sLast3);
                            if (iLast3 > 399 & iLast3 < 600)
                            {
                                lstFilteredItems.Add(oItem);
                            }
                        }
                    }
                    break;
                case "MAX":
                    foreach (clsSvgLink oItem in lstItems)
                    {
                        if (oItem.TrendNr.Count() == 6)
                        {
                            string sLast3 = oItem.TrendNr.Substring(3);
                            int iLast3 = int.Parse(sLast3);
                            if (iLast3 > 599 & iLast3 < 800)
                            {
                                lstFilteredItems.Add(oItem);
                            }
                        }
                    }
                    break;
                case "MIN":
                    foreach (clsSvgLink oItem in lstItems)
                    {
                        if (oItem.TrendNr.Count() == 6)
                        {
                            string sLast3 = oItem.TrendNr.Substring(3);
                            int iLast3 = int.Parse(sLast3);
                            if (iLast3 > 799 & iLast3 < 1000)
                            {
                                lstFilteredItems.Add(oItem);
                            }
                        }
                    }
                    break;
                case "AVG":
                    foreach (clsSvgLink oItem in lstItems)
                    {
                        if (oItem.TrendNr.Count() == 7 & oItem.TrendNr.Substring(0, 1) == "1")
                        {                           
                                lstFilteredItems.Add(oItem);                          
                        }
                    }
                    break;
                case "ED":
                    foreach (clsSvgLink oItem in lstItems)
                    {
                        if (oItem.TrendNr.Count() == 7 & oItem.TrendNr.Substring(0, 1) == "2")
                        {
                            lstFilteredItems.Add(oItem);
                        }
                    }
                    break;
                case "CNT":
                    foreach (clsSvgLink oItem in lstItems)
                    {
                        if (oItem.TrendNr.Count() == 7 & oItem.TrendNr.Substring(0, 1) == "3")
                        {
                            lstFilteredItems.Add(oItem);
                        }
                    }
                    break;
                case "OEE":
                    foreach (clsSvgLink oItem in lstItems)
                    {
                        if (oItem.TrendNr.Count() == 7 & oItem.TrendNr.Substring(0, 1) == "4")
                        {
                            lstFilteredItems.Add(oItem);
                        }
                    }
                    break;
                default:
                    MessageBox.Show(sDatagroep + " not implemented");
                    break;
            }
            //data toekennen zonder selectie
            lboItems.SelectionMode = SelectionMode.None;
            lboItems.DataSource = new BindingSource(lstFilteredItems, null);
            lboItems.SelectionMode = SelectionMode.One;
        }


        //OEE & Progressbar
        //#####################################################################

        //De checkbox enablen en aanvinken indien van toepassing
        private void _UpdateCboPB()
        {
            try
            {
                if (_SelectedSvgGroup.IsRectangle())
                {
                    cboProgressBar.Enabled = true;

                    if (_SelectedSvgGroup.IsProgressBar())
                    {
                        string sDir = _SelectedSvgGroup.ProgressBarDirection();
                        cboProgressBar.SelectedItem = sDir;
                    }
                    else
                    {
                        cboProgressBar.SelectedItem = "None";
                    }
                }
                else
                {
                    cboProgressBar.Enabled = false;
                    cboProgressBar.SelectedItem = "None";
                }
            }
            catch (Exception)
            {
                cboProgressBar.Enabled = false;
                cboProgressBar.SelectedItem = "None";
            }
        }

        private void cboProgressBar_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //PB verwijderen
            if (cboProgressBar.GetItemText(cboProgressBar.SelectedItem) == "None")
            {
                _SelectedSvgGroup.RemoveProgressBar();
            }
            else
            {
                string sDir = cboProgressBar.GetItemText(cboProgressBar.SelectedItem);
                _SelectedSvgGroup.MakeProgressBar(sDir);
            }
            
            //opslaan na aanpassingen
            _oSvgHandler.SaveSvgFile(_oSvg, _sVestiging, _sAfdeling);

            _UpdateWebBrowser();
        }






        #endregion

        #region TEST dynamisch wijzigen browser zonder refresh()

        //HtmlElementCollection oEls = webBrowser1.Document.GetElementsByTagName("g");  //GetElementById("211004");

        //foreach (HtmlElement oEl in oEls)
        //{
        //    if (oEl.GetAttribute("mID") == "1")
        //    {
        //        string sValue = oEl.GetAttribute("id");

        //        HtmlElement oRect = oEl.GetElementsByTagName("rect")[0];

        //        //https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.htmlelement.setattribute?redirectedfrom=MSDN&view=netframework-4.8#System_Windows_Forms_HtmlElement_SetAttribute_System_String_System_String_
        //        oRect.SetAttribute("classname", "st4");

        //        //https://stackoverflow.com/questions/9707869/system-comobject-is-returned-when-i-use-getattribute



        //        frmTest frm = new frmTest(oEl.OuterHtml);
        //        frm.Show();
        //    }


        //    //webBrowser1.Refresh();

        //    //_UpdateWebBrowser();
        //}

        //HtmlElement oRect = oEl.GetElementsByTagName("rect")[5];
        //string sTest = oRect.GetAttribute("style");
        //oRect.SetAttribute("style", "fill:#00ff00;");
        //SetAttribute("style", "fill:#00ff00;");

        #endregion

        private void chbOEE_CheckedChanged(object sender, EventArgs e)
        {
            if (chbOEE.Checked)
            {
                _SelectedSvgGroup.MakeOEE();

                ////QLIK

                //string sTrendNr = dgvLinkedItems.SelectedRows[0].Cells["TrendNr"].Value.ToString();
                //string sTagName = dgvLinkedItems.SelectedRows[0].Cells["Name"].Value.ToString();

                //sTrendNr = "4" + sTrendNr;
                //sTagName = "OEE_" + sTagName;

                //_oMachineHandler.RemoveLink(_oSvg, _SelectedSvgGroup.mID);
                //_oMachineHandler.AddLink(_oSvg, _SelectedSvgGroup.mID, sTrendNr, sTagName);
                //_oSvgHandler.SaveSvgFile(_oSvg, _sVestiging, _sAfdeling);

            }
            else
            {
                _SelectedSvgGroup.RemoveOEE();

                ////QLIK

                //string sTrendNr = dgvLinkedItems.SelectedRows[0].Cells["TrendNr"].Value.ToString();
                //string sTagName = dgvLinkedItems.SelectedRows[0].Cells["Name"].Value.ToString();

                //sTrendNr = sTrendNr.Substring(1,6);
                //sTagName = "OEE_" + sTagName.Substring(4, sTagName.Length -4);

                //_oMachineHandler.RemoveLink(_oSvg, _SelectedSvgGroup.mID);
                //_oMachineHandler.AddLink(_oSvg, _SelectedSvgGroup.mID, sTrendNr, sTagName);
                //_oSvgHandler.SaveSvgFile(_oSvg, _sVestiging, _sAfdeling);

            }

            //opslaan na aanpassingen
            _oSvgHandler.SaveSvgFile(_oSvg, _sVestiging, _sAfdeling);

            _UpdateWebBrowser();
        }

       
    }
}
