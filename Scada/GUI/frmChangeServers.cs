﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using BLL;
using DAL;

namespace GUI
{

    //https://stackoverflow.com/questions/5468342/how-to-modify-my-app-exe-config-keys-at-runtime
    //https://docs.microsoft.com/en-us/dotnet/api/system.configuration.configurationmanager?redirectedfrom=MSDN&view=netframework-4.7.2
    //https://stackoverflow.com/questions/40737395/modify-custom-app-config-config-section-and-save-it


    public partial class frmChangeServers : Form
    {
        private clsServerHandler _oServerHandler = new clsServerHandler();

        private Dictionary<string, string> oVestigingen;
        private Dictionary<string, string> oAfdelingen;
        //private clsSqlHandler _oSqlHandler = new clsSqlHandler();
        private Configuration oConfigFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

        private string sSelectedVestiging;
        private string sSelectedAfdeling;

        //Constructor
        public frmChangeServers()
        {
            InitializeComponent();
            
            oAfdelingen = new Dictionary<string, string>();
            _UpdateVestigingen();
            //Afdelingen worden pas weergeven bij selectie vestiging

            //enkel uppercase 
            txtNaamVestiging.CharacterCasing = CharacterCasing.Upper;
            txtNaamAfdeling.CharacterCasing = CharacterCasing.Upper;
        }

        #region Vestiging

        //Update listbox Vestigingen
        private void _UpdateVestigingen()
        {            
            List<string> lstVestigingen = _oServerHandler.GetServers();
            clbVestiging.DataSource = new BindingSource(lstVestigingen, null);
            clbVestiging.SelectedIndex = -1;
        }

        //OK
        //is de vestiging reeds gedefiniëerd?
        private void txtNaamVestiging_TextChanged(object sender, EventArgs e)
        {
            List<string> lstVestigingen = _oServerHandler.GetServers();
            string sVestiging = txtNaamVestiging.Text;

            if (lstVestigingen.Exists(item => item == sVestiging))
            {
                btnAddVestiging.Text = "Wijzigen";
                btnDeleteVestiging.Enabled = true;
                gpbAfdeling.Enabled = true;
                _UpdateAfdelingen();
            }
            else
            {
                btnAddVestiging.Text = "Toevoegen";
                btnDeleteVestiging.Enabled = false;
                gpbAfdeling.Enabled = false;
            }
        }

        //OK
        //toevoegen of wijzigen
        private void btnAddVestiging_Click(object sender, EventArgs e)
        {      
            string sVestiging = txtNaamVestiging.Text;
            string sServer = txtServer.Text;
            string sUserId = txtUserId.Text;
            string sPwd = txtPwd.Text;

            if (sVestiging.Count() > 0) 
            {
                if (sServer.Count() > 0)
                {
                    //kan toevoegen of wijzigen zijn....
                    _oServerHandler.AddOrChangeServer(sVestiging, sServer, sUserId, sPwd);
                    _UpdateVestigingen();                  
                }
            }         
        }

        //OK
        //verwijderen
        private void btnDeleteVestiging_Click(object sender, EventArgs e)
        {
            //Bevestiging vragen aan de gebruiker
            string sMessage = "De vestiging '"+sSelectedVestiging+"' en de bijhorende afdelingen verwijderen?";
            DialogResult oDelete = MessageBox.Show( sMessage, "Confirm Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (oDelete == DialogResult.OK)
            {
                //De server verwijderen
                _oServerHandler.DeleteServer(txtNaamVestiging.Text);
                _UpdateVestigingen();
            }           
        }

        //OK
        //Informatie over de geselecteerde vestiging weergeven
        private void clbVestiging_SelectedIndexChanged(object sender, EventArgs e)
        {
            sSelectedVestiging = clbVestiging.GetItemText(clbVestiging.SelectedItem);
            string sServer = "";
            string sUserId = "";
            string sPwd = "";

            if (clbVestiging.SelectedIndex > -1)
            {
                //Beter klasse maken => clsServer & clsDataBase
                //Dan kan deze worden gebruikt in plaats van 3 gegevens op te vragen in 1 functie
                _oServerHandler.GetServerInfo(sSelectedVestiging, out sServer, out sUserId, out sPwd);              
            }

            txtNaamVestiging.Text = sSelectedVestiging;
            txtServer.Text = sServer;
            txtUserId.Text = sUserId;
            txtPwd.Text = sPwd;
        }
             
        #endregion

        #region Afdeling

        //Update listbox Afdelingen
        private void _UpdateAfdelingen()
        {
            txtNaamAfdeling.Text = "";
            txtDatabase.Text = "";

            List<string> lstAfdelingen = _oServerHandler.GetDatabases(sSelectedVestiging);
            clbAfdeling.DataSource = new BindingSource(lstAfdelingen, null);
            clbAfdeling.SelectedIndex = -1;
        }

        //Afdeling bestaat al?
        private void txtNaamAfdeling_TextChanged(object sender, EventArgs e)
        {
            List<string> lstAfdelingen = _oServerHandler.GetDatabases(sSelectedVestiging);
            string sAfdeling = txtNaamAfdeling.Text;

            if (lstAfdelingen.Exists(item => item == sAfdeling))
            {
                btnAddAfdeling.Text = "Wijzigen";
                btnDeleteAfdeling.Enabled = true;
            }
            else
            {
                btnAddAfdeling.Text = "Toevoegen";
                btnDeleteAfdeling.Enabled = false;
            }
        }
        
        //toevoegen of wijzigen
        private void btnAddAfdeling_Click(object sender, EventArgs e)
        {
            string sAfdeling = txtNaamAfdeling.Text;
            string sDatabase = txtDatabase.Text;

            if (sAfdeling.Count() > 0)
            {
                if (sDatabase.Count() > 0)
                {
                    //kan toevoegen of wijzigen zijn....
                    _oServerHandler.AddOrChangeDatabase(sSelectedVestiging, sAfdeling, sDatabase);
                    _UpdateAfdelingen();
                }
            }
        }

        //verwijderen
        private void btnDeleteAfdeling_Click(object sender, EventArgs e)
        {
            //bevestiging vragen aan de gebruiker
            string sMessage = "De afdeling '"+sSelectedAfdeling+"' uit '"+sSelectedVestiging+"' verwijderen?";
            DialogResult oDelete = MessageBox.Show( sMessage, "Confirm Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (oDelete == DialogResult.OK)
            {
                _oServerHandler.DeleteDatabase(sSelectedVestiging, sSelectedAfdeling);
                clbAfdeling.SelectedIndex = -1;
                _UpdateVestigingen();
            }
        }
        
        //Info weergeven
        private void clbAfdeling_SelectedIndexChanged(object sender, EventArgs e)
        {
            sSelectedAfdeling = clbAfdeling.GetItemText(clbAfdeling.SelectedItem);
            string sDatabase = "";

            if (clbAfdeling.SelectedIndex > -1)
            {
                _oServerHandler.GetDatabaseInfo(sSelectedVestiging, sSelectedAfdeling, out sDatabase);
            }

            txtNaamAfdeling.Text = sSelectedAfdeling;
            txtDatabase.Text = sDatabase;
        }
        
        #endregion
    }
}
