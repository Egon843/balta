﻿namespace GUI
{
    partial class frmChangeServers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChangeServers));
            this.gpbVestiging = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.clbVestiging = new System.Windows.Forms.ListBox();
            this.lblNaamVestiging = new System.Windows.Forms.Label();
            this.lblServer = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.txtNaamVestiging = new System.Windows.Forms.TextBox();
            this.btnDeleteVestiging = new System.Windows.Forms.Button();
            this.btnAddVestiging = new System.Windows.Forms.Button();
            this.gpbAfdeling = new System.Windows.Forms.GroupBox();
            this.clbAfdeling = new System.Windows.Forms.ListBox();
            this.lblNaamAfdeling = new System.Windows.Forms.Label();
            this.btnAddAfdeling = new System.Windows.Forms.Button();
            this.btnDeleteAfdeling = new System.Windows.Forms.Button();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.txtNaamAfdeling = new System.Windows.Forms.TextBox();
            this.gpbVestiging.SuspendLayout();
            this.gpbAfdeling.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpbVestiging
            // 
            this.gpbVestiging.Controls.Add(this.label1);
            this.gpbVestiging.Controls.Add(this.label2);
            this.gpbVestiging.Controls.Add(this.txtPwd);
            this.gpbVestiging.Controls.Add(this.txtUserId);
            this.gpbVestiging.Controls.Add(this.clbVestiging);
            this.gpbVestiging.Controls.Add(this.lblNaamVestiging);
            this.gpbVestiging.Controls.Add(this.lblServer);
            this.gpbVestiging.Controls.Add(this.txtServer);
            this.gpbVestiging.Controls.Add(this.txtNaamVestiging);
            this.gpbVestiging.Controls.Add(this.btnDeleteVestiging);
            this.gpbVestiging.Controls.Add(this.btnAddVestiging);
            this.gpbVestiging.Location = new System.Drawing.Point(12, 12);
            this.gpbVestiging.Name = "gpbVestiging";
            this.gpbVestiging.Size = new System.Drawing.Size(318, 180);
            this.gpbVestiging.TabIndex = 0;
            this.gpbVestiging.TabStop = false;
            this.gpbVestiging.Text = "Vestiging";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "User ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(156, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Password:";
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new System.Drawing.Point(156, 146);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.Size = new System.Drawing.Size(156, 20);
            this.txtPwd.TabIndex = 11;
            // 
            // txtUserId
            // 
            this.txtUserId.Location = new System.Drawing.Point(156, 107);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(156, 20);
            this.txtUserId.TabIndex = 10;
            // 
            // clbVestiging
            // 
            this.clbVestiging.FormattingEnabled = true;
            this.clbVestiging.Location = new System.Drawing.Point(6, 19);
            this.clbVestiging.Name = "clbVestiging";
            this.clbVestiging.Size = new System.Drawing.Size(143, 121);
            this.clbVestiging.TabIndex = 8;
            this.clbVestiging.SelectedIndexChanged += new System.EventHandler(this.clbVestiging_SelectedIndexChanged);
            // 
            // lblNaamVestiging
            // 
            this.lblNaamVestiging.AutoSize = true;
            this.lblNaamVestiging.Location = new System.Drawing.Point(156, 13);
            this.lblNaamVestiging.Name = "lblNaamVestiging";
            this.lblNaamVestiging.Size = new System.Drawing.Size(38, 13);
            this.lblNaamVestiging.TabIndex = 7;
            this.lblNaamVestiging.Text = "Naam:";
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(156, 52);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(41, 13);
            this.lblServer.TabIndex = 6;
            this.lblServer.Text = "Server:";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(156, 68);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(156, 20);
            this.txtServer.TabIndex = 5;
            // 
            // txtNaamVestiging
            // 
            this.txtNaamVestiging.Location = new System.Drawing.Point(156, 29);
            this.txtNaamVestiging.Name = "txtNaamVestiging";
            this.txtNaamVestiging.Size = new System.Drawing.Size(156, 20);
            this.txtNaamVestiging.TabIndex = 4;
            this.txtNaamVestiging.TextChanged += new System.EventHandler(this.txtNaamVestiging_TextChanged);
            // 
            // btnDeleteVestiging
            // 
            this.btnDeleteVestiging.Location = new System.Drawing.Point(6, 144);
            this.btnDeleteVestiging.Name = "btnDeleteVestiging";
            this.btnDeleteVestiging.Size = new System.Drawing.Size(70, 23);
            this.btnDeleteVestiging.TabIndex = 3;
            this.btnDeleteVestiging.Text = "Verwijderen";
            this.btnDeleteVestiging.UseVisualStyleBackColor = true;
            this.btnDeleteVestiging.Click += new System.EventHandler(this.btnDeleteVestiging_Click);
            // 
            // btnAddVestiging
            // 
            this.btnAddVestiging.Location = new System.Drawing.Point(74, 144);
            this.btnAddVestiging.Name = "btnAddVestiging";
            this.btnAddVestiging.Size = new System.Drawing.Size(76, 23);
            this.btnAddVestiging.TabIndex = 1;
            this.btnAddVestiging.Text = "Toevoegen";
            this.btnAddVestiging.UseVisualStyleBackColor = true;
            this.btnAddVestiging.Click += new System.EventHandler(this.btnAddVestiging_Click);
            // 
            // gpbAfdeling
            // 
            this.gpbAfdeling.Controls.Add(this.clbAfdeling);
            this.gpbAfdeling.Controls.Add(this.lblNaamAfdeling);
            this.gpbAfdeling.Controls.Add(this.btnAddAfdeling);
            this.gpbAfdeling.Controls.Add(this.btnDeleteAfdeling);
            this.gpbAfdeling.Controls.Add(this.lblDatabase);
            this.gpbAfdeling.Controls.Add(this.txtDatabase);
            this.gpbAfdeling.Controls.Add(this.txtNaamAfdeling);
            this.gpbAfdeling.Location = new System.Drawing.Point(12, 198);
            this.gpbAfdeling.Name = "gpbAfdeling";
            this.gpbAfdeling.Size = new System.Drawing.Size(318, 168);
            this.gpbAfdeling.TabIndex = 8;
            this.gpbAfdeling.TabStop = false;
            this.gpbAfdeling.Text = "Afdeling";
            // 
            // clbAfdeling
            // 
            this.clbAfdeling.FormattingEnabled = true;
            this.clbAfdeling.Location = new System.Drawing.Point(6, 19);
            this.clbAfdeling.Name = "clbAfdeling";
            this.clbAfdeling.Size = new System.Drawing.Size(143, 134);
            this.clbAfdeling.TabIndex = 9;
            this.clbAfdeling.SelectedIndexChanged += new System.EventHandler(this.clbAfdeling_SelectedIndexChanged);
            // 
            // lblNaamAfdeling
            // 
            this.lblNaamAfdeling.AutoSize = true;
            this.lblNaamAfdeling.Location = new System.Drawing.Point(155, 16);
            this.lblNaamAfdeling.Name = "lblNaamAfdeling";
            this.lblNaamAfdeling.Size = new System.Drawing.Size(38, 13);
            this.lblNaamAfdeling.TabIndex = 7;
            this.lblNaamAfdeling.Text = "Naam:";
            // 
            // btnAddAfdeling
            // 
            this.btnAddAfdeling.Location = new System.Drawing.Point(236, 130);
            this.btnAddAfdeling.Name = "btnAddAfdeling";
            this.btnAddAfdeling.Size = new System.Drawing.Size(75, 23);
            this.btnAddAfdeling.TabIndex = 1;
            this.btnAddAfdeling.Text = "Toevoegen";
            this.btnAddAfdeling.UseVisualStyleBackColor = true;
            this.btnAddAfdeling.Click += new System.EventHandler(this.btnAddAfdeling_Click);
            // 
            // btnDeleteAfdeling
            // 
            this.btnDeleteAfdeling.Location = new System.Drawing.Point(155, 130);
            this.btnDeleteAfdeling.Name = "btnDeleteAfdeling";
            this.btnDeleteAfdeling.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteAfdeling.TabIndex = 3;
            this.btnDeleteAfdeling.Text = "Verwijderen";
            this.btnDeleteAfdeling.UseVisualStyleBackColor = true;
            this.btnDeleteAfdeling.Click += new System.EventHandler(this.btnDeleteAfdeling_Click);
            // 
            // lblDatabase
            // 
            this.lblDatabase.AutoSize = true;
            this.lblDatabase.Location = new System.Drawing.Point(155, 55);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(56, 13);
            this.lblDatabase.TabIndex = 6;
            this.lblDatabase.Text = "Database:";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(155, 71);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(156, 20);
            this.txtDatabase.TabIndex = 5;
            // 
            // txtNaamAfdeling
            // 
            this.txtNaamAfdeling.Location = new System.Drawing.Point(155, 32);
            this.txtNaamAfdeling.Name = "txtNaamAfdeling";
            this.txtNaamAfdeling.Size = new System.Drawing.Size(156, 20);
            this.txtNaamAfdeling.TabIndex = 4;
            this.txtNaamAfdeling.TextChanged += new System.EventHandler(this.txtNaamAfdeling_TextChanged);
            // 
            // frmNavigatie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 374);
            this.Controls.Add(this.gpbAfdeling);
            this.Controls.Add(this.gpbVestiging);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmNavigatie";
            this.Text = "BFV Databases";
            this.gpbVestiging.ResumeLayout(false);
            this.gpbVestiging.PerformLayout();
            this.gpbAfdeling.ResumeLayout(false);
            this.gpbAfdeling.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbVestiging;
        private System.Windows.Forms.Label lblNaamVestiging;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.TextBox txtNaamVestiging;
        private System.Windows.Forms.Button btnDeleteVestiging;
        private System.Windows.Forms.Button btnAddVestiging;
        private System.Windows.Forms.GroupBox gpbAfdeling;
        private System.Windows.Forms.Label lblNaamAfdeling;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.TextBox txtNaamAfdeling;
        private System.Windows.Forms.Button btnDeleteAfdeling;
        private System.Windows.Forms.Button btnAddAfdeling;
        private System.Windows.Forms.ListBox clbVestiging;
        private System.Windows.Forms.ListBox clbAfdeling;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.TextBox txtUserId;
    }
}