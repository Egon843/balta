﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmLogin());

            //string sConn = "Server=extprddbssbv01;Database=ProcesDataTuft;User Id=ProcesDbUser;Password=balta";
            //string sName = "TrekW_1_run";
            //string sTrendNr = "221002";
            //bool xBit = false;

            //Application.Run(new frmTrending(sConn, sName, sTrendNr, xBit));

        }
    }
}
