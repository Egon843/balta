﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace GUI
{
    public partial class frmChangeUsers : Form
    {

        //Constructor
        public frmChangeUsers()
        {
            InitializeComponent();
            
            txtPwd.PasswordChar = '●';
            _UpdateForm();
        }
        
        //Gebruiker toevoegen
        private void btnAddUser_Click(object sender, EventArgs e)
        {
            clsHasher.AddUser(txtUser.Text, txtPwd.Text);
            _UpdateForm();
        }

        //Gebruiker verwijderen
        private void btnRemoveUser_Click(object sender, EventArgs e)
        {
            string sUser = lboUsers.GetItemText(lboUsers.SelectedItem);
            clsHasher.RemoveUser(sUser);
            _UpdateForm();
        }    

        //Update
        private void _UpdateForm()
        {
            txtUser.Text = "";
            txtPwd.Text = "";

            var blUsers = new BindingList<string>(clsHasher.GetUsers());
            lboUsers.DataSource = blUsers;
        }              

    }
}
