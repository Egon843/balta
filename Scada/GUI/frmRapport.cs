﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using DAL;
using BLL;
using BEC;
using System.IO;


namespace GUI
{
    public partial class frmRapport : Form
    {
        private clsMachineHandler _oMachineHandler;
        private List<int> lstAddedRows = new List<int>();

        //constructor        
        public frmRapport(string sConnString, List<clsItemData> oBitItems) //enkel bit items voor het berekenen van de OEE
        {
            InitializeComponent();
            
            _oMachineHandler = new clsMachineHandler();
            _oMachineHandler.SetDatabase(sConnString);

            //sorteren in volgorde
            oBitItems.Sort((x, y) => x.ToString().CompareTo(y.ToString()));

            //toevoegen aan checked listbox
            foreach (clsItemData item in oBitItems)
            {
                clbItems.Items.Add(item, false);
            }

            btnUndo.Enabled = false;

            _InitDataGrid(); //instellingen voor de datagridview
        }

        //instellingen datagridview
        private void _InitDataGrid()
        {
            dgvRapport.RowHeadersVisible = false;//lege linker kolom verbergen
            dgvRapport.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvRapport.AllowUserToAddRows = false;

            //kollomen instellen
            dgvRapport.ColumnCount = 7;
            dgvRapport.Columns[0].HeaderText = "";
            dgvRapport.Columns[1].HeaderText = "Dag";
            dgvRapport.Columns[2].HeaderText = "P1";
            dgvRapport.Columns[3].HeaderText = "P2";
            dgvRapport.Columns[4].HeaderText = "P3";
            dgvRapport.Columns[5].HeaderText = "W1";
            dgvRapport.Columns[6].HeaderText = "W2";
            
            //eerste rij met informatie toevoegen
            dgvRapport.Rows.Add("", "24h", "05h-13h", "13h-21h", "21h-05h", "5h-17h", "17h-05h");
        }
            
        //Rapport van de gekozen dag toevoegen aan de tabel
        private void btnDagRapport_Click(object sender, EventArgs e)
        {
            //datum toevoegen
            DateTime dtRapport = dtpDatum.Value;
            string sDT = dtRapport.Date.ToString("dddd, dd/MM/yyyy");
            dgvRapport.Rows.Add(sDT);

            int iRowsAdded = 1; // 1e rij = sDT

            //elke 'checked' item toevoegen 
            foreach (clsItemData item in clbItems.CheckedItems)
            {
                _AddRow(dtRapport, item); //voegt voor elke ploeg van die dag de OEE toe aan de tabel
                iRowsAdded++;
            }

            //bijhouden hoeveel rijen zijn toegevoegd voor het ongedaan maken van deze actie
            lstAddedRows.Add(iRowsAdded);
            btnUndo.Enabled = true;

            dgvRapport.AutoResizeColumn(0);//de datum is de grootste tekst waarde
        }

        //Laatst toegevoegde rijen terug verwijderen
        private void btnUndo_Click(object sender, EventArgs e)
        {
            int iRowsToRemove = lstAddedRows.Last(); //hoeveel rijen zijn laatst toegevoegd

            //deze rijen uit datagrid verwijderen
            for (int i = 0; i < iRowsToRemove; i++)
            {
                int iLast = dgvRapport.Rows.Count -1;
                dgvRapport.Rows.RemoveAt(iLast);
            }

            //aantal uit lijst halen
            lstAddedRows.RemoveAt(lstAddedRows.Count - 1);

            //button disablen indien nodig
            if (lstAddedRows.Count == 0)
            {
                btnUndo.Enabled = false;
            }

        }

        //De OEE berekenen van het item voor de verschillende ploegen van de gekozen dag
        private void _AddRow(DateTime dtDay, clsItemData oItem)
        {
            //Volledige dag
            TimeSpan oTS = new TimeSpan(24, 0, 0); //12 uren per ploeg
            TimeSpan oDagStart = new TimeSpan(0, 0, 0); //Startuur Dag

            //OEE Dag
            DateTime dtDagStart = dtDay.Date + oDagStart;
            DateTime dtDagEind = dtDagStart + oTS;
            string sDag = _oMachineHandler.GetOEE(oItem.TrendNr, dtDagStart, dtDagEind);

            //weeekend
            if (dtDay.DayOfWeek == DayOfWeek.Saturday | dtDay.DayOfWeek == DayOfWeek.Sunday)
            {
                oTS = new TimeSpan(12,0,0); //12 uren per ploeg
                TimeSpan oW1Start = new TimeSpan(5, 0, 0); //Startuur ploeg W1
                TimeSpan oW2Start = new TimeSpan(17, 0, 0); //startuur ploeg W2

                //OEE W1
                DateTime dtW1Start = dtDay.Date + oW1Start;
                DateTime dtW1Eind = dtW1Start + oTS;
                string sW1 = _oMachineHandler.GetOEE(oItem.TrendNr, dtW1Start, dtW1Eind);

                //OEE W2
                DateTime dtW2Start = dtDay.Date + oW2Start;
                DateTime dtW2Eind = dtW2Start + oTS;
                string sW2 = _oMachineHandler.GetOEE(oItem.TrendNr, dtW2Start, dtW2Eind);

                //rij toevoegen
                dgvRapport.Rows.Add(oItem.ToString(), sDag,"","","", sW1, sW2);
            }
            //week
            else
            {
                //werkuren + starturen
                oTS = new TimeSpan(8,0,0);
                TimeSpan oP1Start = new TimeSpan(5, 0, 0); //P1
                TimeSpan oP2Start = new TimeSpan(13, 0, 0); //P2
                TimeSpan oP3Start = new TimeSpan(21, 0, 0); //P3

                //OEE P1
                DateTime dtP1Start = dtDay.Date + oP1Start;
                DateTime dtP1Eind = dtP1Start + oTS;
                string sP1 = _oMachineHandler.GetOEE(oItem.TrendNr, dtP1Start, dtP1Eind);

                //OEE P2
                DateTime dtP2Start = dtDay.Date + oP2Start;
                DateTime dtP2Eind = dtP2Start + oTS;
                string sP2 = _oMachineHandler.GetOEE(oItem.TrendNr, dtP2Start, dtP2Eind);

                //OEE P3
                DateTime dtP3Start = dtDay.Date + oP3Start;
                DateTime dtP3Eind = dtP3Start + oTS;
                string sP3 = _oMachineHandler.GetOEE(oItem.TrendNr, dtP3Start, dtP3Eind);

                //rij toevoegen
                dgvRapport.Rows.Add(oItem.ToString(),sDag, sP1, sP2, sP3);
            }
        }
            
        //Alle items 'checken'
        private void btnAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < clbItems.Items.Count; i++)
            {
                clbItems.SetItemChecked(i, true);
            }
        }

        //Alle items 'unchecken'
        private void btnNone_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < clbItems.Items.Count; i++)
            {
                clbItems.SetItemChecked(i, false);
            }
        }
             
        //Exporteren van het opgestelde rapport naar CSV formaat (voor excel wordt ';' gebruikt als scheidingsteken)
        //https://stackoverflow.com/questions/9943787/exporting-datagridview-to-csv-file
        private void btnExportCsv_Click(object sender, EventArgs e)
        {
            string sFileName = "Rapport_" + dtpDatum.Value.Date.ToString("dd_MMM"); //naam suggestie

            //de gebruiker een bestandslocatie laten kiezen
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "CSV (*.csv)|*.csv";
            sfd.FileName = sFileName + ".csv";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                var sb = new StringBuilder();

                //toevoegen van de headers
                var headers = dgvRapport.Columns.Cast<DataGridViewColumn>();
                sb.AppendLine(string.Join(";", headers.Select(column => "\"" + column.HeaderText + "\"").ToArray()));

                //toevoegen van de rijen
                foreach (DataGridViewRow row in dgvRapport.Rows)
                {
                    var cells = row.Cells.Cast<DataGridViewCell>();
                    sb.AppendLine(string.Join(";", cells.Select(cell => "\"" + cell.Value + "\"").ToArray()));
                }

                //bestand opslaan
                try
                {
                    File.WriteAllText(sfd.FileName, sb.ToString());
                }
                catch (Exception)
                {
                    MessageBox.Show( "Een bestand met dezelfde naam wordt momenteel door een ander programma gebruikt.",
                                    "Niet opgeslagen", 
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }
               
    }
}
