﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices; //ComVisibleAttribute
using System.IO;
using System.ComponentModel;
using BLL;
using BEC;
using System.Diagnostics;
using System.Threading;


namespace GUI
{
    public partial class frmUser : Form
    {
        //Database keuze
        private string _sVestiging;
        private string _sAfdeling;

        //Tab Navigatie
        private clsMachine _SelectedMachine;
        private clsSvg _oSvg; //= null als machine nog geen SVG heeft

        //Tab Gegevens
        private List<clsItemData> _lstLinkedItemValues = new List<clsItemData>();
        private List<clsSvgLink> _lstLinks = new List<clsSvgLink>();
        private System.Windows.Forms.Timer _oTimer;
        
        //BLL
        private clsSvgHandler _oSvgHandler = new clsSvgHandler();
        private clsMachineHandler _oMachineHandler = new clsMachineHandler();
        private clsServerHandler _oServerHandler = new clsServerHandler();
               

        //Constructor
        public frmUser()
        {
            InitializeComponent();
            
            //timer
            _oTimer = new System.Windows.Forms.Timer();
            _oTimer.Tick += new EventHandler(_oTimer_Tick);//EventHandler toewijzen
                       
            //WebBrowser init
            webBrowser1.ObjectForScripting = new SvgEventHandler(this); //Hier staan de methodes die vanuit de SVG worden opgeroepen
            webBrowser1.Navigate("about:blank");                

            //Mogelijke servers laden
            cboVestiging.DataSource = _oServerHandler.GetServers();

            //DatagridView init
            dgvLinkedItems.RowHeadersVisible = false;
            dgvLinkedItems.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dgvLinkedItems.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvLinkedItems.ReadOnly = true;
            dgvLinkedItems.AllowUserToDeleteRows = false;
            dgvLinkedItems.AllowUserToAddRows = false;
            dgvLinkedItems.MultiSelect = false;

            //datetimepickers
            //https://stackoverflow.com/questions/93472/datetimepicker-pick-both-date-and-time
            dtpFrom.Format = DateTimePickerFormat.Custom;
            dtpFrom.CustomFormat = "dd/MM/yyy   HH:mm";
            dtpTo.Format = DateTimePickerFormat.Custom;
            dtpTo.CustomFormat = "dd/MM/yyy   HH:mm";
        }

        #region CyclischUpdate
        //Cyclisch updaten van de database gegevens
        private void _oTimer_Tick(object sender, EventArgs e)
        {
            _oTimer.Enabled = false;

            ////De stopwatch kan worden gebruikt om de cyclustijd te bekijken
            
            //Stopwatch watch = new Stopwatch();
            //watch.Start();

            _UpdateValues();

            //watch.Stop();
            //MessageBox.Show(watch.ElapsedMilliseconds.ToString());

            _oTimer.Enabled = true;
        }

        /// <summary>
        /// De actuele waarde uit de database ophalen en weergeven in de SVG en datagrid
        /// </summary>
        private void _UpdateValues()
        {
            if (_oSvg != null)
            {
                //Thread gebruiken zodat de software operationeel blijft
                //https://www.pluralsight.com/guides/how-to-write-your-first-multi-threaded-application-with-c
                Thread thrGetValues = new Thread(() =>
                {
                    _lstLinkedItemValues = _oMachineHandler.GetItemValues(_lstLinks);//Gegevens opvragen
                    _oSvg.UpdateValues(_lstLinkedItemValues);//SVG aanpassen
                });

                thrGetValues.Start();
                thrGetValues.Join();
                
                //Gegevens updaten in de tabel
                if (_lstLinkedItemValues != null)
                {
                    var blLinkedItems = new BindingList<clsItemData>(_lstLinkedItemValues);
                    dgvLinkedItems.DataSource = blLinkedItems;                   
                }
            }

            //SVG updaten in de webbrowser
            _UpdateWebBrowser();
        }

        /// <summary>
        /// Weergave van de webbrowser updaten
        /// </summary>
        private void _UpdateWebBrowser()
        {
            string sSVG;

            //Controle: machine en SVG geselecteerd?
            if (_SelectedMachine == null)
            {
                sSVG = "No machine selected.";
            }
            else if (_oSvg == null)
            {
                sSVG = "This machine has no SVG-file.";
            }
            else
            {
                sSVG = _oSvg.GetXmlString();//string opvragen van de SVG-figuur
            }            

            //String in de webbrowser laden 
            webBrowser1.Document.OpenNew(true);
            webBrowser1.Document.Write(sSVG);
            webBrowser1.Refresh(); 
        }
        #endregion

        //Vangt de click-event op uit de WebBrowser (SVG)
        #region SVG_Click
            
        [ComVisible(true)] //COM interface zichtbaar maken
        public class SvgEventHandler //De klasse waarvan het COM zichtbaar wordt
        {
            private frmUser frmMain;

            //constructor
            public SvgEventHandler(frmUser MainForm) 
            {
                frmMain = MainForm; 
            }

            //Methode die door het SVG-event wordt aangeroepen
            public void SvgGroup_Click(string mID) //mID wordt meegegeven vanuit de SVG zelf
            {
                frmMain.OpenGroupOptions(mID);
            }
        }

        //Deze methode wordt aangeroepen vanuit de klasse 'SvgEventHandler'
        //en bepaalt welke opties mogelijk zijn bij het klikken op de svg-tekening
        private void OpenGroupOptions(string mID)
        {
            //SVG-group aanduiden
            _oSvg.GlowGroup(mID);
            _UpdateWebBrowser();

            //Gelinkte data aanduiden
            if (_oSvg.GetSvgGroup(mID).TrendTag.Count() > 5)//Bij geen link is TrendTag = '-1'
            {
                //gelinkte data selecteren
                _SelectLink(_oSvg.GetSvgGroup(mID).TrendTag);
            }
            else
            {
                //geen enkele link selecteren
                dgvLinkedItems.ClearSelection();                
            }          

            //Menustrip kan gebruikt worden om rechtstreeks de opties voor trending aan te  bieden

            ////ContextMenuStrip opbouwen
            //cmsSvgClick.Items.Clear();
            //cmsSvgClick.Items.Insert(0, new ToolStripLabel("mID: " + mID));
            //cmsSvgClick.Items.Insert(1, new ToolStripSeparator());
            //ToolStripItem tsiInfo = cmsSvgClick.Items.Add("Info");
            //ToolStripItem tsiTrends = cmsSvgClick.Items.Add("Trends");

            ////EventHandlers toevoegen
            //tsiInfo.Click += TsiInfo_Click;
            //tsiTrends.Click += TsiTrends_Click;

            ////ContextMenuStrip weergeven
            //Point MousePosition = PointToClient(System.Windows.Forms.Control.MousePosition);
            //cmsSvgClick.Show(this, MousePosition);
        }

        //Voor het selecteren van de gelinkte data bij het aanklikken van een SVG-group
        private void _SelectLink(string sTrendNr)
        {
            //Huidige selectie verwijderen
            dgvLinkedItems.ClearSelection();

            //De juiste link in de dgv zoeken
            DataGridViewRow row = dgvLinkedItems.Rows
                .Cast<DataGridViewRow>()
                .Where(r => r.Cells["TrendNr"].Value.ToString().Equals(sTrendNr))
                .First();

            //De link selecteren en weergeven
            int iIndex = row.Index;
            dgvLinkedItems.Rows[iIndex].Selected = true;
            dgvLinkedItems.FirstDisplayedScrollingRowIndex = iIndex;
        }

        #endregion


        //TabControl
        //#######################################################

        //Laden van gegevens bij het veranderen van tabvenster
        //!!!Indexen aanpassen als Tabs verplaatst worden
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Tab Navigatie = 0
            if (tabControl1.SelectedIndex == 0)
            {

            }
            //Tab Gegevens = 1
            else if (tabControl1.SelectedIndex == 1)
            {
                //De gegevens updaten in de SVG figuur en in de datagrid
                _UpdateValues();
            }
        }

        //Events en methodes van het navigatie tabblad
        #region Navigatie

        //Vestiging selecteren
        private void cboVestiging_SelectedIndexChanged(object sender, EventArgs e)
        {
            _sVestiging = cboVestiging.SelectedItem.ToString();

            //de mogelijke afdelingen weergeven
            cboAfdeling.DataSource = _oServerHandler.GetDatabases(_sVestiging);
        }

        //Afdeling selecteren
        private void cboAfdeling_SelectedIndexChanged(object sender, EventArgs e)
        {         
            _sAfdeling = cboAfdeling.SelectedItem.ToString();

            //connectie maken met de gekozen database
            string sConnStr = _oServerHandler.GetConnectionString(_sVestiging, _sAfdeling);
            _oMachineHandler.SetDatabase(sConnStr);

            //De mogelijke groepen weergeven
            try
            {
                List<clsMachine> lstGroups = _oMachineHandler.GetGroups();

                //Bij het weergeven van de groepen nog geen selectie maken
                lboMachinegroep.SelectionMode = SelectionMode.None;
                lboMachinegroep.DataSource = new BindingSource(lstGroups, null);
                lboMachinegroep.SelectionMode = SelectionMode.One;

                //Na het selecteren van de afdeling moet eerst een 'Groep' worden gekozen
                lboLevel.DataSource = null;
                lboSubgroep.DataSource = null;
            }
            //Als de mogelijke groepen niet kunnen geladen worden is er geen verbinding met de database
            catch (Exception)
            {
                MessageBox.Show("Kan geen verbinding maken met de gekozen database.");
            }           
        }

        //Groep selecteren
        private void lboMachinegroep_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lboMachinegroep.SelectedIndex > -1)
            {
                //Selecteren van de machine
                clsMachine oGroup = lboMachinegroep.SelectedItem as clsMachine;
                _SelectMachine(oGroup);

                //De mogelijke levels weergeven zonder een level te selecteren
                List<clsMachine> lstLevels = _oMachineHandler.GetLevels(oGroup.GroupNr);
                lboLevel.SelectionMode = SelectionMode.None;
                lboLevel.DataSource = new BindingSource(lstLevels, null);
                lboLevel.SelectionMode = SelectionMode.One;

                lboSubgroep.DataSource = null;
            }
        }

        //Level selecteren
        private void lboLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lboLevel.SelectedIndex > -1)
            {
                //Selecteren van de machine
                clsMachine oLevel = (clsMachine)lboLevel.SelectedItem;
                _SelectMachine(oLevel);

                //De mogelijke Subgroepen weergeven zonder selectie
                List<clsMachine> lstSubGroepen = _oMachineHandler.GetSubgroups(oLevel.GroupNr, oLevel.LevelNr);
                lboSubgroep.SelectionMode = SelectionMode.None;
                lboSubgroep.DataSource = new BindingSource(lstSubGroepen, null);
                lboSubgroep.SelectionMode = SelectionMode.One;
            }
        }

        //Subgroep selecteren
        private void lboSubgroep_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lboSubgroep.SelectedIndex > -1)
            {
                //Selecteren van de machine
                clsMachine oSubgroup = (clsMachine)lboSubgroep.SelectedItem;
                _SelectMachine(oSubgroup);
            }
        }

        //Methode voor het selecteren van een machine
        private void _SelectMachine(clsMachine oMachine)
        {
            if (oMachine != null)
            {
                _SelectedMachine = oMachine;

                //De SVG-bestandsnaam opvragen
                string sSvgFileName = oMachine.GenerateSvgFileName();

                //Controleren of de SVG reeds geconfigureerd is
                //_oSvg blijft 'null' als de SVG nog niet bestaat
                _oSvg = _oSvgHandler.LoadExistingFile(sSvgFileName, _sVestiging, _sAfdeling);

                //De links uit de database opvragen
                _lstLinks = _oMachineHandler.GetLinkedItems(_SelectedMachine, _oSvg);

                //De actuele waarde van de procesdata opvragen en in de SVG en datagrid weergeven
                _UpdateValues();
            }
           
        }

        #endregion

        //Events en methodes van het Gegevens tabblad
        #region Gegevens

        //Cyclisch updaten starten of stoppen
        private void btnEnableUpdate_Click(object sender, EventArgs e)
        {
            //Als de timer loopt
            if (_oTimer.Enabled)
            {
                //Cyclisch updaten pauzeren
                _oTimer.Enabled = false;
                btnEnableUpdate.Text = "Start";
            }
            //Als de timer gestopt is
            else
            {
                int iSeconden;

                //Controleren of het aantal seconden een getal is
                if (int.TryParse(txtCycleTime.Text, out iSeconden))
                {
                    //De timer starten
                    _oTimer.Interval = iSeconden * 1000;
                    _oTimer.Enabled = true;

                    btnEnableUpdate.Text = "Stop";
                    txtCycleTime.BackColor = Color.White;
                }
                else
                {
                    MessageBox.Show("Gelieve een correcte cyclustijd mee te geven.");
                    txtCycleTime.BackColor = Color.Red;
                }
            }

        }
        
        //Berekenen van alle OEE waarden in de SVG-tekening
        private void btnOEE_Click(object sender, EventArgs e)
        {
            if (_oSvg != null)
            {
                //alle groepen met een OEE link verzamelen
                foreach (clsSvgGroep oGroup in _oSvg.SvgGroups.FindAll(oGr => oGr.IsOEE()))
                {
                    DateTime oBegin = dtpFrom.Value;
                    DateTime oEind = dtpTo.Value;

                    //OEE waarde opvragen en in de SVG-figuur plaatsen
                    string sOEE = _oMachineHandler.GetOEE(oGroup.TrendTag, oBegin, oEind);
                    oGroup.UpdateOEE(sOEE);
                }

                //Actuele waarden refreshen
                _UpdateValues();//Zorgt ook voor het updaten van de Webbrowser
            }          
        }

        private void btnRemoveOEE_Click(object sender, EventArgs e)
        {
            _SelectMachine(_SelectedMachine);
        }

        //Selecteren van een gelinkt item
        private void dgvLinkedItems_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvLinkedItems.SelectedRows.Count > 0)
            {
                //De gelinkte SVG-elementen markeren
                try
                {
                    string sTrendNr = dgvLinkedItems.SelectedRows[0].Cells["TrendNr"].Value.ToString();                    
                    _oSvg.GlowGroups(sTrendNr);
                    _UpdateWebBrowser();
                }
                catch (Exception)
                { }
            }
        }

        //Trend weergeven van het geselecteerde item
        private void btnTrending_Click(object sender, EventArgs e)
        {
            if (dgvLinkedItems.SelectedRows.Count > 0)
            {
                try
                {
                    //ConnectionString meegeven
                    string sConnString = _oServerHandler.GetConnectionString(_sVestiging, _sAfdeling);

                    //Gegevens uit de selectie halen
                    string sName = dgvLinkedItems.SelectedRows[0].Cells["Naam"].Value.ToString();
                    string sTrendNr2 = dgvLinkedItems.SelectedRows[0].Cells["TrendNr"].Value.ToString();
                    //controleren of het item een bitwaarde is
                    bool xIsBit = dgvLinkedItems.SelectedRows[0].Cells["Eenheid"].Value.ToString().ToLower() == "bit";

                    //Trending form openen
                    frmTrending frmTrend = new frmTrending(sConnString, sName, sTrendNr2, xIsBit);
                    frmTrend.Show();
                }
                catch (Exception)
                {
                    MessageBox.Show("Historische data niet beschikbaar.");              
                }
                
            }
            else
            {
                MessageBox.Show("Geen item geselecteerd.");
            }            
        }

        //Rapport weergeven van de digitale waarden (OEE)        
        private void btnRapport_Click(object sender, EventArgs e)
        {
            //kan uitgebreid worden naar analoge waarden (getufte lengte => optellen | tuftsnelheid => gemiddelde)

            //Digitale waarden verzamelen van de machine
            List<clsItemData> lstBitItems = new List<clsItemData>();
            foreach (clsItemData item in _lstLinkedItemValues)
            {
                if (item.Eenheid.ToLower() == "bit")
                {
                    lstBitItems.Add(item);
                }
            }

            //Connection string meegeven
            string sConnStr = _oServerHandler.GetConnectionString(_sVestiging, _sAfdeling);

            //Openen van rapport form
            frmRapport oFormRapport = new frmRapport(sConnStr, lstBitItems);
            oFormRapport.Show();
        }


        #endregion

        private void btnFullScreen_Click(object sender, EventArgs e)
        {
            frmUser_FS oFullScreen = new frmUser_FS(_SelectedMachine, _sVestiging, _sAfdeling, _lstLinks);
            oFullScreen.Show();
        }

        private void frmUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_oTimer.Enabled)
            {
                e.Cancel = true;
                MessageBox.Show("Gelieve eerst de updatecyclus uit te schakelen");
            }
        }
    }
}
