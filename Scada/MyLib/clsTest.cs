﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLib
{

    class MainApp
    {       
        static void Main()
        {
            clGroup document = new clGroup();                  
        }
    }

     class clGroup
    {
        private List<clssVisual> _Visuals = new List<clssVisual>();

        // Constructor calls abstract Factory method
        public clGroup()
        {
            this.CreateVisuals();
        }

        public List<clssVisual> Visuals
        {
            get { return _Visuals; }
        }

        public void CreateVisuals()
        { 
            Visuals.Add(new clssShape());
            Visuals.Add(new clssText());
        }
    }

    class clssVisual
    { }

    class clssShape : clssVisual
    { }

    class clssText : clssVisual
    { }

}
