﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BEC;


namespace DAL
{
    public class clsSqlHandler
    {
        private string _sConnStr;

        //constructor
        public clsSqlHandler()
        { }
        
        public string ConnectionString
        {
            get { return _sConnStr; }
            set { _sConnStr = value; }
        }        
        private SqlConnection _SqlConnection()
        {
            SqlConnection oConn = new SqlConnection(_sConnStr);
            oConn.ConnectionString = _sConnStr;
            return oConn;
        }


        //##################################################

        //GROEP
        public List<clsMachine> GetGroups()
        {
            List<clsMachine> lstGroups = new List<clsMachine>();

            //Invullen van het SQL commando
            string sCmdText = "SELECT [GroepNr] ,[GroepNaam] " +
                                     "FROM [dbo].[tblGroepNamen] " +
                                     "order by GroepNr";
            
            //het gebruik van 'using' zorgt ervoor dat de connectie automatisch wordt gesloten na gebruik --> 'IDisposable'
            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open(); //Openen van de SqlConnection
                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    using (SqlDataReader oReader = oCommand.ExecuteReader()) //uitvoeren van de Query en resultaat in reader plaatsen
                    {
                        foreach (IDataRecord r in oReader)
                        {
                            string sNaam = r["GroepNaam"].ToString();
                            int iGroepNr = Convert.ToInt32(r["GroepNr"].ToString());

                            clsMachine oGroup = new clsMachine(sNaam, iGroepNr);
                            lstGroups.Add(oGroup);
                        }
                    }
                }
            }
            return lstGroups;
        }
               
        //LEVEL
        public List<clsMachine> GetLevels(int iGroepNr)
        {
            List<clsMachine> lstLevels = new List<clsMachine>();

            string sGroepNr = iGroepNr.ToString();

            //Invullen van het SQL commando
            string sCmdText = "SELECT [GroepNr], [LevelNr] ,[LevelNaam] " +
                                     "FROM [dbo].[tblLevelNamen] " +
                                     "where GroepNr = " + sGroepNr +
                                     "order by LevelNr";

            //het gebruik van 'using' zorgt ervoor dat de connectie automatisch wordt gesloten na gebruik --> 'IDisposable'
            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open(); //Openen van de SqlConnection

                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    using (SqlDataReader oReader = oCommand.ExecuteReader()) //uitvoeren van de Query en resultaat in reader plaatsen
                    {
                        foreach (IDataRecord r in oReader)
                        {
                            string sNaam = r["LevelNaam"].ToString();
                            int iLevelNr = Convert.ToInt32(r["LevelNr"].ToString());

                            lstLevels.Add(new clsMachine(sNaam, iGroepNr, iLevelNr));
                        }
                    }
                }
            }
            return lstLevels;
        }

        //SUBGROEP
        public List<clsMachine> GetSubgroups(int iGroupNr, int iLevelNr)
        {
            List<clsMachine> lstSubgroups = new List<clsMachine>();
            

            string sGroupNr = iGroupNr.ToString();
            string sLevelNr = iLevelNr.ToString();

            //https://stackoverflow.com/questions/5021693/distinct-for-only-one-column/5021739
            //Invullen van het SQL commando
            string sCmdText = "SELECT [Trend_SubGroepNr], [Trend_SubGroepNaam], [Offset], [Trend_GroepNr] " +
                                     "FROM ( " +
                                     "SELECT [Trend_SubGroepNr], [Trend_SubGroepNaam],  [Offset], [Trend_GroepNr], " +
                                     "ROW_NUMBER() OVER(PARTITION BY Trend_SubgroepNaam ORDER BY Trend_SubgroepNr ASC) rn " +
                                     "FROM [dbo].[tblSubGroepNamen] " +
                                     $"WHERE GroepNr = {iGroupNr} AND LevelNr = {iLevelNr} " +
                                     ") a " +
                                     "WHERE rn = 1";

            //het gebruik van 'using' zorgt ervoor dat de connectie automatisch wordt gesloten na gebruik --> 'IDisposable'
            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open(); //Openen van de SqlConnection

                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    using (SqlDataReader oReader = oCommand.ExecuteReader()) //uitvoeren van de Query en resultaat in reader plaatsen
                    {
                        foreach (IDataRecord r in oReader)
                        {
                            string sNaam = r["Trend_SubGroepNaam"].ToString();
                            int iSubgroepNr = Convert.ToInt32(r["Trend_SubGroepNr"].ToString());
                            int iOffset = Convert.ToInt32(r["Offset"].ToString());
                            int iTrendGroepNr = Convert.ToInt32(r["Trend_GroepNr"].ToString());

                            lstSubgroups.Add(new clsMachine(sNaam, iGroupNr, iLevelNr, iSubgroepNr, iOffset, iTrendGroepNr));

                        }                        
                    }
                }
            }
            return lstSubgroups;
        }

        //TRENDGROEP
        public Dictionary<int,string> GetTrendgroups(int iGroupNr, int iLevelNr, int iSubgroupNr)
        {
            string sGroupNr = iGroupNr.ToString();
            string sLevelNr = iLevelNr.ToString();
            string sSubgroupNr = iSubgroupNr.ToString();

            //Invullen van het SQL commando
            string sCmdText = "SELECT DISTINCT [Trend_GroepNr], [Trend_GroepNaam] " +
                              "FROM [dbo].[tblSubGroepNamen] " +
                              "WHERE [GroepNr] = " + sGroupNr;
            if (iLevelNr > 0)
            {
                sCmdText = sCmdText + " and [LevelNr] = " + sLevelNr;
                if (iSubgroupNr > 0)
                {
                    sCmdText = sCmdText + " and [Trend_SubGroepNr] = " + sSubgroupNr;                   
                }
            }


            Dictionary<int, string> dcTrendgroepen = new Dictionary<int, string>();
            dcTrendgroepen.Add(0, "Alles*");

            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open();
                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    using (SqlDataReader oReader = oCommand.ExecuteReader()) 
                    {
                        foreach (IDataRecord r in oReader)
                        {
                            int iTG = Convert.ToInt32(r["Trend_GroepNr"].ToString());
                            string sTG = (r["Trend_GroepNaam"].ToString());

                            dcTrendgroepen.Add(iTG, sTG);
                        }
                    }
                }
            }
            return dcTrendgroepen;
        }

        ////Het ophalen van de mogelijke Items in de gekozen groep, level of subgroep/trendgroup Via procedure       
        public List<clsSvgLink> GetItems(int iGroupNr, int iLevelNr, int iSubGroepNr, int iTrendGroepNr)
        {
            string sGroupNr = iGroupNr.ToString();
            string sLevelNr = iLevelNr.ToString();
            string sSubgroupNr = iSubGroepNr.ToString();
            string sTrendGroupNr = iTrendGroepNr.ToString();
            
            //Invullen van het SQL commando
            string sCmdText = "[dbo].[ReadTblTrendTags_Qlik]";

            List<clsSvgLink> lstItems = new List<clsSvgLink>(); //<Naam, Nummer>

            //het gebruik van 'using' zorgt ervoor dat de connectie automatisch wordt gesloten na gebruik --> 'IDisposable'
            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open(); //Openen van de SqlConnection

                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    using (SqlDataReader oReader = oCommand.ExecuteReader()) //uitvoeren van de Query en resultaat in reader plaatsen
                    {
                        if (iTrendGroepNr > 0)
                        {
                            foreach (IDataRecord r in oReader)
                            {
                                if (r["GroepNr"].ToString() == sGroupNr & r["LevelNr"].ToString() == sLevelNr & r["Trend_SubGroepNr"].ToString() == sSubgroupNr & r["Trend_GroepNr"].ToString() == sTrendGroupNr)
                                {
                                    clsSvgLink oItem = new clsSvgLink();
                                    oItem.Name = r["Naam"].ToString();
                                    oItem.TrendNr = r["TrendNr_2"].ToString();

                                    lstItems.Add(oItem);
                                }                               
                            }
                        }
                        else if (iSubGroepNr > 0)
                        {
                            foreach (IDataRecord r in oReader)
                            {
                                if (r["GroepNr"].ToString() == sGroupNr & r["LevelNr"].ToString() == sLevelNr & r["Trend_SubGroepNr"].ToString() == sSubgroupNr)
                                {
                                    clsSvgLink oItem = new clsSvgLink();
                                    oItem.Name = r["Naam"].ToString();
                                    oItem.TrendNr = r["TrendNr_2"].ToString();

                                    lstItems.Add(oItem);
                                }
                            }
                        }
                        else if (iLevelNr > 0)
                        {
                            foreach (IDataRecord r in oReader)
                            {
                                if (r["GroepNr"].ToString() == sGroupNr & r["LevelNr"].ToString() == sLevelNr)
                                {
                                    clsSvgLink oItem = new clsSvgLink();
                                    oItem.Name = r["Naam"].ToString();
                                    oItem.TrendNr = r["TrendNr_2"].ToString();

                                    lstItems.Add(oItem);
                                }
                            }
                        }
                        else if (iGroupNr > 0)
                        {
                            foreach (IDataRecord r in oReader)
                            {
                                if (r["GroepNr"].ToString() == sGroupNr)
                                {
                                    clsSvgLink oItem = new clsSvgLink();
                                    oItem.Name = r["Naam"].ToString();
                                    oItem.TrendNr = r["TrendNr_2"].ToString();

                                    lstItems.Add(oItem);
                                }
                            }
                        }                       
                    }
                }
            }
            return lstItems;
        }

        public Dictionary<string, string> GetTagNames(List<string> lstTrendNrs)
        {
            //Invullen van het SQL commando
            string sCmdText = "[dbo].[ReadTblTrendTags_Qlik]";

            Dictionary<string, string> dcTrendNr2TagName = new Dictionary<string, string>();

            //het gebruik van 'using' zorgt ervoor dat de connectie automatisch wordt gesloten na gebruik --> 'IDisposable'
            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open(); //Openen van de SqlConnection

                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    using (SqlDataReader oReader = oCommand.ExecuteReader()) //uitvoeren van de Query en resultaat in reader plaatsen
                    {
                        foreach (IDataRecord r in oReader)
                        {
                            if (lstTrendNrs.Any(sNr => sNr == r["TrendNr_2"].ToString()))
                            {
                                dcTrendNr2TagName.Add(r["TrendNr_2"].ToString(), r["Naam"].ToString());
                            }
                        }
                    }
                }
                return dcTrendNr2TagName;
            }
        }

        //###############################################################################################
        //Ophalen van de items die aan de SVG gelinkt zijn
        public List<clsSvgLink> GetLinkedItems(string sSvgName)
        {
            //Invullen van het SQL commando
            string sCmdText = "SELECT [tblSvgs].[TrendNr_2], [tblSvgs].[mID], [tblSvgs].[Name] " +
                                     "FROM [dbo].[tblSvgs] " +
                                     $"WHERE [tblSvgs].[SVG] = '{sSvgName}'";

            List<clsSvgLink> lstItems = new List<clsSvgLink>(); //<Naam, Nummer>

            //het gebruik van 'using' zorgt ervoor dat de connectie automatisch wordt gesloten na gebruik --> 'IDisposable'
            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open(); //Openen van de SqlConnection

                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    using (SqlDataReader oReader = oCommand.ExecuteReader()) //uitvoeren van de Query en resultaat in reader plaatsen
                    {
                        foreach (IDataRecord r in oReader)
                        {
                            clsSvgLink oItem = new clsSvgLink();
                            oItem.TrendNr = r["TrendNr_2"].ToString();
                            try
                            {
                                oItem.mID = r["mID"].ToString().Replace(" ", "");
                                oItem.Name = r["Name"].ToString();
                            }
                            catch (Exception)
                            { }
                            lstItems.Add(oItem);
                        }
                    }
                }
            }
            return lstItems;
        }

        //Met procedure
        public List<clsItemData> GetItemValues(List<string> lstTrendNrs)
        {
            //Invullen van het SQL commando
            string sCmdText = "[dbo].[ReadTblTrendTags_Qlik]";

            Dictionary<string, string> dcTrendNr2TagName = new Dictionary<string, string>();

            //het gebruik van 'using' zorgt ervoor dat de connectie automatisch wordt gesloten na gebruik --> 'IDisposable'
            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open(); //Openen van de SqlConnection

                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    using (SqlDataReader oReader = oCommand.ExecuteReader()) //uitvoeren van de Query en resultaat in reader plaatsen
                    {
                        List<clsItemData> oItems = new List<clsItemData>();

                        foreach (IDataRecord r in oReader)
                        {

                            if (lstTrendNrs.Any(sNr => sNr == r["TrendNr_2"].ToString()))
                            {
                                clsItemData oItem = new clsItemData();

                                oItem.TrendNr = r["TrendNr_2"].ToString();
                                oItem.Naam = r["Naam"].ToString();
                                oItem.Value = r["Value"].ToString();
                                oItem.Eenheid = r["Eenheid"].ToString();
                                oItem.MaxGrens = r["MaxGrens"].ToString();
                                oItem.MinGrens = r["MinGrens"].ToString();
                                oItem.KleurNr = r["KleurNr"].ToString();

                                oItem.Machine = r["Machine"].ToString();

                                oItems.Add(oItem);
                            }
                        }
                        return oItems;
                    }
                }                                
            }
            return new List<clsItemData>();
        }


        //public List<clsItemData> GetItemValues(List<string> oTrendNrs)
        //{
        //    string sTrendNrs = "";
        //    foreach (string sTrendNr in oTrendNrs)
        //    {
        //        sTrendNrs = sTrendNrs + ", " + sTrendNr;
        //    }

        //    //Als er items zijn
        //    if (sTrendNrs.Count() > 0)
        //    {
        //        //eerste komma verwijderen
        //        sTrendNrs = sTrendNrs.Remove(0, 1);

        //        //Invullen van het SQL commando
        //        string sCmdText = "SELECT [TrendNr_2],[Naam],[Value],[Eenheid],[MaxGrens],[MinGrens],[KleurNr], [Machine]" +
        //                          "FROM [dbo].[tblTrendTags_QLIK]" +
        //                          $"WHERE[TrendNr_2] IN ({sTrendNrs})";

        //        //het gebruik van 'using' zorgt ervoor dat de connectie automatisch wordt gesloten na gebruik --> 'IDisposable'
        //        using (SqlConnection oConnection = _SqlConnection())
        //        {
        //            oConnection.Open(); //Openen van de SqlConnection

        //            using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
        //            {
        //                using (SqlDataReader oReader = oCommand.ExecuteReader()) //uitvoeren van de Query en resultaat in reader plaatsen
        //                {
        //                    List<clsItemData> oItems = new List<clsItemData>();

        //                    foreach (IDataRecord r in oReader)
        //                    {
        //                        clsItemData oItem = new clsItemData();

        //                        oItem.TrendNr = r["TrendNr_2"].ToString();
        //                        oItem.Naam = r["Naam"].ToString();
        //                        oItem.Value = r["Value"].ToString();
        //                        oItem.Eenheid = r["Eenheid"].ToString();
        //                        oItem.MaxGrens = r["MaxGrens"].ToString();
        //                        oItem.MinGrens = r["MinGrens"].ToString();
        //                        oItem.KleurNr = r["KleurNr"].ToString();

        //                        oItem.Machine = r["Machine"].ToString();

        //                        oItems.Add(oItem);
        //                    }
        //                    return oItems;
        //                }
        //            }
        //        }
        //    }
        //    return new List<clsItemData>();
        //}



        public void AddLinkToDB(string sSvg, string sTrendNr, string sMId, string sTagName)
        {
            string sCmdText = "INSERT INTO [dbo].[tblSvgs]" +
                                "([SVG], [TrendNr_2], [mID], [Name])" +
                                $"VALUES ('{sSvg}', '{sTrendNr}', '{sMId}', '{sTagName}')";

            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open(); //Openen van de SqlConnection

                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    oCommand.ExecuteNonQuery();
                }
            }
        }

        public void AddTagName2TrendNr(string sSvgName, Dictionary<string,string> Trend2TagName)
        {
            foreach (KeyValuePair<string, string> kp in Trend2TagName)
            {
                string sCmdText = "UPDATE [dbo].[tblSvgs]" +
                                 $"SET [Name] = '{kp.Value}'" +
                                 $"WHERE [SVG] = '{sSvgName}' AND [TrendNr_2] = '{kp.Key}'";

                using (SqlConnection oConnection = _SqlConnection())
                {
                    oConnection.Open(); //Openen van de SqlConnection

                    using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                    {
                        oCommand.ExecuteNonQuery();
                    }
                }
            }
        }

        public void AddMId2TrendNr(string sSvgName, Dictionary<string,string> Trend2mID)
        {
            foreach  (KeyValuePair<string,string> kp in Trend2mID)
            {
                string sCmdText = "UPDATE TOP (1) [dbo].[tblSvgs]" +
                                 $"SET [mID] = '{kp.Value}'" +
                                 $"WHERE [SVG] = '{sSvgName}' AND [TrendNr_2] = '{kp.Key}'";

                using (SqlConnection oConnection = _SqlConnection())
                {
                    oConnection.Open(); //Openen van de SqlConnection

                    using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                    {
                        oCommand.ExecuteNonQuery();
                    }
                }
            }              
        }

        public void RemoveLinkFromDB(string sSvg, string mID)
        {
            string sCmdText = "DELETE TOP (1)" + //only delete 1
                                "FROM [dbo].[tblSvgs]" +
                                $"WHERE [SVG] = '{sSvg}'" + 
                                $"AND [mID] = '{mID}'";

            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open(); //Openen van de SqlConnection

                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    oCommand.ExecuteNonQuery();
                }
            }
        }


        //#################################################################################################################

        public Dictionary<string, List<string>> GetDataXY(int iUren, int iTrendNr2)
        {
            //Berekenen van de starttijd (afhankelijk van gewenst bereik)
            TimeSpan timeDiff = new TimeSpan(iUren, 0, 0);
            DateTime timeNow = DateTime.Now;
            DateTime timeStart = timeNow - timeDiff;

            return GetDataXY(timeStart, timeNow, iTrendNr2);
        }


        //Data ophalen voor het maken van een trend
        public Dictionary<string, List<string>> GetDataXY(DateTime dtStart, DateTime dtEnd, int iTrendNr2)
        {            
            Dictionary<string, List<string>> oDataXY = new Dictionary<string, List<string>>();      
                       
            string sStartTime = dtStart.ToString("yyyy-MM-dd HH:mm:ss.fff"); //2019-02-15 08:00:10.000 ==> correct format
            string sEndTime = dtEnd.ToString("yyyy-MM-dd HH:mm:ss.fff");

            string sTrendGroep;

            if (iTrendNr2 > 999999)
            {

                sTrendGroep = iTrendNr2.ToString().Substring(1,3);
            }
            else
            {

                sTrendGroep = iTrendNr2.ToString().Substring(0,3);
            }


            //Invullen van het SQL commando
            string sCmdText = "SELECT [Waarde] ,[DatumTijd] " +
                                     $"FROM [dbo].[TrendGroep_{sTrendGroep}] " +
                                     "where TrendNr_2 = " + iTrendNr2 +
                                     "and DatumTijd > '" + sStartTime + "' " +
                                     "and DatumTijd < '" + sEndTime + "' " +
                                     "order by DatumTijd";

            //het gebruik van 'using' zorgt ervoor dat de connectie automatisch wordt gesloten na gebruik --> 'IDisposable'
            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open(); //Openen van de SqlConnection

                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    using (SqlDataReader oReader = oCommand.ExecuteReader()) //uitvoeren van de Query en resultaat in reader plaatsen
                    {
                        //XY data extracten
                        var list = (from IDataRecord r in oReader
                                    select new
                                    {
                                        DatumTijd = (string)r["DatumTijd"].ToString(),
                                        Waarde =  (string)r["Waarde"].ToString()
                                    }).ToList();

                        List<string> DataX = list.Select(r => r.DatumTijd).ToList();
                        List<string> DataY = list.Select(r => r.Waarde).ToList();

                        oDataXY.Add("X", DataX);
                        oDataXY.Add("Y", DataY);
                    }
                }
            }
            return oDataXY;
        }

        
        //Execute OEE procedure
        public string GetOEE(string sTrendNr2, DateTime oBegin, DateTime oEind)
        {
            string sValue = "";
            string sBegin = oBegin.ToString("yyyy-MM-dd HH:mm:ss");
            string sEind = oEind.ToString("yyyy-MM-dd HH:mm:ss");

            //Invullen van het SQL commando
            string sCmdText = "[dbo].[ReadOEE_TrendNr_2]";

            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open();

                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@TrendNr_2", sTrendNr2);
                    oCommand.Parameters.AddWithValue("@BeginTijd", sBegin);
                    oCommand.Parameters.AddWithValue("@EindTijd", sEind);

                    var oReturn = oCommand.Parameters.Add("@OEE", SqlDbType.Float);
                    oReturn.Direction = ParameterDirection.Output;

                    oCommand.ExecuteNonQuery();

                    sValue = oReturn.Value.ToString();                
                }
            }
            return sValue;
        }

        //#################################################################################################################

        //Voor simulatie
        public void UpdateDbValues(string sTrendNr, string sValue, string sColor)
        {
            string sCmdText = "UPDATE [ProcesDataTuft].[dbo].[tblTrendTags_QLIK]" +
                    $"SET [Value] = '{sValue}', [KleurNr] = '{sColor}'" +
                    $"WHERE [TrendNr_2] = '{sTrendNr}'";

            using (SqlConnection oConnection = _SqlConnection())
            {
                oConnection.Open(); //Openen van de SqlConnection

                using (SqlCommand oCommand = new SqlCommand(sCmdText, oConnection))
                {
                    oCommand.ExecuteNonQuery();
                }
            }
        }

    }
}
