﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BEC
{
    public class clsSvgGroep
    {
        private XElement _oXgroup;
        private XNamespace _oNS;

        //constructor
        public clsSvgGroep()
        {
            
        }
        public clsSvgGroep(XElement oXel)
        {
            _oXgroup = oXel;
            _oNS = oXel.Document.Root.GetDefaultNamespace();
        }
        
        //propertie
        public string mID
        {
            get { return _oXgroup.Attribute("mID").Value; }
            //set { _xElement.Attribute("mID").Value = value; }
        }
        public string TrendTag
        {
            get { return _oXgroup.Attribute("id").Value; }        
        }
        public XElement GroupElement
        {
            get
            {
                return _oXgroup;
            }

            set
            {
                _oXgroup = value;
            }
        }

        public bool IsRectangle()
        {
            return _oXgroup.Elements(_oNS + "rect").Any();
        }
        
        public bool IsProgressBar()
        {
            return _oXgroup.Attributes("ProgressBar").Any();
        }
        public string ProgressBarDirection()
        {
            return _oXgroup.Attribute("ProgressBar").Value;
        }
        public bool IsText()
        {
            return _oXgroup.Elements(_oNS + "text").Any();
        }
        public bool IsOEE()
        {
            return _oXgroup.Attributes("OEE").Any();
        }

        //################################################################################
        //ADMIN

        //Link TrendTag
        public void SetTrendTag(string sTrendNr, string sTrendName)
        {
            _SetClass();
            _oXgroup.SetAttributeValue("id", sTrendNr);
            _oXgroup.SetElementValue(_oNS + "title", sTrendName);

            List<XElement> lstTitles = _oXgroup.Descendants(_oNS + "title").ToList();

            foreach (XElement oXel in lstTitles)
            {
                oXel.Value = sTrendName;
            }
        }
        public void RemoveTrendTag()
        {
            _ResetClass();
            _oXgroup.SetAttributeValue("id", "-1");
            _oXgroup.SetElementValue(_oNS + "title", "");

            List<XElement> lstTitles = _oXgroup.Descendants(_oNS + "title").ToList();

            foreach (XElement oXel in lstTitles)
            {
                oXel.Value = "";
            }
        }
        public string GetTagName()
        {
            return _oXgroup.Element(_oNS + "title").Value;
        }

        //de class wordt 'Qstx' voor ELK visueel element binnen deze SvgGroep (voor QLIK)
        private void _SetClass()
        {
            //Een lijst maken van alle child elementen die het 'class' attribuut bevatten
            List<XElement> lstElements = _oXgroup.Descendants().Where(El => El.Attributes("class").Any()).ToList();

            foreach (XElement Xel in lstElements)
            {
                string sClass = Xel.Attributes("class").FirstOrDefault().Value;

                //Is de klasse al ingesteld op een Qlik klasse?
                if (sClass.Contains("Q"))
                {
                    //Niet opnieuw instellen
                }
                else
                {
                    sClass = "Q" + sClass;
                    Xel.SetAttributeValue("class", sClass);

                    //behouden van de kleur mogelijk door deze in het group-element te plaatsen ipv de css-stijl
                    //https://parrot-tutorial.com/jsref/elem_style.html

                    //Xel.SetAttributeValue("style", "fill: #ffff00;");
                    //string sStyles = _oXelement.Document.Descendants(_oNS + "style").First().Value;
                }
            }
        }
        private void _ResetClass()
        {
            List<XElement> lstElements = _oXgroup.Descendants().Where(El => El.Attributes("class").Any()).ToList();

            foreach (XElement Xel in lstElements)
            {
                string sClass = Xel.Attributes("class").FirstOrDefault().Value;
                sClass = sClass.Replace("Q", "");
                Xel.SetAttributeValue("class", sClass);
            }
        } 

        //Set 'Selected' fiter
        public void Glow()
        { 
            _oXgroup.SetAttributeValue("filter", "url(#Glow)");
        }
        public void NoGlow()
        {
            _oXgroup.SetAttributeValue("filter", null);
        }

        /// <summary>
        /// Returns false if the operation failed
        /// </summary>
        /// <returns></returns>
        public bool MakeProgressBar(string sDirection)//, int iMinValue, int iMaxValue
        {
            //indien de groep al een PB is
            if (_oXgroup.Attributes("ProgressBar").Any())
            {
                this.RemoveProgressBar();
            }

            //Om een progressbar te maken moet de SVG-group één 'rect'-element bezitten
            try
            {               
                _oXgroup.SetAttributeValue("ProgressBar", sDirection);

                //de rechthoek in het group element zoeken
                XElement oSvgRect = _oXgroup.Elements(_oNS + "rect").First();

                //een kopie maken
                XElement oSvgProgressBar = new XElement(oSvgRect);
                oSvgProgressBar.SetAttributeValue("PB", true);
                oSvgProgressBar.SetAttributeValue("style", "fill:#ffffff");//witte kleur

                if (sDirection == "Up")
                {
                    oSvgProgressBar.SetAttributeValue("height", "0");//hoogte instellen     
                }
                else
                {
                    oSvgProgressBar.SetAttributeValue("width", "0");//breedte instellen    
                }
                    

                //de witte rechthoek toevoegen 
                oSvgRect.AddAfterSelf(oSvgProgressBar);
                oSvgRect.AddAfterSelf(Environment.NewLine);

                return true;
            }
            catch (Exception)
            {
                return false;
            }           
            
        }
    
      
        public void RemoveProgressBar()
        {
            if (_oXgroup.Attributes("ProgressBar").Any())
            {
                //tag uit groep element verwijderen
                _oXgroup.SetAttributeValue("ProgressBar", null);

                List<XElement> lstPbRects = _oXgroup.Elements(_oNS + "rect").ToList();
                foreach (XElement oRect in lstPbRects)
                {
                    if (oRect.Attributes("PB").Any())
                    {
                        oRect.Remove();
                    }
                }
            }
        }


        public void MakeOEE()
        {
            //Het OEE attribuut toevoegen aan het element            
                _oXgroup.SetAttributeValue("OEE", true);      
                         

        }
        public void RemoveOEE()
        {
            if (_oXgroup.Attribute("OEE").Value == "true")
            {
                //tag uit groep element verwijderen
                _oXgroup.SetAttributeValue("OEE", null);

            }
        }

        //###########################################################################
        //User
                
        public void UpdateText(string sText)
        {           
            _oXgroup.SetElementValue(_oNS + "text", sText);
        }
               
        public void UpdateColor(string sColorCode)
        {
            _oXgroup.SetAttributeValue("fill", sColorCode);
        }

        public void UpdateOEE(string sOeeValue)
        {
            if (_oXgroup.Attributes("OEE").Any())
            {
                //Als de SVG group een tekst element bevat
                if (_oXgroup.Elements(_oNS + "text").Any())
                {
                    //de tekst aanpassen
                    _oXgroup.SetElementValue(_oNS + "text", sOeeValue);
                }
                //Als de SVG group een figuur element bevat
                else
                {
                    //Nieuw tekst element aanmaken
                    XElement oTekstElement = new XElement(_oNS + "text", sOeeValue);

                //ANDERE FIGUREN
                //https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Basic_Shapes

                    //Rechthoek
                    try
                    {
                        XElement oRect = _oXgroup.Elements().First(Elem => Elem.Name == _oNS + "rect");

                        //begin de tekst aan de linkerkant van de bijhorende figuur
                        string sPosX = oRect.Attribute("x").Value;
                        oTekstElement.SetAttributeValue("x", sPosX);

                        //plaatst de tekst boven de figuur 
                        string sPosY = oRect.Attribute("y").Value;

                        ////als de tekst in de figuur moet komen (op de onderzijde van de figuur)
                        string sHeight = oRect.Attribute("height").Value;
                        double dPosY = double.Parse(sPosY.Replace(".", ","), System.Globalization.NumberStyles.Any);
                        double dHeight = double.Parse(sHeight.Replace(".", ","), System.Globalization.NumberStyles.Any);
                        sPosY = (dPosY + dHeight).ToString("0.###").Replace(",", ".");

                        oTekstElement.SetAttributeValue("y", sPosY);
                    }
                    catch (Exception)
                    {
                        return; //bij een return wordt de verdere code niet meer uitgevoerd
                    }

                    //tekst stijl toevoegen aan het nieuwe element
                    oTekstElement.SetAttributeValue("style", "fill:#000000;fill-opacity:1.0;font-family:Arial;font-size:100%");

                    //tekst element toevoegen aan het groep element
                    XElement oLastElement = _oXgroup.Elements().Last();
                    oLastElement.AddAfterSelf(oTekstElement);
                }
            }
        }
              

        public void UpdateProgressBar(int iProcent)
        {
            if (_oXgroup.Attributes("ProgressBar").Any())
            {
                string sDir = this.ProgressBarDirection();

                //witruimte berekenen  = omgekeerd %
                iProcent = 100 - iProcent;

                //Originele rechthoek vinden
                //de rechthoek in het group element zoeken
                XElement oSvgRect = _oXgroup.Elements(_oNS + "rect").First();

                string sMaxHeight = oSvgRect.Attribute("height").Value;
                string sMaxWidth = oSvgRect.Attribute("width").Value;
                

                //Soms werkt de conversie alleen met ',' en niet met '.'
                double dWidth;
                double dHeight;

                System.Globalization.CultureInfo cInfo = System.Globalization.CultureInfo.InvariantCulture;

                double.TryParse(sMaxWidth, System.Globalization.NumberStyles.Number, cInfo, out dWidth);
                double.TryParse(sMaxHeight, System.Globalization.NumberStyles.Number, cInfo, out dHeight);
                
                //Hoogte berekenen
                dHeight = dHeight / 100;
                dHeight = dHeight * iProcent;

                //Breedte berekenen
                double dWidthPB = dWidth / 100;
                dWidthPB = dWidthPB * iProcent;

                if (dHeight < 0)
                {
                    dHeight = 0;
                }
                if (dWidthPB < 0)
                {
                    dWidthPB = 0;
                }

                //Horizontale verplaatsing berekenen
                double dShift = dWidth -  dWidthPB;

                //ProgressBar element vinden 
                XElement oSvgProgressBar = _oXgroup.Elements(_oNS + "rect")
                    .First(rect => rect.Attributes("PB").Any());

                if (sDir == "Up")
                {
                    //hoogte instellen 
                    string sHeight = dHeight.ToString().Replace(",",".");
                    oSvgProgressBar.SetAttributeValue("height", sHeight);
                }
                else if (sDir == "Left")
                {
                    //Breedte instellen 
                    string sWidth = dWidthPB.ToString().Replace(",", ".");
                    oSvgProgressBar.SetAttributeValue("width", sWidth);
                }
                else if (sDir == "Right")
                {
                    //Breedte instellen 
                    string sWidth = dWidthPB.ToString().Replace(",", ".");
                    oSvgProgressBar.SetAttributeValue("width", sWidth);

                    //Startpunt verplaatsen
                    string sX = oSvgRect.Attribute("x").Value;
                    double dX;
                    double.TryParse(sX, System.Globalization.NumberStyles.Number, cInfo, out dX);

                    dX = dX + dShift;

                    sX = dX.ToString().Replace(",", ".");

                    oSvgProgressBar.SetAttributeValue("x", sX);
                }
            }
        }
    }
}
