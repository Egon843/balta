﻿using System.Collections.Generic;

namespace BEC
{
    
    public class clsMachine
    {
        private string _sName;
        private int _iGroup;
        private int _iLevel = 0;
        private int _iSubgroup = 0;
         
        //for generating
        private int _iOffset = 0;
        private int _iTrend_GroepNr = 0;
      
        //constructor
        public clsMachine()
        { }
        public clsMachine(string sName, int iGroup)
        {
            _sName = sName;
            _iGroup = iGroup; 
        }
        public clsMachine(string sName, int iGroup, int iLevel)
        {
            _sName = sName;
            _iGroup = iGroup;
            _iLevel = iLevel;
        }
        public clsMachine(string sName, int iGroup, int iLevel, int iSubgroup, int iOffset, int iTrendgroup)
        {
            _sName = sName;
            _iGroup = iGroup;
            _iLevel = iLevel;
            _iSubgroup = iSubgroup;
            _iOffset = iOffset;
            _iTrend_GroepNr = iTrendgroup;
        }

        //properties
        public string Name
        {
            get
            {
                return _sName;
            }
        }
        public int GroupNr
        {
            get { return _iGroup; }
        }
        public int LevelNr
        {
            get { return _iLevel; }
        }
        public int SubgroupNr
        {
            get { return _iSubgroup; }
        }

        public int Offset
        {
            get { return _iOffset; }
        }

        public int TrendgroupNr
        {
            get
            { return _iTrend_GroepNr; }            
        }

        public override string ToString()
        {
            return _sName;
        }

        //De bestandsnaam voor het SVG-bestand van deze machine
        public string GenerateSvgFileName()
        {
            return "svg" + _iGroup.ToString() + _iLevel.ToString() + "_" + _iSubgroup.ToString() + ".svg";
        }

    }
}
