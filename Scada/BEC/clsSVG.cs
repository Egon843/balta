﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BEC
{
    public class clsSvg
    {
        private string _sFileName;
        private List<clsSvgGroep> _lstSvgGroups;
        private List<string> _lstLinked_mIds = new List<string>();

        private XDocument _oSvgDoc;
        private XNamespace _Ns;           
        
        //constructor
        public clsSvg()
        { }
        public clsSvg(XDocument oSvgFile, string sFileName)
        {
            _sFileName = sFileName;
            _oSvgDoc = new XDocument(oSvgFile);
            _Ns = _oSvgDoc.Root.GetDefaultNamespace().ToString();
            _lstSvgGroups = new List<clsSvgGroep>();

            //lijst maken van SvgGroup & linked mID's
            XElement oMainGroup = _oSvgDoc.Root.Elements(_Ns + "g").First();
            foreach (XElement oXel in oMainGroup.Elements(_Ns + "g"))
            {
                clsSvgGroep oSvgGroup = new clsSvgGroep(oXel);
                _lstSvgGroups.Add(oSvgGroup);

                if (oSvgGroup.TrendTag.Length > 5)
                {
                    _lstLinked_mIds.Add(oSvgGroup.mID);
                }
            }
        }          
                
        public XDocument SvgDocument
        {
            get { return _oSvgDoc; }
        }     
        public List<clsSvgGroep> SvgGroups
        {
            get
            {
                return _lstSvgGroups;
            }

            set
            {
                _lstSvgGroups = value;
            }
        }
        public List<string> Linked_mIds
        {
            get
            {
                return _lstLinked_mIds;
            }

            set
            {
                _lstLinked_mIds = value;
            }
        }
        public string FileName
        {
            get { return _sFileName; }
            set { _sFileName = value; }
        }


        //Deze string wordt in de webbrowser gebruikt om de svg weer te geven
        public string GetXmlString()
        {
            string sXmlDeclaration = _oSvgDoc.Declaration.ToString();
            string sSVG = sXmlDeclaration + _oSvgDoc.ToString();

            return sSVG;
        }

        public clsSvgGroep GetSvgGroup(string mID)
        {
            return this.SvgGroups.Find(oGroup => oGroup.mID == mID);
        }

        //Item linken
        public void MakeLink(string mID, string sTrendNr, string sTagName)
        {           
            //TrendTag aan de SvgGroup toewijzen
            clsSvgGroep oSvgGroup = this.SvgGroups.Find(group => group.mID == mID);
            oSvgGroup.SetTrendTag(sTrendNr, sTagName);                           
        }
        public void RemoveLink(string mID)
        {
            _lstLinked_mIds.Remove(mID);

            try
            {
                clsSvgGroep oSvgGroup = _lstSvgGroups.Find(gr => gr.mID == mID);
                oSvgGroup.RemoveTrendTag();
                oSvgGroup.RemoveOEE();
                oSvgGroup.RemoveProgressBar();
            }
            catch (Exception)
            {         
                       
            }            
        }

        //Highlight SvgGroup
        public void GlowGroup(string mID)
        {
            RemoveAllGlow();

            clsSvgGroep oSelectedGroup = _lstSvgGroups.Find(oSvgGr => oSvgGr.mID == mID);
            oSelectedGroup.Glow();
        }
        //Indien meerdere SVG elementen naar hetzelfde trendnummer verwijzen
        public void GlowGroups(string sTrendNr)
        {
            RemoveAllGlow();

            List<clsSvgGroep> lstGroups = _lstSvgGroups.FindAll(gr => gr.TrendTag == sTrendNr);

            foreach (clsSvgGroep oGr in lstGroups)
            {
                oGr.Glow();
            }
        }
        public void RemoveAllGlow()
        {
            foreach (clsSvgGroep oSvgGr in _lstSvgGroups)
            {
                oSvgGr.NoGlow();
            }
        }

        //public void ModifyAdmin(string sUserName)
        //{
        //    _oSvgDoc.AddFirst(new XComment($"This file is modified by {sUserName} /n with 'Balta Factory Visualisation'"));
        //}

        //###########################################################
        //USER

        public void UpdateValues(List<clsItemData> oItems)
        {
            try
            {
                foreach (string mID in _lstLinked_mIds)
                {
                    clsSvgGroep oGroup = _lstSvgGroups.Find(gr => gr.mID == mID);
                    string sTrendNr = oGroup.TrendTag;

                    clsItemData oLinkedItem = oItems.Find(item => item.TrendNr == sTrendNr);

                    //TextWaarde
                    string sTextValue = oLinkedItem.Value + " [" + oLinkedItem.Eenheid + "]";
                    //sTextValue = oLinkedItem.Naam + "/n" + sTextValue;

                    if (oGroup.IsOEE())
                    {
                        //waarde niet invullen als de group de OEE voorstelt
                    }
                    else
                    {
                        oGroup.UpdateText(sTextValue);
                    }

                    //Kleur
                    string sColorValue = oLinkedItem.KleurNr;
                    oGroup.UpdateColor(sColorValue);

                    //ProgressBar
                    try
                    {
                        double dValue = Convert.ToDouble(oLinkedItem.Value);
                        double dMin = 0;
                        double dMax = Convert.ToDouble(oLinkedItem.MaxGrens);
                        double dProcent = Math.Round(((dValue - dMin) / (dMax - dMin)) * 100);
                        int iProcent = Convert.ToInt32(dProcent);
                        oGroup.UpdateProgressBar(iProcent);
                    }
                    catch (Exception)
                    {

                    }

                    //OEE wordt hier niet geupdate, omdat deze waarde afhankelijk is van een start en eind tijd.
                    //daarvoor wordt de procedure gebruikt
                }
            }
            catch (Exception)
            {
                
            }
          
        }
    }
}
