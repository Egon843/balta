﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BEC
{
    public class clsSvgLink
    {
        private string _sName;
        private string _sTrendNr;
        private string _sLinkedMId;

        //Constructor
        public clsSvgLink()
        {

        }
        public clsSvgLink(string mID, string sTrendNr)
        {
            _sLinkedMId = mID;
            _sTrendNr = sTrendNr;
        }
        public clsSvgLink(clsSvgLink oItem)
        {
            _sTrendNr = oItem.TrendNr;
            _sName = oItem.Name;
            _sLinkedMId = oItem._sLinkedMId;
        }

        //Propertie
        public string Name
        {
            get
            {
                return _sName;
            }

            set
            {
                _sName = value;
            }
        }
        public string TrendNr
        {
            get
            {
                return _sTrendNr;
            }

            set
            {
                _sTrendNr = value;
            }
        }
        public string mID
        {
            get
            {
                if (_sLinkedMId == "")
                {
                    return null;
                }
                else
                {
                    return _sLinkedMId;
                }
            }
            set { _sLinkedMId = value; }
        }

        //ToString
        public override string ToString()
        {
            return Name;
        }

    }
}
