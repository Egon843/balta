﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BEC
{
    public class clsItemData
    {
        private string _sNaam;
        private string _sValue;
        private string _sEenheid;
        private string _sMaxGrens;
        private string _sMinGrens;
        private string _sKleurNr;
        private string _sTrendNr;

        private string _sSubgroep;

        public clsItemData()
        { }
        public clsItemData(string sTrendNr, string sNaam, string sValue, string sEenheid, string sMaxGrens, string sMinGrens, string sKleurNr, string sSubgroep)
        {
            _sTrendNr = sTrendNr;
            _sNaam = sNaam;
            _sValue = sValue;
            _sEenheid = sEenheid;
            _sMaxGrens = sMaxGrens;
            _sMinGrens = sMinGrens;
            _sKleurNr = sKleurNr;
            _sSubgroep = sSubgroep;
        }

        
        public override string ToString()
        {
            return Machine +"-"+ Naam;
        }

        public string Naam
        {
            get { return _sNaam; }
            set { _sNaam = value; }
        }
        public string Value
        {
            get { return _sValue; }
            set { _sValue = value; }
        }
        public string Eenheid
        {
            get { return _sEenheid; }
            set { _sEenheid = value; }
        }
        public string MaxGrens
        {
            get { return _sMaxGrens; }
            set { _sMaxGrens = value; }
        }
        public string MinGrens
        {
            get { return _sMinGrens; }
            set { _sMinGrens = value; }
        }
        public string KleurNr
        {
            get { return _sKleurNr; }
            set { _sKleurNr = value; }
        }
        public string TrendNr
        {
            get { return _sTrendNr; }
            set { _sTrendNr = value; }
        }

        public string Machine
        {
            get
            {
                return _sSubgroep;
            }

            set
            {
                _sSubgroep = value;
            }
        }
    }
}
