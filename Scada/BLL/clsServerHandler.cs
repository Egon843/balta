﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using DAL;
using BEC;
using System.IO;

namespace BLL
{
    public class clsServerHandler
    {
        //Aanpassen en afvragen van XML-bestand
        //Geen commentaar toegevoegd


        private string _sServerFilePath;//de bestandslocatie wordt toegewezen in de constructor
               
        //constructor
        public clsServerHandler()
        {
            //bestandslocatie
            string sPath = System.IO.Directory.GetCurrentDirectory();
            _sServerFilePath = Path.Combine(sPath + "\\Servers.config");
        }
             
        //Opvragen ConnectionString        
        public string GetConnectionString(string sVestiging, string sAfdeling)
        {
            string sServer;
            string sUserId;
            string sPwd;
            string sDatabase;

            GetServerInfo(sVestiging, out sServer, out sUserId, out sPwd);
            GetDatabaseInfo(sVestiging, sAfdeling, out sDatabase);

            //Andere connectionstring zonder wachtwoord
            if (sPwd.Count() == 0)
            {
                return "Trusted_Connection=True ;Server=" + sServer + ";Database=" + sDatabase;
            }
            else
            {
                return "Server=" + sServer + ";Database=" + sDatabase + ";User Id=" + sUserId + ";Password=" + sPwd;
            }

            //structuur voorbeeld
            //_sConnStr = "Data Source = extprddbssbv01; Initial Catalog=ProcesDataTuft; User ID=ProcesDbUser; Password = balta";
           
        }

        //Opvragen mogelijke Vestigingen
        public List<string> GetServers()
        {
            List<string> oVestigingen = new List<string>();

            XDocument oXdoc = XDocument.Load(_sServerFilePath);
            List<XElement> oXvestigingen = oXdoc.Descendants("Vestiging").ToList();

            foreach (XElement oXvestiging in oXvestigingen)
            {
                oVestigingen.Add(oXvestiging.Attribute("Name").Value);
            }
            return oVestigingen;
        }

        //Opvragen mogelijke Afdelingen
        public List<string> GetDatabases(string sVestiging)
        {
            List<string> oAfdelingen = new List<string>();

            XDocument oXdoc = XDocument.Load(_sServerFilePath);
            List<XElement> lstXVestigingen = oXdoc.Descendants("Vestiging").ToList();
            XElement oXvestiging = lstXVestigingen.First(Xel => Xel.Attribute("Name").Value == sVestiging);

            foreach (XElement oXafdeling in oXvestiging.Nodes())
            {
                oAfdelingen.Add(oXafdeling.Attribute("Name").Value);
            }
            return oAfdelingen;
        }

        //Voor het aanpassen van de mogelijke DB connecties
        #region Wijzigen Vestigingen/Afdelingen

        public void GetServerInfo(string sVestiging, out string sServer, out string sUserId, out string sPwd)
        {
            XDocument oXdoc = XDocument.Load(_sServerFilePath);
            XElement oXvestiging = oXdoc.Descendants("Vestiging")
                .FirstOrDefault(oXel => oXel.Attribute("Name").Value == sVestiging);

            sServer = oXvestiging.Attribute("Server").Value;
            sUserId = oXvestiging.Attribute("UserId").Value;
            sPwd = oXvestiging.Attribute("Pwd").Value;
        }

        public void GetDatabaseInfo(string sVestiging, string sAfdeling, out string sDatabase)
        {
            XDocument oXdoc = XDocument.Load(_sServerFilePath);
            XElement oXvestiging = oXdoc.Descendants("Vestiging")
                .FirstOrDefault(oXel => oXel.Attribute("Name").Value == sVestiging);
            XElement oXafdeling = oXvestiging.Descendants("Afdeling")
                .FirstOrDefault(oXel => oXel.Attribute("Name").Value == sAfdeling);

            sDatabase = oXafdeling.Attribute("Database").Value;
        }

        public void AddOrChangeServer(string sVestiging, string sServer, string sUserId, string sPwd)
        {
            XDocument oXdoc = XDocument.Load(_sServerFilePath);

            List<XElement> lstXVestigingen = oXdoc.Descendants("Vestiging").ToList();

            XElement Xvestiging;
            //Xvestiging nieuw of bestaand?
            if (lstXVestigingen.Exists(Xel => Xel.Attribute("Name").Value == sVestiging))
            {
                //bestaande vestiging gebruiken
                Xvestiging = lstXVestigingen.Find(Xel => Xel.Attribute("Name").Value == sVestiging);
            }
            else
            {
                //definiëren van de nieuwe vestiging en toevoegen
                Xvestiging = new XElement("Vestiging", null);
                oXdoc.Root.Add(Xvestiging);
            }

            //aanpassen gegevens van de vestiging
            Xvestiging.SetAttributeValue("Name", sVestiging);
            Xvestiging.SetAttributeValue("Server", sServer);
            Xvestiging.SetAttributeValue("UserId", sUserId);
            Xvestiging.SetAttributeValue("Pwd", sPwd);

            oXdoc.Save(_sServerFilePath);
        }

        public void DeleteServer(string sVestiging)
        {
            XDocument oXdoc = XDocument.Load(_sServerFilePath);

            oXdoc.Descendants("Vestiging")
                .First(Xel => Xel.Attribute("Name").Value == sVestiging)
                .Remove();

            oXdoc.Save(_sServerFilePath);
        }

        public void AddOrChangeDatabase(string sVestiging, string sAfdeling, string sDatabase)
        {
            XDocument oXdoc = XDocument.Load(_sServerFilePath);

            XElement Xvestiging = oXdoc.Descendants("Vestiging")
                .First(Xel => Xel.Attribute("Name").Value == sVestiging);

            List<XElement> lstXAfdelingen = Xvestiging.Elements().ToList();

            XElement Xafdeling;

            if (lstXAfdelingen.Exists(Xel => Xel.Attribute("Name").Value == sAfdeling))
            {
                //bestaande afdeling gebruiken
                Xafdeling = lstXAfdelingen.Find(Xel => Xel.Attribute("Name").Value == sAfdeling);
            }
            else
            {
                //definiëren van de nieuwe afdeling en toevoegen
                Xafdeling = new XElement("Afdeling");
                Xvestiging.Add(Xafdeling);
            }

            Xafdeling.SetAttributeValue("Name", sAfdeling);
            Xafdeling.SetAttributeValue("Database", sDatabase);

            oXdoc.Save(_sServerFilePath);
        }

        public void DeleteDatabase(string sVestiging, string sAfdeling)
        {
            XDocument oXdoc = XDocument.Load(_sServerFilePath);

            List<XElement> lstVestigingen = oXdoc.Descendants("Vestiging").ToList();
            XElement Xvestiging = lstVestigingen
                .First(Xel => Xel.Attribute("Name").Value == sVestiging);
            Xvestiging.Elements("Afdeling")
                .First(Xel => Xel.Attribute("Name").Value == sAfdeling)
                .Remove();

            oXdoc.Save(_sServerFilePath);
        }

        #endregion
    }
}
