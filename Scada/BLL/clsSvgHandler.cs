﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEC;
using DAL;
using System.IO;
using System.Xml.Linq;

namespace BLL
{
    public class clsSvgHandler
    {
        //De locatie van de geconfigureerde Svg bestanden
       //private string _sSvgDirectory = @"C:\sch00l\2019\Masterproef\SVG bestand laden\SvgFiles\Svgs";
        private string _sSvgDirectory = "\\\\sbvtech\\Technische_dienst\\Technische_projecten\\Svgs";

        //De XML namespace van een SVG-document
        private XNamespace _oNS;
        
        //Constructor
        public clsSvgHandler()
        { }

        //Convert SVG
        #region Convertion

        //Deze methode spreekt alle onderliggende methodes (binnen deze region) aan
        private XDocument _ConvertToScada(XDocument oSVG)
        {            
            oSVG = _Simplify(oSVG);
            _AddQlikStyles(oSVG);
            _AddGlowFilter(oSVG);
            //_AddWidth(oSVG.Root);

            XElement oMainGroup = oSVG.Root.Elements(_oNS + "g").First();
            foreach (XElement oGroup in oMainGroup.Elements(_oNS + "g"))
            {
                _AddOnClickEvent(oGroup);
                _EraseTitle(oGroup);

                // '-1' = geen link (QLIK)
                oGroup.SetAttributeValue("id", "-1");
            }

            //Tag as converted file
            oSVG.Root.SetAttributeValue("scada-file", "true");

            return oSVG;
        }
        
        private XDocument _Simplify(XDocument oSvg)
        {
            //VISIO
            //Replace v:mID to mID 
            string sSvg = oSvg.ToString();
            sSvg = sSvg.Replace("v:mID", "mID");
            oSvg = XDocument.Parse(sSvg);
            oSvg.Declaration = new XDeclaration("1.0", "UTF-8", "no");


            //Alle elementen die verwijzen naar de 'Visio' namespace zijn overbodig
            List<XElement> oNodesToDelete = oSvg.Descendants().Where(oNode => oNode.Name.NamespaceName == "http://schemas.microsoft.com/visio/2003/SVGExtensions/").ToList();
            foreach (XElement oXel in oNodesToDelete)
            {
                oXel.Remove();
            }

            //De groep waarin alle visuele groepen zich bevinden
            XElement oMainGroup = oSvg.Root.Elements(_oNS + "g").First();

            //voor elke visuele groep
            foreach (XElement oGroup in oMainGroup.Elements(_oNS + "g"))
            {
                //Het group-element + child-elementen 
                foreach (XElement oXel in oGroup.DescendantsAndSelf())
                {
                    List<XAttribute> oAttsToDelete = oXel.Attributes().Where(oAttr => oAttr.Name.NamespaceName == "http://schemas.microsoft.com/visio/2003/SVGExtensions/").ToList();
                    //Voor elk element die 'Visio' attributen bevat
                    foreach (XAttribute oXattr in oAttsToDelete)
                    {
                        //Verwijder de attributen
                        oXattr.Remove();
                    }
                }
                
                //Als de groep een text element bevat mogen de andere elementen verwijdert worden.
                if (oGroup.Elements(_oNS + "text").Any())
                {
                    oGroup.Element(_oNS + "rect").Remove();
                }
            }

            return oSvg;
        }
        private void _AddWidth(XElement oRoot)
        {
            string sWidth = oRoot.Attribute("width").Value;
            string sFirst = sWidth[0].ToString();
            int iFirst = Convert.ToInt32(sFirst);
            if (iFirst == 9)
            {
                sWidth = "1" + sWidth;
            }
            else
            {
                sWidth = (iFirst + 1).ToString() + sWidth.Substring(1);
            }
            oRoot.SetAttributeValue("width", sWidth);
        }
        private void _AddGlowFilter(XDocument oXdoc)
        {
            XElement oMainGroup = oXdoc.Root.Elements(_oNS + "g").First();

            //Add GlowFilter for selection
            XElement oFilter = XElement.Parse("<filter id='Glow' height='300%' width='300%' x='-75%' y='-75%'> \n" +
                                    "\t\t<feMorphology operator='dilate' radius='4' in='SourceAlpha' result='thicken' /> \n" +
                                    "\t\t<feGaussianBlur in='thicken' stdDeviation='5' result='blurred' /> \n" +
                                    "\t\t<feFlood flood-color='rgb(0,100,20)' result='glowColor' /> \n" +
                                    "\t\t<feComposite in='glowColor' in2='blurred' operator='in' result='softGlow_colored' /> \n" +
                                    "\t\t<feMerge> \n" +
                                    "\t\t\t<feMergeNode in='softGlow_colored'/> \n" +
                                    "\t\t\t<feMergeNode in='SourceGraphic'/> \n" +
                                    "\t\t</feMerge> \n" +
                                    "\t</filter> \n\n ", LoadOptions.PreserveWhitespace);

            oMainGroup.AddBeforeSelf(oFilter);

            oMainGroup.AddBeforeSelf(Environment.NewLine);
            oMainGroup.AddBeforeSelf(Environment.NewLine);
            oMainGroup.AddBeforeSelf(Environment.NewLine);

            //Delete empty namespaces????
            oFilter.Attributes("xmlns").Remove();
            oFilter.Name = oFilter.Parent.Name.Namespace + oFilter.Name.LocalName;

            foreach (XElement oXel in oFilter.Descendants())
            {
                oXel.Attributes("xmlns").Remove();
                oXel.Name = oFilter.Parent.Name.Namespace + oXel.Name.LocalName;
            }
        }
        private void _AddQlikStyles(XDocument oXdoc)
        {
            try
            {
                XElement oStyleSheet = oXdoc.Root.Element(_oNS + "style");
                string sStyles = oStyleSheet.Value;

                //verwijdert de '.st'
                string[] arrsStyles = sStyles.Split(new string[] { ".st" }, StringSplitOptions.None);

                foreach (string sStyle in arrsStyles)
                {
                    //witte regels uitsluiten
                    if (sStyle.Contains("{"))
                    {
                        //'.Qst' toevoegen
                        string sNewStyle = ".Qst" + sStyle;

                        //remove fill attribute (niet bij tekst-stijlen)
                        if (sNewStyle.Contains("font"))
                        {
                            string[] sSplit = sNewStyle.Split('{');
                            sNewStyle = sSplit[0] + "{fill-opacity:1;" + sSplit[1];
                        }
                        else
                        {
                            int iFillStart = sNewStyle.IndexOf("fill");
                            if (iFillStart > -1)
                            {
                                if (sNewStyle.Contains(";"))
                                {
                                    int iFillEnd = sNewStyle.IndexOf(";", iFillStart);
                                    sNewStyle = sNewStyle.Remove(iFillStart, iFillEnd - iFillStart + 1);
                                }
                                
                            }
                        }
                        sStyles = sStyles + sNewStyle;
                    }
                }
                oStyleSheet.Value = sStyles;
            }
            catch (Exception)
            {
                //Geen stylesheet aanwezig 
            }
        }
        private void _AddOnClickEvent(XElement oXel)
        {
            string Event = "onclick";
            string Form = "window.external.";
            string EventHandler = "SvgGroup_Click";
            string Argument = "(this.getAttribute('mID'))";

            oXel.SetAttributeValue(Event, Form + EventHandler + Argument);


            //if (oXel.Elements(_oNS + "text").Any())
            //{
            //    //niet bij een text
            //}
            //else
            ////nooit een text in een figuur plaatsen, altijd apart (andere groep) (bij te converteren bestand)
            //{
            ////doorzichtige vulkleur vulkleur zorgt ervoor dat de inhoud van een figuur ook 'klikbaar' is en niet alleen de rand
            oXel.SetAttributeValue("fill", "#FFFFFF");//wit
            oXel.SetAttributeValue("fill-opacity", "0.0");//doorzichtig

            //anders wordt de fill-opacity van de SVG-group overgeërfd
            foreach (XElement item in oXel.Elements())
            {
                item.SetAttributeValue("fill-opacity", "1.0");
            }
            //}
        }
        private void _EraseTitle(XElement oXel)
        {
            string sTitle = oXel.Element(_oNS + "title").Value;

            if (sTitle.Contains("Blad"))
            {
                oXel.SetElementValue(_oNS + "title", "");
            }
            else if (sTitle.Contains("Sheet"))
            {
                oXel.SetElementValue(_oNS + "title", "");
            }
            else if (sTitle.Contains("Page"))
            {
                oXel.SetElementValue(_oNS + "title", "");
            }
            else if (sTitle.Contains("Circle"))
            {
                oXel.SetElementValue(_oNS + "title", "");
            }
            //...
        }


        #endregion

        // returns 'null' if file does not exist
        public clsSvg LoadExistingFile(string sFileName, string sVestiging, string sAfdeling)
        {
            string sFilePath = Path.Combine(_sSvgDirectory, sVestiging, sAfdeling, sFileName);
            return LoadNewFile(sFileName, sFilePath);          
        }

        //converteert de svg file 
        public clsSvg LoadNewFile(string sSvgName, string sFilePath)
        {

            if (File.Exists(sFilePath))
            {
                XDocument oXDoc = XDocument.Load(sFilePath);
                _oNS = oXDoc.Root.GetDefaultNamespace();

                if (oXDoc.Root.Attributes("scada-file").Any(attr => attr.Value == "true"))
                {
                    //File is reeds geconverteerd
                    return new clsSvg(oXDoc, sSvgName);
                }
                else
                {
                    oXDoc = _ConvertToScada(oXDoc);
                    return new clsSvg(oXDoc, sSvgName);
                }
            }
            else
            {//File bestaat niet
                return null;
            }
        }
        
        //Opslaan van het SVG-bestand
        public void SaveSvgFile(clsSvg oSvg, string sVestiging, string sAfdeling)
        {
            //Map aanmaken indien deze nog niet bestaat
            string sDirectoryPath = Path.Combine(_sSvgDirectory, sVestiging, sAfdeling);
            Directory.CreateDirectory(sDirectoryPath);

            //het XML-bestand uit de machine opslaan met de correcte naam
            string sFileName = oSvg.FileName;
            string sFilePath = Path.Combine(sDirectoryPath, sFileName);

            //De filters verwijderen bij het opslaan van het bestand
            oSvg.RemoveAllGlow();

            //Opslaan
            XDocument oXdoc = oSvg.SvgDocument;
            oXdoc.Save(sFilePath);
        }

        //Verwijderen van het SVG-bestand
        public void RemoveSvgFile(clsSvg oSvg, string sVestiging, string sAfdeling)
        {
            string sDirectoryPath = Path.Combine(_sSvgDirectory, sVestiging, sAfdeling);
            string sFilePath = Path.Combine(sDirectoryPath, oSvg.FileName);
            File.Delete(sFilePath);
        }
        


        //zet de gebruiker in commentaar in het SVG-bestand
        //public void ModifyAdmin(string sUserName, clsSvg oSvg)
        //{
        //    oSvg.ModifyAdmin(sUserName);
        //}

        //niet hier genereren?

        //public void GenerateSubgroups(clsSubgroep oGenerateFrom, List<clsSubgroep> lstSvgToGenerate)
        //{
        //    Dictionary<string, string> OriginalLinks = _oLinksSvg2Sql;

        //    foreach (clsSubgroep subgroep in lstSvgToGenerate)
        //    {
        //        int iOrigOffset = oGenerateFrom.Offset;
        //        int iNewOffset = subgroep.Offset;
        //        string sSvgName = subgroep.Name;

        //        foreach (var SqlLink in oGenerateFrom.oLinksSvg2Sql)
        //        {
        //            string mID = SqlLink.Key;
        //            int iOrigTrendNr = Convert.ToInt32(SqlLink.Value);
        //            string iNewTrendNr = (iOrigTrendNr - iOrigOffset + iNewOffset).ToString();

        //            //clsSvgGroep???
        //            //SVG groep zoeken met deze 'mID' en het TrendNr invullen in het 'id'
        //            XElement oSvgGroup = _lstSvgGroups.Find(El => El.Attributes("mID").FirstOrDefault().Value == mID);
        //            oSvgGroup.SetAttributeValue("id", iNewTrendNr);
        //        }

        //        string sFilePath = AppDomain.CurrentDomain.BaseDirectory;
        //        SaveFile(sFilePath + "\\CreatedSvgs\\" + sSvgName);
        //    }
        //}


    }
}
