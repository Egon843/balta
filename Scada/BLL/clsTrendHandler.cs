﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Windows.Forms.DataVisualization.Charting;
 

namespace BLL
{
    public class clsTrendHandler
    {
        //Om gegevens uit de database te halen
        private clsSqlHandler _oSqlHandler = new clsSqlHandler();
   
        //Constructor
        public clsTrendHandler(string sConnString)
        {
            _oSqlHandler.ConnectionString = sConnString;
        }
           
        //Alle gegevens opvragen door het aanpassen van het trendnummer
        public List<Series> GetAllSeries(DateTime dtBegin, DateTime dtEind, string sTrendNr2)
        {
            int iTrendNr2 = int.Parse(sTrendNr2);

            Series oCreatedSeries;//Als tussenstap
                        
            List<Series> oSeries = new List<Series>();

            oCreatedSeries = _GetSeries(dtBegin, dtEind, iTrendNr2);
            oCreatedSeries.Name = "Proces value";
            oSeries.Add(oCreatedSeries);

            oCreatedSeries = _GetSeries(dtBegin, dtEind, iTrendNr2 + 200);
            oCreatedSeries.Name = "Setpoint";
            oSeries.Add(oCreatedSeries);

            oCreatedSeries = _GetSeries(dtBegin, dtEind, iTrendNr2 + 400);
            oCreatedSeries.Name = "Output value";
            oSeries.Add(oCreatedSeries);

            oCreatedSeries = _GetSeries(dtBegin, dtEind, iTrendNr2 + 600);
            oCreatedSeries.Name = "Maximum";
            oSeries.Add(oCreatedSeries);

            oCreatedSeries = _GetSeries(dtBegin, dtEind, iTrendNr2 + 800);
            oCreatedSeries.Name = "Minimum";
            oSeries.Add(oCreatedSeries);

            oCreatedSeries = _GetSeries(dtBegin, dtEind, iTrendNr2 + 1000000);
            oCreatedSeries.Name = "Average";
            oSeries.Add(oCreatedSeries);

            oCreatedSeries = _GetSeries(dtBegin, dtEind, iTrendNr2 + 2000000);
            oCreatedSeries.Name = "ED";
            oSeries.Add(oCreatedSeries);

            oCreatedSeries = _GetSeries(dtBegin, dtEind, iTrendNr2 + 3000000);
            oCreatedSeries.Name = "Count value";
            oSeries.Add(oCreatedSeries);

            //De series die geen gegevens bevatten niet meegeven
            oSeries.RemoveAll(oS => oS.Points.Count == 0);
            return oSeries;
        }

        //Gegevens opvragen van 1 'soort'
        private Series _GetSeries(DateTime dtBegin, DateTime dtEind, int iTrendNr2)
        {
            //de correcte data opvragen in 2 lijsten X & Y
            Dictionary<string, List<string>> oDataXY = _oSqlHandler.GetDataXY(dtBegin, dtEind, iTrendNr2);
            List<string> oDataX = oDataXY["X"];
            List<string> oDataY = oDataXY["Y"];

            List<DateTime> oDataXtime = new List<DateTime>();
            foreach (string sTime in oDataX)
            {
                oDataXtime.Add(Convert.ToDateTime(sTime));
            }

            //de Y data converteren naar decimaal waarden
            //kleine variaties (bits) wegwerken tot 2 getallen na de komma
            List<double> oDataYdouble = oDataY.Select(y => Math.Round(double.Parse(y.Replace(".",",")),2)).ToList();
            
            //de series opbouwen
            Series oSeries = new Series();
            oSeries.Points.DataBindXY(oDataXtime, oDataYdouble);
            oSeries.ChartType = SeriesChartType.Line;

            return oSeries;
        }

      
    }
}
