﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;//hashing
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BLL
{
    //statische klasse, geen constructor, geen objecten
    public static class clsHasher
    {        

        //Bestands locatie
        private static string _FilePath()
        {
            //bestandslocatie
            string sPath = System.IO.Directory.GetCurrentDirectory();
            return Path.Combine(sPath + "\\Hashes");
        }

        //Gebruiker Toevoegen
        public static void AddUser(string sUser, string sPwd)
        {
            string sText = sUser.ToLower() + sPwd;
            string sHash = GenerateHashString(new SHA1Managed(), sText);
            _AddHash(sUser, sHash);
        }

        //Wachtwoord Controleren
        public static bool CheckHash(string sUser, string sPwd)
        {
            string sText = sUser.ToLower() + sPwd;
            string sHash = GenerateHashString(new SHA1Managed(), sText);

            XDocument oXdoc = XDocument.Load(_FilePath());
            return oXdoc.Descendants("Hash").Any(oHash => oHash.Value == sHash);
        }

        //Gebruiker Verwijderen
        public static void RemoveUser(string sUser)
        {
            try
            {
                XDocument oXdoc = XDocument.Load(_FilePath());
                oXdoc.Descendants("Hash").First(oHash => oHash.Attribute("User").Value == sUser).Remove();
                oXdoc.Save(_FilePath());
            }
            catch (Exception)
            {  }            
        }

        //Gebruikers opvragen
        public static List<string> GetUsers()
        {
            List<string> oUsers = new List<string>();

            XDocument oXdoc = XDocument.Load(_FilePath());
            foreach(XElement oXel in oXdoc.Descendants("Hash"))
            {
                oUsers.Add(oXel.Attribute("User").Value);
            }

            return oUsers;
        }

        //Tutorial
        //https://www.godo.dev/tutorials/csharp-string-hash/
        private static string GenerateHashString(HashAlgorithm algo, string text)
        {
            // Compute hash from text parameter
            algo.ComputeHash(Encoding.UTF8.GetBytes(text));

            // Get has value in array of bytes
            var result = algo.Hash;

            // Return as hexadecimal string
            return string.Join(
                string.Empty,
                result.Select(x => x.ToString("x2")));
        }      

        //Hash toevoegen
        private static void _AddHash(string sUser, string sHash)
        {
            XDocument oXdoc = XDocument.Load(_FilePath());
            oXdoc.Root.Add(new XElement("Hash", new XAttribute("User", sUser), sHash));
            oXdoc.Save(_FilePath());
        }

    }
}
