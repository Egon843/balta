﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BEC;

namespace BLL
{
    public class clsMachineHandler
    {
        private clsSqlHandler _oSqlHandler = new clsSqlHandler();

        //De gekozen database instellen
        public void SetDatabase(string sConnStr)
        {
            _oSqlHandler.ConnectionString = sConnStr;
        }      
           
        //Mogelijke groepen laden
        public List<clsMachine> GetGroups()
        {
            return _oSqlHandler.GetGroups();
        }

        //Mogelijke levels laden
        public List<clsMachine> GetLevels(int iGroup)
        {
            return _oSqlHandler.GetLevels(iGroup);
        }

        //Mogelijke subgroepen laden
        public List<clsMachine> GetSubgroups(int iGroup, int iLevel)
        {
            return _oSqlHandler.GetSubgroups(iGroup, iLevel);
        }

        //Mogelijke trendgroepen laden
        public Dictionary<int,string> GetTrendgroups(clsMachine oSubgroup)
        {
            return _oSqlHandler.GetTrendgroups(oSubgroup.GroupNr, oSubgroup.LevelNr, oSubgroup.SubgroupNr);
        }

        //Mogelijke TrendTags laden
        // in clsmachine??
        public List<clsSvgLink> GetItems(int iGroep, int iLevel, int iSubGroep, int iTrendGroupNr)
        {
            try
            {
                return _oSqlHandler.GetItems(iGroep, iLevel, iSubGroep, iTrendGroupNr);
            }
            catch (Exception)
            {
                return null;
            }             
        }

        //Meest recente waarde van alle gelinkte TrendTags opvragen
        public List<clsItemData> GetItemValues(List<clsSvgLink> oLinkedItems)
        {
            List<string> oTrendNrs = new List<string>();

            foreach (clsSvgLink oLink in oLinkedItems)
            {
                oTrendNrs.Add(oLink.TrendNr);
            }
            return _oSqlHandler.GetItemValues(oTrendNrs);
        }
        
        //???
        //OEE waarde opvragen
        public string GetOEE(string sTrendNr2, DateTime oBegin, DateTime oEind)
        {
            //'4' verwijderen?
            if (sTrendNr2.Count() == 7)
            {
                sTrendNr2 = sTrendNr2.Substring(1);
            }

            string sValue = _oSqlHandler.GetOEE(sTrendNr2, oBegin, oEind);
            return sValue + " [%]";
        }
        
        //Links opvragen
        public List<clsSvgLink> GetLinkedItems(clsMachine oMachine, clsSvg oSvg)
        {            
            if (oMachine != null)
            {
                if (oSvg != null)
                {
                    //Lijst van 'SvgLinks' opvragen
                    List<clsSvgLink> lstLinkedItems = _oSqlHandler.GetLinkedItems(oSvg.FileName);

                    if (lstLinkedItems.Count() > 0)
                    {
                        //Voor de eerder gemaakte links zonder 'TagName' => 'Naam'
                        if (lstLinkedItems.Any(oItem => oItem.Name == ""))
                        {
                            try
                            {
                                List<string> lstTrendNrs = new List<string>();

                                foreach (clsSvgLink oItem in lstLinkedItems)
                                {
                                    lstTrendNrs.Add(oItem.TrendNr);                             
                                }

                                Dictionary<string, string> dcTrendNr2TagName = _oSqlHandler.GetTagNames(lstTrendNrs);

                                _oSqlHandler.AddTagName2TrendNr(oSvg.FileName, dcTrendNr2TagName);

                                //Lijst van 'SvgLinks' opnieuw opvragen
                                lstLinkedItems = _oSqlHandler.GetLinkedItems(oSvg.FileName);
                            }
                            catch (Exception)
                            {
                            }
                        }

                        //Voor de eerder gemaakte links zonder 'mID'
                        if (lstLinkedItems[0].mID == null)
                        {
                            //De SVG-groups met een link verzamelen uit de SVG
                            List<clsSvgGroep> lstLinkedGroups = new List<clsSvgGroep>();
                            foreach (string mID in oSvg.Linked_mIds)
                            {
                                clsSvgGroep oSvgGroup = oSvg.SvgGroups.Find(svgGroup => svgGroup.mID == mID);
                                lstLinkedGroups.Add(oSvgGroup);
                            }

                            try
                            {
                                Dictionary<string, string> dcTrendNr2mID = new Dictionary<string, string>();

                                foreach (clsSvgLink oItem in lstLinkedItems)
                                {
                                    int iIndex = lstLinkedGroups.FindIndex(oGr => oGr.TrendTag == oItem.TrendNr);
                                    oItem.mID = lstLinkedGroups[iIndex].mID;
                                    lstLinkedGroups.RemoveAt(iIndex);

                                    dcTrendNr2mID.Add(oItem.TrendNr, oItem.mID);
                                }

                                //tabel updaten met mID's
                                _oSqlHandler.AddMId2TrendNr(oSvg.FileName, dcTrendNr2mID);

                            }
                            catch (Exception)
                            {
                            }
                        }
                    }                   
                    
                    return lstLinkedItems;
                }
            }
            //Als er geen links gevonden zijn
            return new List<clsSvgLink>();
        }

        //Link toevoegen
        public void AddLink(clsSvg oSvg, string mID, string sTrendNr, string sTagName)
        {
            oSvg.MakeLink(mID, sTrendNr, sTagName);
            _oSqlHandler.AddLinkToDB(oSvg.FileName, sTrendNr, mID, sTagName);
        }

        //Link toevoegen enkel aan SVG
        public void AddLinkToSvg(clsSvg oSvg, string mID, string sTrendNr, string sTagName)
        {
            oSvg.MakeLink(mID, sTrendNr, sTagName);
        }

        //Link verwijderen enkel uit DB
        public void RemoveLinkFromDb(clsSvg oSvg, string mID)
        {
            _oSqlHandler.RemoveLinkFromDB(oSvg.FileName, mID);
        }

        //Link verwijderen
        public void RemoveLink(clsSvg oSvg, string mID)
        {
            oSvg.RemoveLink(mID);
            _oSqlHandler.RemoveLinkFromDB(oSvg.FileName, mID);
        }

        //???
        //Genereren van Subgroepen
        public List<clsSvg> GenerateSubgroup(clsSvg oOrigSvg, clsMachine oOrigMachine, List<clsMachine> lstToGenerate)
        {
            List<clsSvg> lstNewSvg = new List<clsSvg>();

            foreach (clsMachine oGenMachine in lstToGenerate)
            {
                //kopie maken van SVG
                clsSvg oGenSvg = new clsSvg(oOrigSvg.SvgDocument, oGenMachine.GenerateSvgFileName());

                //Oude en nieuwe offset bepalen
                int iOrigOffset = oOrigMachine.Offset;
                int iNewOffset = oGenMachine.Offset;

                //Voor elke SVG-group
                foreach (string mID in oOrigSvg.Linked_mIds)
                {
                    //Offset verrekenen
                    string sOrigTag = oOrigSvg.SvgGroups.Find(svgGroep => svgGroep.mID == mID).TrendTag;
                    int iOrigTag = Convert.ToInt32(sOrigTag);
                    string sNewTrendNr = (iOrigTag - iOrigOffset + iNewOffset).ToString();

                    //Tuftmachines??????????????????????????????????????????????????????????????????????????????????,
                    string sTrendNr_First3 = ((oGenMachine.GroupNr * 100) + (oGenMachine.LevelNr * 10) + (oGenMachine.TrendgroupNr)).ToString();
                    sNewTrendNr = sTrendNr_First3 + sNewTrendNr.Substring(sNewTrendNr.Length - 3);

                    //opletten met 4xxxxxx

                    string sTagName = oOrigSvg.SvgGroups.Find(svgGr => svgGr.mID == mID).GetTagName();

                    AddLink(oGenSvg, mID, sNewTrendNr, sTagName);
                }
                lstNewSvg.Add(oGenSvg);
            }
            return lstNewSvg;
        }        
    }
}
