# LOG #

### 05/10/2018 ###
* Wat wordt er verwacht?
- 'scada' systeem in .net om...

* SVG bestand maken van machine(lijn)
* SVG bestand inladen in .NET (C#)
* SVG library ontdekken

clsLoadGsv_V1 -> inladen van de .svg file en weergeven in de applicatie als 1 figuur

### 12/10/2018 ###
* Overlopen van het totale project met Etienne
* Project sheet invullen
* Ruwe jaarplanning opstellen
* nuttige functies uit de SVG library zoeken om de elementen apart te weergeven

### 16/10/2018 ###
* Figuren tekenen in C# (System.Drawing.Drawing2D, GraphicsPath)
	
### 19/10/2018 ###
* De nuttige informatie uit het SVG bestand halen
* Branch 'ElementXmlTool'
+ Deze tool toont de SvgElementen in XML-formaat

### 26/10/2018 ###
* SVG-library documenteren om een beter inzicht te verkrijgen 
- Hoe wordt een SvgDocument tot bitmap gerenderd?
- Kan een SvgGroup op dezelfde manier worden gerenderd?

### 27/10/2018 ###
* clsMachine kan nu zijn 'MachineParts' terug geven als lijst van SvgGroup
+ GetMyParts()

### 28/10/2018 ###
* clsSvgHandler kan nu ook SvgGroups tot bitmap renderen
+ GetBitmapFromGroup()
- bitmap past zich niet aan tot juiste formaat


### De XML-tool moet bijgewerkt worden ###
